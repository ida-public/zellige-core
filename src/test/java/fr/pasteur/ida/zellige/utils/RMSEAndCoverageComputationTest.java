/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils;

import fr.pasteur.ida.zellige.utils.test.RMSEAndCoverageComputation;
import fr.pasteur.ida.zellige.utils.test.exception.DifferentReferenceTestedSizeException;
import fr.pasteur.ida.zellige.utils.test.exception.NotAnHeightMapException;
import io.scif.img.IO;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.IntType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;

public class RMSEAndCoverageComputationTest
{

    @DisplayName( " The reference height map is not null" )
    @Test
    void whenTheReferenceHMisNullAnExceptionIsRaised()
    {
        ImgFactory< IntType > testedFactory = new ArrayImgFactory<>( new IntType() );
        RandomAccessibleInterval< IntType > tested = testedFactory.create( 4, 5 );
        assertThrows( NullPointerException.class, () -> new RMSEAndCoverageComputation< IntType, IntType >( null, 0, tested ) );
    }

    @DisplayName( " The tested height map is not null" )
    @Test
    void whenTheTestedHMisNullAnExceptionIsRaised()
    {
        ImgFactory< IntType > refFactory = new ArrayImgFactory<>( new IntType() );
        RandomAccessibleInterval< IntType > ref = refFactory.create( 5, 5 );
        assertThrows( NullPointerException.class, () -> new RMSEAndCoverageComputation< IntType, IntType >( ref, 0, null ) );
    }

    @DisplayName( "The reference height map is a 2D image" )
    @Test
    void a3DImageCannotBeAReferenceHeightMap()
    {
        ImgFactory< IntType > refFactory = new ArrayImgFactory<>( new IntType() );
        RandomAccessibleInterval< IntType > ref = refFactory.create( 5, 5, 5 );
        ImgFactory< IntType > testedFactory = new ArrayImgFactory<>( new IntType() );
        RandomAccessibleInterval< IntType > tested = testedFactory.create( 4, 5 );
        assertThrows( NotAnHeightMapException.class, () -> new RMSEAndCoverageComputation<>( ref, 0, tested ) );
    }

    @DisplayName( "The tested height map is a 2D image" )
    @Test
    void a3DImageCannotBeATestedHeightMap()
    {
        ImgFactory< IntType > refFactory = new ArrayImgFactory<>( new IntType() );
        RandomAccessibleInterval< IntType > ref = refFactory.create( 5, 5 );
        ImgFactory< IntType > testedFactory = new ArrayImgFactory<>( new IntType() );
        RandomAccessibleInterval< IntType > tested = testedFactory.create( 4, 5, 2 );
        assertThrows( NotAnHeightMapException.class, () -> new RMSEAndCoverageComputation<>( ref, 0, tested ) );
    }

    @DisplayName( " The reference height map has the same size as the tested height map" )
    @Test
    void whenTheReferenceHMAndTheTestedHMHaveADifferentSizeAnExceptionIsRaised()
    {
        ImgFactory< IntType > refFactory = new ArrayImgFactory<>( new IntType() );
        RandomAccessibleInterval< IntType > ref = refFactory.create( 5, 5 );
        ImgFactory< IntType > testedFactory = new ArrayImgFactory<>( new IntType() );
        RandomAccessibleInterval< IntType > tested = testedFactory.create( 4, 5 );
        assertThrows( DifferentReferenceTestedSizeException.class, () -> new RMSEAndCoverageComputation<>( ref, 0, tested ) );
    }


    @SuppressWarnings( "unchecked" )
    @DisplayName( "Two identical image has an RMSE equals to 0 " )
    @Test
    < T extends RealType< T > & NativeType< T >, R extends RealType< R > & NativeType< R > >
    void whenTheRefHMAndTheTestedHMAreIdenticalTheRMSEEqualsZero() throws Exception
    {
        final String imagePath = "doc/fakeHeightMap.tif";
        final SCIFIOImgPlus< ? > imgPlus = IO.openAll( imagePath ).get( 0 );

        final Img< T > ref = ( Img< T > ) imgPlus.getImg();
        final Img< R > tested = ( Img< R > ) imgPlus.getImg();
        RMSEAndCoverageComputation< T, R > determination = new RMSEAndCoverageComputation<>( ref, 0, tested );
        RMSEAndCoverageComputation.setSubtract( 0 );
        determination.compute();
        assertThat( determination.getRMSE() == 0 ).isTrue();

    }

    @SuppressWarnings( "unchecked" )
    @DisplayName( "Two identical image has an reconstruction proportion equals to 1 " )
    @Test
    < T extends RealType< T > & NativeType< T >, R extends RealType< R > & NativeType< R > >
    void whenTheRefHMAndTheTestedHMAreIdenticalTheReconstructionProportionEqualsOne() throws Exception
    {
        final String imagePath = "doc/fakeHeightMap.tif";
        final SCIFIOImgPlus< ? > imgPlus = IO.openAll( imagePath ).get( 0 );

        final Img< T > ref = ( Img< T > ) imgPlus.getImg();
        final Img< R > tested = ( Img< R > ) imgPlus.getImg();
        RMSEAndCoverageComputation< T, R > determination = new RMSEAndCoverageComputation<>( ref, 0, tested );
        determination.compute();
        Assertions.assertEquals( determination.getCoverage(), 1 );
    }
}
