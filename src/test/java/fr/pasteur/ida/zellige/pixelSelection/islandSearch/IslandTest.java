/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.pixelSelection.islandSearch;

import fr.pasteur.ida.zellige.steps.selection.postTreatment.islandSearch.Island;
import fr.pasteur.ida.zellige.steps.selection.postTreatment.islandSearch.Sand;
import fr.pasteur.ida.zellige.steps.selection.postTreatment.islandSearch.World;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class IslandTest
{

    @Test
    void addAll()
    {
        World world = new World( 10, 5, 3, 6, 4);
        world.setSand( 1, 1, 0 );
        world.setSand( 0, 1, 0 );
        world.setSand( 2, 1, 0 );
        world.setSand( 1, 0, 0 );
        world.setSand( 1, 2, 0 );
        Island island = world.getFourConnected( world.getSand( 1, 1, 0 ) );
        ArrayList< Sand > list = new ArrayList<>();
        list.add( new Sand( 0, 1, 0 ) );
        list.add( new Sand( 2, 1, 0 ) );
        int size = island.size();
        island.addAll( list );
        assertEquals( size, island.size() );
    }

    @Test
    void add()
    {
        World world = new World( 10, 5, 3, 6, 4);
        world.setSand( 1, 1, 0 );
        world.setSand( 0, 1, 0 );
        world.setSand( 2, 1, 0 );
        world.setSand( 1, 0, 0 );
        world.setSand( 1, 2, 0 );
        Island island = world.getFourConnected( world.getSand( 1, 1, 0 ) );
        assertTrue( island.add( world.getSand( 1, 1, 0 ) ) );
        assertFalse( island.add( world.getSand( 1, 1, 0 ) ) );
    }

    @Test
    void reset()
    {
        Island island = new Island();
        island.add( new Sand( 1, 1, 0 ) );
        island.add( new Sand( 0, 1, 0 ) );
        island.add( new Sand( 2, 1, 0 ) );
        island.add( new Sand( 1, 0, 0 ) );
        island.add( new Sand( 1, 2, 0 ) );
        assertTrue( island.get( 0 ).isNotVisited() );
        island.get( 0 ).setNotVisited( false );
        assertFalse( island.get( 0 ).isNotVisited() );
        island.reset();
        assertTrue( island.get( 0 ).isNotVisited() );
    }
}
