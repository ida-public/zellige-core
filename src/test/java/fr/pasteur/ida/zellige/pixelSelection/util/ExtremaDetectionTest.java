/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.pixelSelection.util;

import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;
import fr.pasteur.ida.zellige.steps.selection.util.ExtremaDetection;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.numeric.integer.IntType;
import net.imglib2.type.numeric.real.FloatType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;

@SuppressWarnings( { "unchecked", "rawtypes" } )
public class ExtremaDetectionTest
{

    @DisplayName( "The input is a 2D or 3D image" )
    @Test
    void wrongInputWhenProvidedRaisesAnException()
    {
        Img< IntType > input = new ArrayImgFactory<>( new IntType() ).create( 5, 5, 5, 5 );
        final ImgFactory< IntType > factory = input.factory();
        String type = "max";
        DataValidationException exception =
                assertThrows( DataValidationException.class,
                        () -> new ExtremaDetection<>( input, factory, type ) );
        assertThat( exception ).hasMessageContaining( "3D image" );
    }


    @DisplayName( "The input and the factory are the same type" )
    @Test
    void differentTypeBetweenInputAndFactoryWhenProvidedRaisesAnException()
    {
        Img< IntType > input = new ArrayImgFactory<>( new IntType() ).create( 5, 5, 5 );
        ImgFactory< FloatType > factory = new ArrayImgFactory<>( new FloatType() );
        String type = "max";
        DataValidationException exception =
                assertThrows( DataValidationException.class,
                        () -> new ExtremaDetection( input, factory, type ) );
        assertThat( exception ).hasMessageContaining( "type" );
    }

    @DisplayName( "The type parameter is 'min' or 'max'" )
    @Test
    void wrongTypeWhenProvidedRaisesAnException()
    {
        Img< IntType > input = new ArrayImgFactory<>( new IntType() ).create( 5, 5, 5 );
        ImgFactory< IntType > factory = input.factory();
        String type = "mean";
        DataValidationException exception =
                assertThrows( DataValidationException.class,
                        () -> new ExtremaDetection<>( input, factory, type ) );
        assertThat( exception ).hasMessageContaining( "min" );
    }

    @DisplayName( "A good set of parameters don't raise any exception" )
    @Test
    void rightParametersWhenProvidedDoNotRaiseAnException()
    {
        Img< IntType > input = new ArrayImgFactory<>( new IntType() ).create( 5, 5, 5 );
        ImgFactory< IntType > factory = input.factory();
        String type = "min";
        assertDoesNotThrow( () -> new ExtremaDetection<>( input, factory, type ) );
    }

    @DisplayName( "The method 'compute extrema' is executed the right number of times" )
    @Test
    void anImageWithXDimensionEqualsTo20whenProcessExecute20TimesComputeExtremaMethod() throws DataValidationException
    {
        Img< IntType > input = new ArrayImgFactory<>( new IntType() ).create( 20, 5, 5 );
        ImgFactory< IntType > factory = input.factory();
        String type = "min";
        ExtremaDetection< IntType > detection3D = spy( new ExtremaDetection<>( input, factory, type ) );
        detection3D.run();
        Mockito.verify( detection3D, Mockito.times( 20 ) ).
                computeExtrema( any( RandomAccessibleInterval.class ), any( RandomAccessibleInterval.class ) );
    }
}
