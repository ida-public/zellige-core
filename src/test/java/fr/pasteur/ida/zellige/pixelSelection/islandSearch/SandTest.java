/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.pixelSelection.islandSearch;

import fr.pasteur.ida.zellige.steps.selection.postTreatment.islandSearch.Sand;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SandTest
{


    @Test
    void setIslandStatus()
    {
        Sand sand = new Sand( 0, 0, 0 );
        sand.setIslandStatus( -1 );
        assertEquals( -1, sand.getIslandStatus() );
    }

    @Test
    void getX()
    {
        Sand sand = new Sand( 0, 0, 0 );
        assertEquals( sand.getX(), 0 );
    }

    @Test
    void getY()
    {
        Sand sand = new Sand( 0, 0, 0 );
        assertEquals( sand.getY(), 0 );
    }

    @Test
    void getZ()
    {
        Sand sand = new Sand( 0, 0, 0 );
        assertEquals( sand.getZ(), 0 );
    }

    @Test
    void isNotVisited()
    {
        Sand sand = new Sand( 0, 0, 0 );
        assertTrue( sand.isNotVisited() );
    }

    @Test
    void setVisited()
    {
        Sand sand = new Sand( 0, 0, 0 );
        sand.setNotVisited( false);
        assertFalse( sand.isNotVisited());
    }

    @Test
    void testEquals()
    {
        Sand sand = new Sand( 0, 0, 0 );
        Sand sand2 = new Sand( 0, 0, 0 );
        assertEquals( sand, sand2 );
    }
}
