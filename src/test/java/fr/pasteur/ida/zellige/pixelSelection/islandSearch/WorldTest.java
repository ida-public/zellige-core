/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.pixelSelection.islandSearch;

import fr.pasteur.ida.zellige.steps.selection.postTreatment.islandSearch.Sand;
import fr.pasteur.ida.zellige.steps.selection.postTreatment.islandSearch.World;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WorldTest
{

    @Test
    void run()
    {

        World world = new World( 10, 5, 3, 6, 4);


        world.setSand( 1, 1, 0 );
        world.setSand( 0, 1, 0 );
        world.setSand( 2, 1, 0 );
        world.setSand( 1, 0, 0 );
        world.setSand( 1, 2, 0 );
        world.setSand( 8, 0, 0 );
        world.setSand( 6, 1, 0 );
        world.setSand( 7, 1, 0 );
        world.setSand( 8, 1, 0 );
        world.setSand( 9, 0, 0 );
        world.setSand( 8, 2, 0 );


        world.setSand( 0, 0, 1 );
        world.setSand( 0, 2, 1 );
        world.setSand( 2, 0, 1 );
        world.setSand( 2, 2, 1 );


    }

    @Test
    void setIslandStatus()
    {
        World world = new World( 10, 5, 3, 6,4 );


        world.setSand( 1, 1, 0 );
        world.setSand( 0, 1, 0 );
        world.setSand( 2, 1, 0 );
        world.setSand( 1, 0, 0 );
        world.setSand( 1, 2, 0 );

        world.setSand( 8, 0, 0 );
        world.setSand( 6, 1, 0 );
        world.setSand( 7, 1, 0 );
        world.setSand( 8, 1, 0 );
        world.setSand( 9, 0, 0 );
        world.setSand( 8, 2, 0 );


        world.setSand(0, 0, 1 ) ;
        world.setSand(0, 2, 1 ) ;
        world.setSand(2, 0, 1 ) ;
        world.setSand(2, 2, 1 ) ;


        Sand s1 = world.getSand( 1, 1, 0  );
        world.setIslandStatus( s1 );

        Sand s2 = world.getSand(  1, 2, 0  );
        world.setIslandStatus( s2 );

        Sand s3 = world.getSand(0, 1, 0  );
        Sand s4 = world.getSand(8, 0, 0  );
        Sand s5 = world.getSand(0, 2, 1 ) ;
        Sand s6 = world.getSand(0, 0, 1 ) ;
        assertEquals( s3.getIslandStatus(), 1 );


    }


}
