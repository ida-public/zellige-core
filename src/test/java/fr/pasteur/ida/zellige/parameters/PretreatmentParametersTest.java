/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.parameters;

import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;
import fr.pasteur.ida.zellige.steps.selection.pretreatment.PretreatmentParameters;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static fr.pasteur.ida.zellige.steps.selection.pretreatment.PretreatmentParameters.GAUSSIAN_BLUR;
import static fr.pasteur.ida.zellige.steps.selection.pretreatment.PretreatmentParameters.NOT_IMPLEMENTED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PretreatmentParametersTest
{

    @DisplayName( "Any method associated with the right number of parameters should not raise any exception" )
    @ParameterizedTest
    @CsvSource( { GAUSSIAN_BLUR + ", 1" } )
    void rightMethodWithRightParameterValue_whenChecked_doesntRaiseAnyException( String method, double value )
    {
        assertDoesNotThrow( () -> new PretreatmentParameters( method, value ) );
    }

    @DisplayName( "A wrong method should raise DataValidationException for any method" )
    @Test
    void wrongDenoisingMethod_whenChecked_RaisesADataValidationException()
    {
        DataValidationException exception = assertThrows( DataValidationException.class, () ->
                new PretreatmentParameters( "Wrong method", 15 ) );
        assertThat( exception ).hasMessageContaining( NOT_IMPLEMENTED );
    }

    @DisplayName( "A parameter value inferior to zero  should raise a DataValidationException for any method" )
    @ParameterizedTest
    @ValueSource( strings = { GAUSSIAN_BLUR } )
    void gaussianBlurMethodWithLessThanThreeParameter_whenChecked_RaisesAnException( String method )
    {
        double value = - 1;
        DataValidationException exception = assertThrows( DataValidationException.class, () -> new PretreatmentParameters( method, value ) );
        assertThat( exception ).hasMessageContaining( "parameter value" );
    }

    @DisplayName( "A  parameter value superior to 10  should raise a DataValidationException for any method" )
    @Test
    void parameterValueSuperiorTo10_whenChecked_RaisesADataValidationException()
    {
        DataValidationException exception = assertThrows( DataValidationException.class, () ->
                new PretreatmentParameters( GAUSSIAN_BLUR, 15 ) );
        assertThat( exception ).hasMessageContaining( "parameter value" );
    }

}
