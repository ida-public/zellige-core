/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.parameters;

import fr.pasteur.ida.zellige.steps.selection.classification.ClassificationParameters;
import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ClassificationParametersTest
{
    @DisplayName( "A good set of parameters don't raise any exception" )
    @Test
    void rightPixelClassificationParameters_whenCreatingANewInstance_DonTRaiseAValidationDataException()
    {
        int amplitudeThreshold = 5;
        int otsuThreshold = 5;
        assertDoesNotThrow( () -> new ClassificationParameters( amplitudeThreshold, otsuThreshold ) );
    }


    @DisplayName( " An amplitude threshold inferior to zero raises a ValidationDataException with a specific message" )
    @Test
    void amplitudeThresholdUnderZero_WhenCreatingANewInstance_raisesAValidationDataException()
    {
        int amplitudeThreshold = - 1;
        int otsuThreshold = 2;
        DataValidationException exception = assertThrows( DataValidationException.class, () ->
                new ClassificationParameters( amplitudeThreshold, otsuThreshold ) );
        assertThat( exception ).hasMessageContaining( "Amplitude" );
    }

    @DisplayName( " An otsu threshold inferior to zero raises a ValidationDataException with a specific message " )
    @Test
    void otsuThresholdUnderZero_WhenCreatingANewInstance_raisesAValidationDataException()
    {
        int amplitudeThreshold = 1;
        int otsuThreshold = - 1;
        DataValidationException exception = assertThrows( DataValidationException.class, () ->
                new ClassificationParameters( amplitudeThreshold, otsuThreshold ) );
        assertThat( exception ).hasMessageContaining( "Otsu" );
    }


}
