/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.behavior;

import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.element.Surface;
import fr.pasteur.ida.zellige.element.ose.AbstractOSE;
import fr.pasteur.ida.zellige.element.ose.OSEList;
import fr.pasteur.ida.zellige.element.ose.OSEListArray;
import fr.pasteur.ida.zellige.element.surfaceLine.SurfaceLine;
import fr.pasteur.ida.zellige.steps.construction.rounds.ose.OseConstructionXZ;
import fr.pasteur.ida.zellige.steps.construction.rounds.surface.SurfaceConstruction;
import fr.pasteur.ida.zellige.steps.selection.Selection;
import fr.pasteur.ida.zellige.steps.selection.classification.ClassificationParameters;
import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;
import fr.pasteur.ida.zellige.steps.selection.exception.NoClassificationException;
import fr.pasteur.ida.zellige.steps.selection.postTreatment.PostTreatmentParameters;
import fr.pasteur.ida.zellige.steps.selection.pretreatment.PretreatmentParameters;
import io.scif.img.IO;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import org.junit.Ignore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.TreeMap;

public class StartingOSESameSurfaceTest
{
    private final static Logger LOGGER = LoggerFactory.getLogger( StartingOSESameSurfaceTest.class );

    @Ignore
    @DisplayName( "Two starting OSE from the same surface don't necessarily produce the exact same surface" )
    @Test
    < T extends RealType< T > & NativeType< T > > void TwoStartingOSEFromTheSameSurface_whenUseForStartASurface_DonTProduceTheExactSameSurface() throws DataValidationException, NoClassificationException
    {
        /* Parameters*/
        int amplitude = 5;
        int otsu = 5;
        double sigmaXY = 2;
        double sigmaZ = 0;
        double startingSizeThreshold = 0.9;
        int overlap = 10;
        double connexity = 0.5;
        PretreatmentParameters pretreatmentParameters = new PretreatmentParameters( "GaussianBlur", 2 );
        ClassificationParameters classificationParameters = new ClassificationParameters( amplitude, otsu );
        PostTreatmentParameters postTreatmentParameters = new PostTreatmentParameters( sigmaXY, sigmaZ, 5, 8 );

        final String imagePath = "doc/Phantom.tif";
        final SCIFIOImgPlus< ? > imgPlus = IO.openAll( imagePath ).get( 0 );
        @SuppressWarnings( "unchecked" )
        final Img< T > source = ( Img< T > ) imgPlus.getImg();

        Pixels[][] maximums = Selection.run( source, pretreatmentParameters, classificationParameters, postTreatmentParameters );
        OSEListArray oseLists = OseConstructionXZ.run( maximums, startingSizeThreshold );
        int width = maximums[ 0 ].length;

        ArrayList< AbstractOSE > osePartOfSurface = findAllStartingOSE( oseLists );
        AbstractOSE ose1 = oseLists.getAStartingOse();
        Surface reference = SurfaceConstruction.run( oseLists, ose1, width, overlap, connexity );

        ArrayList< AbstractOSE > list2 = findAllStartingOSE( oseLists );
        osePartOfSurface.removeAll( list2 );
        LOGGER.debug( "surface starting ose : " + osePartOfSurface.size() );
        TreeMap< Double, Integer > uniqueSurface = new TreeMap<>();
        for ( AbstractOSE ose : osePartOfSurface )
        {
            oseLists.reset();
            Surface s = SurfaceConstruction.run( oseLists, ose, width, overlap, connexity );
            double overlappingRate = overlappingRate( reference, s );
            LOGGER.debug( " This surface has an overlap rate of " + overlappingRate + " with the reference surface" );
            Integer j = uniqueSurface.get(overlappingRate);
            uniqueSurface.put( overlappingRate, ( j == null ) ? 1 : j + 1 );
        }
        LOGGER.debug( String.valueOf( uniqueSurface ) );
        Assertions.assertTrue( uniqueSurface.size() > 1 );
    }


    public ArrayList<AbstractOSE> findAllStartingOSE (OSEListArray oseListArray)
    {
        ArrayList<AbstractOSE> list = new ArrayList<>();
        for ( OSEList oseList : oseListArray.getOseLists()  )
        {
            for ( AbstractOSE ose :oseList  )
            {
                if(ose.isAStart())
                {
                    list.add(ose);
                }
            }
        }
        return list;
    }

    public double overlappingRate( Surface reference, Surface other )
    {
        int inCommon = 0;
        int count = 0;
        for ( int i = 0; i <= reference.getHeight() - 1; i++ )
        {
            SurfaceLine refLine = reference.get( i );
            if ( refLine != null )
            {
                for ( int j = 0; j < refLine.getLength(); j++ )
                {
                    Pixels refPixels = refLine.get( j );
                    if ( refPixels != null )
                    {
                        count++;
                        if ( refPixels.equals (other.get( i ).get( j ) ))
                        {
                            inCommon++;
                        }
                    }
                }

            }
        }
        return  inCommon / (double) count * 100.0;
    }
}
