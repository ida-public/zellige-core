/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.behavior;

import fr.pasteur.ida.zellige.utils.test.*;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.UnsignedShortType;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;

import static fr.pasteur.ida.zellige.behavior.Utils.buildStringLine;


@SuppressWarnings( { "unchecked", "rawtypes" } )
public class ParameterSweepPhantomTest< T extends RealType< T > & NativeType< T >, R extends RealType< R > & NativeType< R > >
{
    private final static Logger LOGGER = LoggerFactory.getLogger( ParameterSweepPhantomTest.class );
    private static final String[] header = { "Parameter1", "Parameter1Value",
            "GroundTruthNb", "SelectedPixelNb", "RMSE", "Coverage", "SelectionPT",
            "AmplitudePT", "OtsuPT", "IslandSearchPT",
            "ConstructionPT", "FS_OS_PT", "FS_surface_PT", "SS_OS_PT", "SS_surface_PT", "Total_PT",
            "FS_OS_count", "FS_startingOS_count", "SS_OS_count", "SS_startingOS_count",
            "FS_good", "FS_small", "FS_finalised",
            "SS_good", "SS_small", "SS_finalised", "OptimizedSet" };
    public final String[] errorLine =
            new String[]{
                    "NaN", "NaN", "NaN", "NaN", "NaN", "NaN", "NaN", "NaN", "NaN", "NaN", "NaN", "NaN",
                    "NaN", "NaN", "NaN", "NaN", "NaN", "NaN", "NaN", "NaN", "NaN", "NaN", "NaN", "NaN" };
    /* Specific Parameters */
    private static final String sample = "Phantom";
    private static final String imagePath = "doc/Phantom.tif";
    private static final String[] refImagePath = new String[]{
            "src/test/resources/Phantom/phantoms_snrtest_zmap1_gt.tif",
            "src/test/resources/Phantom/phantoms_snrtest_zmap2_gt.tif",
            "src/test/resources/Phantom/phantoms_snrtest_zmap3_gt.tif"
    };
    static ArrayList< Img > ref;
    static Img tested;
    private static CSVWriter writer;
    private final int delta = 0;
    private final String parameterSweepCSVFile = "C:\\Users\\ctrebeau\\Desktop\\Zellige analysis\\Paper_012022\\ProcessingTime\\Phantom_PT.csv";
    private final double surfaceMinSizeFactor = 0.8;

    //    @Ignore
    @BeforeAll
    static < T extends RealType< T > & NativeType< T >, R extends RealType< R > & NativeType< R > > void init() throws IOException
    {
        ParameterSweepFiles files = new ParameterSweepFiles( refImagePath, imagePath, "PT_" + sample );
        ref = files.getRefs();
        tested = files.getTested();
        writer = new CSVWriter( files.getPath() );
        writer.writeToFile( header );
        RMSEAndCoverageComputation.setSubtract( 0 );
        LOGGER.info( "Parameter sweep for the {} sample", sample );
        LOGGER.debug( " The parameter sweep is done for a total of {} ground truths", ref.size() );
    }

    @AfterAll
    static void closeBw() throws IOException
    {
        writer.close();
    }

    @ParameterizedTest
    @CsvFileSource( files = parameterSweepCSVFile, numLinesToSkip = 1, delimiterString = ";" )
    void parameterSweep( String testedParameter, String testedParameterValue, String filter, int filterParameter,
                         int amplitudeThreshold, int otsuThreshold,
                         int ISConnexity, int ISSize,
                         double XYSmoothing, double ZSmoothing,
                         double startingThreshold1, int overlap1, double connexityRate1,
                         double startingThreshold2, int overlap2, double connexityRate2,
                         String optimalSet ) throws Exception
    {

        try
        {
            ZParameters parameters = new ZParameters( filter, filterParameter,
                    amplitudeThreshold, otsuThreshold, ISConnexity, ISSize, XYSmoothing, ZSmoothing,
                    startingThreshold1, overlap1, connexityRate1,
                    startingThreshold2, overlap2, connexityRate2, surfaceMinSizeFactor, delta );

            parameters.print();
            ZelligePipelineAnalyse< T, R > analyse = new ZelligePipelineAnalyse<>();
            analyse.run( parameters, tested, ref );
            for ( RMSEAndCoverageComputation< T, UnsignedShortType > r : analyse.getErrorList() )
            {
                String[] line = buildStringLine( testedParameter, testedParameterValue, analyse.getErrorAnalysis( r ), optimalSet );
                writer.writeToFile( line );
            }
        }
        catch ( Exception exception )
        {
            for ( int i = 0; i < ref.size(); i++ )
            {
                writer.writeToFile( errorLine );
            }
        }
    }
}
