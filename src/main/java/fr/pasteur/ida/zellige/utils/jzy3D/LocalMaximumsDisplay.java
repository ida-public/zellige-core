/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.jzy3D;


import fr.pasteur.ida.zellige.element.Coordinate;
import fr.pasteur.ida.zellige.element.Pixels;
import org.jzy3d.analysis.AbstractAnalysis;
import org.jzy3d.analysis.AnalysisLauncher;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LocalMaximumsDisplay extends AbstractAnalysis
{
    private final static Logger LOGGER = LoggerFactory.getLogger( LocalMaximumsDisplay.class );
    private final Pixels[][] maximumCoordinates;

    public LocalMaximumsDisplay( Pixels[][] localMaximums )
    {
        this.maximumCoordinates = localMaximums;
        init();
    }

    /**
     * Displays the local maximums found using jzy3D package.
     *
     * @param maximums a 2D {@link Pixels} array
     */
    public static void displayMaximums( Pixels[][] maximums )
    {
        LOGGER.debug( "3D maximums display" );
//        if ( ! GraphicsEnvironment.isHeadless() )
        {
            try
            {
                LocalMaximumsDisplay localMaximumsDisplay = new LocalMaximumsDisplay( maximums );
                AnalysisLauncher.open( localMaximumsDisplay );
            }
            catch ( Exception e )
            {
                e.printStackTrace();
            }
        }
//        else
    }

    @Override
    public void init()
    {
        if ( maximumCoordinates != null )
        {
            int length = getNumberOfCoordinates();
            Coord3d[] points = new Coord3d[ length ];
            Color[] colors = new Color[ length ];

            int x;
            int y;
            int z;
            int index = 0;
            for ( int i = 0; i <= this.maximumCoordinates.length - 1; i++ )
            {
                for ( int j = 0; j <= this.maximumCoordinates[ 0 ].length - 1; j++ )
                {
//
                    Pixels pixels = maximumCoordinates[ i ][ j ];
                    if ( pixels != null )
                    {

                        if ( pixels.size() == 1 )
                        {
                            Coordinate pixel = pixels.get( 0 );
                            x = pixel.getX();
                            y = pixel.getY();
                            z = pixel.getZ();
                            points[ index ] = new Coord3d( x, y, z );
                            colors[ index ] = Color.BLACK;
                            index++;
                        }
                        else
                        {
                            for ( Coordinate coordinate : pixels.get() )
                            {
                                z = coordinate.getZ();
                                points[ index ] = new Coord3d( coordinate.getX(), coordinate.getY(), z );
                                colors[ index ] = Color.RED;
//                                .LOGGER.debug( points[index]);
                                index++;
                            }
                        }

                    }
                }
            }

            Scatter scatter = new Scatter( points, colors );
            scatter.setWidth( 2 );
            chart = AWTChartComponentFactory.chart( Quality.Advanced, "awt" );
            chart.getScene().add( scatter );
        }
    }

    private int getNumberOfCoordinates()
    {
        int n = 0;
        for ( int i = 0; i <= maximumCoordinates[ 0 ].length - 1; i++ )
        {
            for ( int j = 0; j <= maximumCoordinates.length - 1; j++ )
            {
                if ( maximumCoordinates[ j ][ i ] != null )
                {
                    n += maximumCoordinates[ j ][ i ].size();
                }
            }
        }
        return n;
    }
}
