/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.test;

import io.scif.img.IO;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class ParameterSweepFiles< T extends RealType< T > & NativeType< T >, R extends RealType< R > & NativeType< R > >
{
    private final static Logger LOGGER = LoggerFactory.getLogger( ParameterSweepFiles.class );
    private final ArrayList< Img< T > > refs;
    private final Img< R > tested;
    private final String path;

    @SuppressWarnings( "unchecked" )
    public ParameterSweepFiles( final String[] refImagePath, final String imagePath, String destinationPath )
    {
        /* The reference height maps*/
        this.refs = new ArrayList<>();
        for ( String path : refImagePath )
        {
            LOGGER.debug( path );
            SCIFIOImgPlus< ? > refImgPlus = IO.openAll( path ).get( 0 );
            refs.add( ( Img< T > ) refImgPlus.getImg() );
        }

        /* The raw image to test*/
        final SCIFIOImgPlus< ? > imgPlus = IO.openAll( imagePath ).get( 0 );
        tested = ( Img< R > ) imgPlus.getImg();

        this.path = destinationPath;
    }

    public ArrayList< Img< T > > getRefs()
    {
        return refs;
    }

    public Img< R > getTested()
    {
        return tested;
    }

    public String getPath()
    {
        return path;
    }
}
