/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.jzy3D;

import fr.pasteur.ida.zellige.utils.Util;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import org.jzy3d.analysis.AbstractAnalysis;
import org.jzy3d.analysis.AnalysisLauncher;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.rendering.canvas.Quality;

import static fr.pasteur.ida.zellige.steps.Utils.setPositionAndGet;

public class HM_GT_Display< T extends RealType< T > & NativeType< T >, R extends RealType< R > & NativeType< R > > extends AbstractAnalysis
{
    private final RandomAccessibleInterval< T > groundTruth;
    private final RandomAccessibleInterval< R > projection;

    public HM_GT_Display( RandomAccessibleInterval< T > groundTruth,
                          RandomAccessibleInterval< R > heightMap )
    {
        this.groundTruth = groundTruth;
        this.projection = heightMap;
        init();
    }

    /**
     * Displays the local maximums found using jzy3D package.
     *
     * @param heightMap   the height map to tested as a {@link RandomAccessibleInterval}
     * @param groundTruth the ground truth to test against
     * @param <T>         the height map type
     * @param <R>         the ground truth type
     */
    public static < T extends RealType< T > & NativeType< T >, R extends RealType< R > & NativeType< R > > void displayDiff( RandomAccessibleInterval< T > groundTruth,
                                                                                                                             RandomAccessibleInterval< R > heightMap )
    {
        {
            try
            {
                HM_GT_Display< T, R > display = new HM_GT_Display<>( groundTruth, heightMap );
                AnalysisLauncher.open( display );
            }
            catch ( Exception e )
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void init()
    {
        float[][] GT = Util.convert( groundTruth, 0 );
        float[][] HM = Util.convert( projection, 0 );
        int count = getCount( GT, HM );
        Coord3d[] points = new Coord3d[ count ];
        Color[] colors = new Color[ count ];
        int index = 0;
        for ( int x = 0; x < GT[ 0 ].length; x++ )
        {
            for ( int y = 0; y < GT.length; y++ )
            {
                float GTValue = GT[ y ][ x ];
                float HMValue = HM[ y ][ x ];
                if ( GTValue != HMValue - 1 )
                {
                    if ( GTValue != 0 )
                    {
                        points[ index ] = new Coord3d( x, y, GTValue );
                        colors[ index ] = Color.RED;
                        index++;
                    }
                    if ( HMValue != 0 )
                    {
                        points[ index ] = new Coord3d( x, y, HMValue - 1 );
                        colors[ index ] = Color.BLACK;
                        index++;
                    }
                }
                else if ( GTValue != 0 )
                {
                    points[ index ] = new Coord3d( x, y, GTValue );
                    colors[ index ] = Color.BLUE;
                    index++;
                }
            }
        }
        Scatter scatter = new Scatter( points, colors );
        scatter.setWidth( 1 );
        chart = AWTChartComponentFactory.chart( Quality.Advanced, "newt" );
        chart.getScene().add( scatter );
    }

    public < S extends RealType< S > & NativeType< S > > float[][] convert( RandomAccessibleInterval< S > input, int subtract )
    {
        float[][] array = new float[ ( int ) input.dimension( 1 ) ][ ( int ) input.dimension( 0 ) ];
        RandomAccess< S > access = input.randomAccess();
        for ( int x = 0; x < ( int ) input.dimension( 0 ); x++ )
        {
            for ( int y = 0; y < ( int ) input.dimension( 0 ); y++ )
            {
                float value = setPositionAndGet( access, x, y ).getRealFloat();
                if ( value + subtract != 0 )
                {
                    array[ y ][ x ] = value;
                }
            }
        }
        return array;
    }

    public int getCount( float[][] GT, float[][] HM )
    {
        int count = 0;
        for ( int x = 0; x < GT[ 0 ].length; x++ )
        {
            for ( int y = 0; y < GT.length; y++ )
            {
                float GTValue = GT[ y ][ x ];
                float HMValue = HM[ y ][ x ];

                if ( GTValue != HMValue - 1 )
                {
//                    LOGGER.debug( GTValue + " / " + HMValue );
                    if ( GTValue != 0 )
                    {
                        count++;
                    }
                    if ( HMValue != 0 )
                    {
                        count++;
                    }
                }
                else if ( GTValue != 0 )
                {
//                    LOGGER.debug( GTValue + " / " + HMValue );
                    count++;
                }
            }
        }
        return count;
    }

}
