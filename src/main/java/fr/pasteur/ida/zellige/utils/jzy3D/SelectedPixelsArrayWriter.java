/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.jzy3D;

import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.utils.test.CSVWriter;
import org.jzy3d.analysis.AbstractAnalysis;

import java.io.IOException;

public class SelectedPixelsArrayWriter extends AbstractAnalysis
{

    private final Pixels[][] selectedPixels;
    private final String originalInput;

    public SelectedPixelsArrayWriter( Pixels[][] selectedPixels, String originalInput ) throws IOException
    {

        this.selectedPixels = selectedPixels;
        this.originalInput = originalInput;
        init();
    }


    public static void writeSelectedPixels( Pixels[][] selectedPixels, String originalInput )
    {
        try
        {
            new SelectedPixelsArrayWriter( selectedPixels, originalInput );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }

    @Override
    public void init() throws IOException
    {
        if ( selectedPixels != null )
        {

            CSVWriter csvWriter =
                    new CSVWriter( originalInput + "_selectedPixels" );
            csvWriter.writeToFile( new String[]{ "x", "y", "z" } );
            for ( int x = 0; x < selectedPixels.length; x++ )
            {
                for ( int y = 0; y < selectedPixels[ 0 ].length; y++ )
                {
                    for ( int z = 0; z < selectedPixels[ y ][ x ].size(); z++ )
                    {
                        Pixels pixel = selectedPixels[ y ][ x ];
                        csvWriter.writeToFile(
                                new String[]{ String.valueOf( x ),
                                        String.valueOf( y ),
                                        String.valueOf( pixel.get( z ).getZ() ) } );
                    }
                }
            }
        }
    }


}
