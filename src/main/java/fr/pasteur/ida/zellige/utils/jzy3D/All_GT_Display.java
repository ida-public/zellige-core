/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.jzy3D;

import fr.pasteur.ida.zellige.utils.Util;
import io.scif.img.IO;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import org.jzy3d.analysis.AbstractAnalysis;
import org.jzy3d.analysis.AnalysisLauncher;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class All_GT_Display< T extends RealType< T > & NativeType< T > > extends AbstractAnalysis
{
    private final static Logger LOGGER = LoggerFactory.getLogger( All_GT_Display.class );
    private final ArrayList< RandomAccessibleInterval< T > > groundTruths;
    private final String[] gtColors = { "#283D66", "#A1B9EA", "#5A88E6", "#465166", "#466AB3", "", "", "" };

    public All_GT_Display( ArrayList< RandomAccessibleInterval< T > > groundTruths )
    {
        this.groundTruths = groundTruths;
        init();
    }

    @SuppressWarnings( "unchecked" )
    public static < T extends RealType< T > & NativeType< T > > void main( String[] args )
    {

        final String[] refImagePath = new String[]{
//                "src/test/resources/Mouche_c01_f0001_p013/Ground truth Height map_1.tif",
                "C:\\Users\\ctrebeau\\Desktop\\Zellige analysis\\files\\Mouche_c01_f0001_p013\\GT\\Ground truth Height map_1.tif",
                "C:\\Users\\ctrebeau\\Desktop\\Zellige analysis\\files\\Mouche_c01_f0001_p013\\GT\\Ground truth Height map_2.tif",
                "C:\\Users\\ctrebeau\\Desktop\\Zellige analysis\\files\\Mouche_c01_f0001_p013\\GT\\Ground truth Height map_3.tif",
                "C:\\Users\\ctrebeau\\Desktop\\Zellige analysis\\files\\Mouche_c01_f0001_p013\\GT\\Ground truth Height map_4.tif"

        };

        ArrayList< RandomAccessibleInterval< T > > GTs = new ArrayList<>();

        for ( String path : refImagePath )
        {
            LOGGER.debug( path );
            SCIFIOImgPlus< ? > refImgPlus = IO.openAll( path ).get( 0 );
            GTs.add( ( Img< T > ) refImgPlus.getImg() );
        }
        All_GT_Display.display( GTs );
    }

    /**
     * Displays the local maximums found using jzy3D package.
     *
     * @param groundTruths the image input as a {@link RandomAccessibleInterval}
     * @param <T>          the {@link RandomAccessibleInterval} type
     */
    public static < T extends RealType< T > & NativeType< T > > void display( ArrayList< RandomAccessibleInterval< T > > groundTruths )
    {
        {
            try
            {
                All_GT_Display< T > display = new All_GT_Display<>( groundTruths );
                AnalysisLauncher.open( display );
            }
            catch ( Exception e )
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void init()
    {
        ArrayList< float[][] > GTs = new ArrayList<>();
        for ( RandomAccessibleInterval< T > GT : groundTruths )
        {
            GTs.add( Util.convert( GT, 0 ) );
        }
        int count = 0;
        for ( float[][] gt : GTs )
        {
            count += getCount( gt );
        }
        Coord3d[] points = new Coord3d[ count ];
        Color[] colors = new Color[ count ];
        int index = 0;
        int gtNb = 0;
        for ( float[][] gt : GTs )// for each GT
        {
            for ( int x = 0; x < gt[ 0 ].length; x++ )
            {
                for ( int y = 0; y < gt.length; y++ )
                {
                    float GTValue = gt[ y ][ x ];
                    if ( GTValue != 0 )
                    {
                        points[ index ] = new Coord3d( x, y, GTValue );
                        colors[ index ] = getColor( gtColors[ gtNb ] );
                        index++;
                    }
                }
            }
            gtNb++;
        }
        Scatter scatter = new Scatter( points, colors );
        scatter.setWidth( 1 );
        chart = AWTChartComponentFactory.chart( Quality.Advanced, "newt" );
        chart.getScene().add( scatter );
    }

    public int getCount( float[][] map )
    {
        int count = 0;
        for ( int x = 0; x < map[ 0 ].length; x++ )
        {
            for ( float[] floats : map )
            {
                float mapValue = floats[ x ];
                if ( mapValue != 0 )
                {
                    count++;
                }
            }
        }
        return count;
    }


    public int[] hexToRGB( final String hex )
    {
        final int[] RGB = new int[ 3 ];
        for ( int i = 0; i < 3; i++ )
        {
            String hex0 = hex.substring( 1 );
            RGB[ i ] = Integer.parseInt( hex0.substring( i * 2, i * 2 + 2 ), 16 );

        }
        return RGB;
    }

    public Color getColor( int[] RGB )
    {
        return new Color( RGB[ 0 ], RGB[ 1 ], RGB[ 2 ] );
    }

    public Color getColor( String hex )
    {
        return getColor( hexToRGB( hex ) );

    }


}
