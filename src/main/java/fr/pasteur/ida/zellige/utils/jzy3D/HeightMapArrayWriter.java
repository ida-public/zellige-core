/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.jzy3D;

import fr.pasteur.ida.zellige.utils.test.CSVWriter;
import io.scif.img.IO;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.Cursor;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import org.jzy3d.analysis.AbstractAnalysis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class HeightMapArrayWriter< T extends RealType< T > & NativeType< T > > extends AbstractAnalysis
{
    private final static Logger LOGGER = LoggerFactory.getLogger( HeightMapArrayWriter.class );
    private final Img< T > surface;
    private final String originalInput;
    private final int surfaceNumber;

    public HeightMapArrayWriter( Img< T > surface, String originalInput, int surfaceNumber ) throws IOException
    {

        this.surface = surface;
        this.originalInput = originalInput;
        this.surfaceNumber = surfaceNumber;
        init();
    }

    public static < T extends RealType< T > & NativeType< T > > void writeHeightMap( Img< T > surface, String originalInput, int surfaceNumber )
    {
        try
        {
            new HeightMapArrayWriter<>( surface, originalInput, surfaceNumber );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }


    @SuppressWarnings( "unchecked" )
    public static < T extends RealType< T > & NativeType< T > > void main( String[] args )
    {
        final String imagePath = "C:\\Users\\ctrebeau\\Desktop\\Zellige analysis\\Paper_012022\\Organoid\\Organoid_GT_HM1.tif"; /* The image path goes here !!!! */
        LOGGER.debug( imagePath );
        /* JY version for opening files. */
        final SCIFIOImgPlus< ? > imgPlus = IO.openAll( imagePath ).get( 0 );
        final Img< T > heightMap = ( Img< T > ) imgPlus.getImg();
        writeHeightMap( heightMap, imgPlus.getName(), 2 );


    }

    @Override
    public void init() throws IOException
    {
        if ( surface != null )
        {
            CSVWriter csvWriter =
                    new CSVWriter( originalInput + "_" + "surface " + surfaceNumber );
            Cursor< T > cursor = surface.cursor();
            csvWriter.writeToFile( new String[]{ "x", "y", "z" } );
            while ( cursor.hasNext() )
            {
                cursor.fwd();
                {
                    if ( cursor.get().getRealFloat() != 0 )
                    {
                        csvWriter.writeToFile(
                                new String[]{ String.valueOf( cursor.getIntPosition( 0 ) ),
                                        String.valueOf( cursor.getIntPosition( 1 ) ),
                                        String.valueOf( cursor.get().getRealFloat() ) } );
                    }
                }
            }

        }
    }


}
