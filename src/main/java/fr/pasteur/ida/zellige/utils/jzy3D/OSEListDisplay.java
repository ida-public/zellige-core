/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.jzy3D;

import fr.pasteur.ida.zellige.element.Coordinate;
import fr.pasteur.ida.zellige.element.ose.AbstractOSE;
import fr.pasteur.ida.zellige.element.ose.OSEList;
import fr.pasteur.ida.zellige.element.ose.OSEListArray;
import org.jzy3d.analysis.AbstractAnalysis;
import org.jzy3d.analysis.AnalysisLauncher;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class OSEListDisplay extends AbstractAnalysis
{

    private final static Logger LOGGER = LoggerFactory.getLogger( OSEListDisplay.class );
    private final OSEListArray oseLists;

    public OSEListDisplay( OSEListArray oseLists )
    {
        this.oseLists = oseLists;
        init();
    }

    /**
     * Displays the local maximums found using jzy3D package.
     *
     * @param oseLists the {@link OSEListArray} to display
     */
    public static void displayOSEList( OSEListArray oseLists )
    {
        {
            try
            {
                OSEListDisplay display = new OSEListDisplay( oseLists );
                AnalysisLauncher.open( display );
            }
            catch ( Exception e )
            {
                e.printStackTrace();
            }
        }
    }

    public void init()
    {
        int count = 0;
        for ( OSEList oseList : oseLists.getOseLists() )
        {
            for ( AbstractOSE os : oseList )
            {
                count = count + os.size();
            }
        }
        Coord3d[] points = new Coord3d[ count ];
        Color[] colors = new Color[ count ];
        LOGGER.debug( "count : " + count );
        Random r = new Random();
        r.setSeed( 50 );
        float x;
        float y;
        float z;

        int index = 0;
        for ( OSEList oseList : oseLists.getOseLists() )
        {

            for ( AbstractOSE os : oseList )
            {
                // one color per OS
                float G = r.nextFloat();
                float R = r.nextFloat();
                float B = r.nextFloat();
                Color color = new Color( G, R, B );
                for ( Coordinate coordinate : os )
                {
                    x = coordinate.getX();
                    y = coordinate.getY();
                    z = coordinate.getZ();
                    points[ index ] = new Coord3d( x, y, z );
//                    colors[index] = Color.BLACK;
                    colors[ index ] = new Color( 100, 100, z + 10 );
                    index++;
                }
            }
        }
            Scatter scatter = new Scatter(points, colors);
        scatter.setWidth( 1 );
            chart = AWTChartComponentFactory.chart(Quality.Advanced, "newt");
            chart.getScene().add(scatter);

    }
}
