/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.test;

import ij.ImageJ;
import io.scif.img.IO;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.img.Img;
import net.imglib2.img.display.imagej.ImageJFunctions;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import net.imglib2.type.numeric.real.FloatType;

public class GroundTruthConversion
{
    @SuppressWarnings( "unchecked" )
    public static void main( String[] args )
    {
        new ImageJ();
        final String refImagePath = "doc/Ground truth Height map.tif";
        final SCIFIOImgPlus< ? > refImgPlus = IO.openAll( refImagePath ).get( 0 );
        Img< FloatType > img = ( Img< FloatType > ) refImgPlus.getImg();
        ImageJFunctions.show( img, "Ground truth" );
        final String maskPath = "doc/STK_BG_ROI.tif";
        final SCIFIOImgPlus< ? > maskImgPlus = IO.openAll( maskPath ).get( 0 );
        Img< UnsignedByteType > mask = ( Img< UnsignedByteType > ) maskImgPlus.getImg();
        ImageJFunctions.show( mask, "BG mask" );
        Cursor< UnsignedByteType > cursor = mask.localizingCursor();
        RandomAccess< FloatType > randomAccess = img.randomAccess();

        while ( cursor.hasNext() )
        {
            cursor.fwd();
            if ( cursor.get().get() != 0 )
            {
                randomAccess.setPosition( cursor );
                randomAccess.get().set( 0 );
            }
        }
        ImageJFunctions.show( img.copy(), "GT without BK" );
    }
}
