/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.test;

import fr.pasteur.ida.zellige.ReferenceSurfaceExtraction;
import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.element.ReferenceSurface;
import fr.pasteur.ida.zellige.steps.construction.exception.NoSurfaceFoundException;
import fr.pasteur.ida.zellige.steps.selection.Selection;
import fr.pasteur.ida.zellige.utils.test.exception.NotAnHeightMapException;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.UnsignedShortType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class ZelligePipelineAnalyse< T extends RealType< T > & NativeType< T >, R extends RealType< R > & NativeType< R > >
{
    private final static Logger LOGGER = LoggerFactory.getLogger( ZelligePipelineAnalyse.class );
    private final ArrayList< RMSEAndCoverageComputation< T, UnsignedShortType > > errorList;
    private long processingTime;
    private long selectionProcessingTime;
    private long constructionProcessingTime;
    private int selectedPixelNumber;
    private ReferenceSurfaceExtraction< R > extraction;

    public ZelligePipelineAnalyse()
    {
        errorList = new ArrayList<>();
    }

    @SuppressWarnings( { "unchecked", "rawtypes" } )
    public void run( final ZParameters parameters, final Img< R > tested, final ArrayList< Img > GT ) throws Exception
    {
        long startTime = System.currentTimeMillis();
        ImgFactory< R > factory = tested.factory();

        extraction = new ReferenceSurfaceExtraction<>( tested, factory
        );
        long extractionStartTime = System.currentTimeMillis();
        extraction.select( parameters.getPretreatmentParameters(), parameters.getClassificationParameters(), parameters.getPostTreatmentParameters() );
        long extractionStopTime = System.currentTimeMillis();
        selectionProcessingTime = extractionStopTime - extractionStartTime;
        LOGGER.debug( "Extraction processing time : {} ms", selectionProcessingTime );
        selectedPixelNumber = getPixelNumber( extraction.getMaximums() );
        int index = 0;
        long stopTime = 0;
        try
        {
            extractionStartTime = System.currentTimeMillis();
            extraction.construct( parameters.getConstructionParameters() );
            stopTime = System.currentTimeMillis();
            constructionProcessingTime = stopTime - extractionStartTime;
            LOGGER.debug( "Construction processing time : {} ms", constructionProcessingTime );
            ArrayList< ReferenceSurface< R > > surfaces = extraction.getReferenceSurfaces();

            for ( Img refImage : GT )// Parsing of GT list.
            {
                index++;
                ArrayList< RMSEAndCoverageComputation< T, UnsignedShortType > > tempList = new ArrayList<>();
                for ( ReferenceSurface< R > rf : surfaces )// Test of each reconstructed surface
                {
                    RMSEAndCoverageComputation< T, UnsignedShortType > errors =
                            new RMSEAndCoverageComputation<>( refImage, index, rf.getzMap() );
                    errors.compute();
                    tempList.add( errors );
                }
                RMSEAndCoverageComputation< T, UnsignedShortType > finalError = findRightSurface( tempList );
                this.errorList.add( finalError );
            }
        }
        catch ( NotAnHeightMapException h )
        {
            LOGGER.debug( " Exception raised for ref n° " + index );
        }

        catch ( NoSurfaceFoundException s )
        {
            stopTime = System.currentTimeMillis();
            LOGGER.debug( "An NoSurfaceFoundException was raised" );
            constructionProcessingTime = stopTime - extractionStartTime;
            for ( int i = 0; i < GT.size(); i++ )
            {
                index++;
                RMSEAndCoverageComputation< T, UnsignedShortType > errors =
                        new RMSEAndCoverageComputation<>( index );
                this.errorList.add( errors );
            }
            LOGGER.debug( "Construction processing time : {} ms", constructionProcessingTime );
        }
        catch ( Exception n )
        {
            LOGGER.debug( "An {} was raised", n.getClass() );
        }
        this.processingTime = ( stopTime - startTime );
//        extraction.project();
    }

    private int getPixelNumber( Pixels[][] maximumCoordinates )
    {
        int n = 0;
        for ( int i = 0; i <= maximumCoordinates[ 0 ].length - 1; i++ )
        {
            for ( int j = 0; j <= maximumCoordinates.length - 1; j++ )
            {
                if ( maximumCoordinates[ j ][ i ] != null )
                {
                    n += maximumCoordinates[ j ][ i ].size();
                }
            }
        }
        return n;
    }

    /**
     * Returns the {@link RMSEAndCoverageComputation} with the smallest RMSE
     *
     * @param list a lis of {@link RMSEAndCoverageComputation}
     * @return a {@link RMSEAndCoverageComputation}
     */
    RMSEAndCoverageComputation< T, UnsignedShortType > findRightSurface( ArrayList< RMSEAndCoverageComputation< T, UnsignedShortType > > list )
    {
        if ( list.size() > 1 )
        {
//            list.removeIf( error -> error.getCoverage() == 0 );
//            if ( list.size() > 1 )
            {
                RMSEAndCoverageComputation< T, UnsignedShortType > errors = null;
                double distance = list.get( 0 ).getDistance();
                for ( RMSEAndCoverageComputation< T, UnsignedShortType > temp : list )
                {
                    double tempDistance = temp.getDistance();
                    if ( tempDistance <= distance )
                    {
                        distance = tempDistance;
                        errors = temp;
                    }
                }
                return errors;
            }
        }
        if ( list.isEmpty() )
        {
            return null;
        }
        return list.get( 0 );
    }

    public ArrayList< RMSEAndCoverageComputation< T, UnsignedShortType > > getErrorList()
    {
        return errorList;
    }

    public int getSelectedPixelNumber()
    {
        return selectedPixelNumber;
    }


    public String[] getErrorAnalysis( RMSEAndCoverageComputation< T, UnsignedShortType > error )
    {
        if ( error != null )
        {
            if ( error.getCoverage() != 0 )
            {
                return new String[]{ String.valueOf( error.getIndex() ),
                        String.valueOf( getSelectedPixelNumber() ),
                        String.valueOf( error.getRMSE() ),
                        String.valueOf( error.getCoverage() ),
                        String.valueOf( selectionProcessingTime ),
                        String.valueOf( Selection.getAmplitudePT() ),
                        String.valueOf( Selection.getOtsuPT() ),
                        String.valueOf( Selection.getIslandSearchPT() ),
                        String.valueOf( constructionProcessingTime ),
                        String.valueOf( extraction.getFS_OSConstructionProcessingTime() ),
                        String.valueOf( extraction.getFS_SurfaceConstructionProcessingTime() ),
                        String.valueOf( extraction.getSS_OSConstructionProcessingTime() ),
                        String.valueOf( extraction.getSS_SurfaceConstructionProcessingTime() ),
                        String.valueOf( processingTime ),
                        String.valueOf( extraction.getFS_OS_count() ),
                        String.valueOf( extraction.getFS_startingOS_count() ),
                        String.valueOf( extraction.getSS_OS_count() ),
                        String.valueOf( extraction.getSS_startingOS_count() ),
                        String.valueOf( extraction.getFS_goodSurfaces() ),
                        String.valueOf( extraction.getFS_smallSurfaces() ),
                        String.valueOf( extraction.getFS_finalizedSurfaces() ),
                        String.valueOf( extraction.getSS_goodSurfaces() ),
                        String.valueOf( extraction.getSS_smallSurfaces() ),
                        String.valueOf( extraction.getReferenceSurfaces().size() ) };
            }
            else
            {

                return new String[]{
                        String.valueOf( error.getIndex() ),
                        String.valueOf( getSelectedPixelNumber() ),
                        "NaN",
                        "NaN",
                        String.valueOf( selectionProcessingTime ),
                        String.valueOf( Selection.getAmplitudePT() ),
                        String.valueOf( Selection.getOtsuPT() ),
                        String.valueOf( Selection.getIslandSearchPT() ),
                        String.valueOf( constructionProcessingTime ),
                        String.valueOf( extraction.getFS_OSConstructionProcessingTime() ),
                        String.valueOf( extraction.getFS_SurfaceConstructionProcessingTime() ),
                        String.valueOf( extraction.getSS_OSConstructionProcessingTime() ),
                        String.valueOf( extraction.getSS_SurfaceConstructionProcessingTime() ),
                        String.valueOf( processingTime ),
                        String.valueOf( extraction.getFS_OS_count() ),
                        String.valueOf( extraction.getFS_startingOS_count() ),
                        String.valueOf( extraction.getSS_OS_count() ),
                        String.valueOf( extraction.getSS_startingOS_count() ),
                        String.valueOf( extraction.getFS_goodSurfaces() ),
                        String.valueOf( extraction.getFS_smallSurfaces() ),
                        String.valueOf( extraction.getFS_finalizedSurfaces() ),
                        String.valueOf( extraction.getSS_goodSurfaces() ),
                        String.valueOf( extraction.getSS_smallSurfaces() ),
                        String.valueOf( extraction.getReferenceSurfaces().size() ) };

            }
        }
        return null;
    }


}
