/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CSVWriter
{

    private BufferedWriter bw;
    private final static String EXTENSION = ".csv";
    private final File csvOutputFile;

    private final static Logger LOGGER = LoggerFactory.getLogger( CSVWriter.class );

    public CSVWriter( String testedFileName )
    {
        String path = "target/" +testedFileName + EXTENSION;

        LOGGER.debug( "CSV file name : {}", path );
        this.csvOutputFile = new File( path );
        try
        {
            bw = new BufferedWriter( new FileWriter( csvOutputFile, true ) );
            LOGGER.debug( "CSV location : " + "target/" + testedFileName + EXTENSION );
        }
       catch ( IOException ioException )
       {
           LOGGER.debug(" The csv file has not be created");
       }
    }

    public void writeToFile( String line ) throws IOException
    {
        bw = new BufferedWriter( new FileWriter( csvOutputFile, true ) );
        bw.write( line );
        bw.newLine();
//        LOGGER.info( " New line added in CSV file :  {}", line );
        bw.close();
    }

    public void writeToFile( String[] line ) throws IOException
    {
        writeToFile( String.join( ";", line ) );
    }


    public void close() throws IOException
    {
        bw.close();
        LOGGER.debug( "BF closed" );
    }
}
