/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.jzy3D;

import fr.pasteur.ida.zellige.element.Coordinate;
import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.element.Surface;
import fr.pasteur.ida.zellige.element.surfaceLine.SurfaceLine;
import org.jzy3d.analysis.AbstractAnalysis;
import org.jzy3d.analysis.AnalysisLauncher;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SurfaceDisplay extends AbstractAnalysis
{

    private final static Logger LOGGER = LoggerFactory.getLogger( SurfaceDisplay.class );
    private final Surface surface;

    /**
     * Displays the local maximums found using jzy3D package.
     *
     * @param surface the {@link Surface} to display
     */
    public static void displaySurface( Surface surface )
    {
        try
        {
            SurfaceDisplay display = new SurfaceDisplay( surface );
            AnalysisLauncher.open( display );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }

    public SurfaceDisplay( Surface surface )
    {

        this.surface = surface;
        init();
    }


    @Override
    public void init( )
    {
        if ( surface != null )
        {
            Coord3d[] points = new Coord3d[ surface.getSize() ];
            Color[] colors = new Color[ surface.getSize() ];

            int x;
            int y;
            int z;
            int index = 0;
            int size = 0;
            for ( int i = 0; i <= surface.getHeight() - 1; i++ )
            {
                SurfaceLine surfaceLine = this.surface.get( i );
                if ( surfaceLine != null )
                {
                    for ( int j = 0; j <= surfaceLine.getDimension().length - 1; j++ )
                    {
                        Pixels pixels = surfaceLine.get( j );
                        if ( pixels != null )
                        {
                            if (pixels.size() >=2)
                            {
                                size++;
                            }
                            for ( Coordinate coordinate :  pixels.get() )
                            {
                                x = coordinate.getX();
                                y = coordinate.getY();
                                z = coordinate.getZ();
                                points[ index ] = new Coord3d( x, y, z );
                                float G = 100f + (z *2);
                                float R = 100f + z + 10 ;
                                float B = 100f + z + 10;
                                colors[ index ] = Color.BLUE;
//                                colors[index] = new Color( R, G, B );
                                index++;
                            }
                        }

                    }

                }
            }
            LOGGER.debug( System.lineSeparator() );
            LOGGER.debug( "Number of duplicate = " + size );
            Scatter scatter = new Scatter( points, colors );
            scatter.setWidth( 1 );
            chart = AWTChartComponentFactory.chart( Quality.Advanced, "newt" );
            chart.getScene().add( scatter );

        }
    }
}
