/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.test;

import fr.pasteur.ida.zellige.steps.Utils;
import fr.pasteur.ida.zellige.utils.test.exception.DifferentReferenceTestedSizeException;
import fr.pasteur.ida.zellige.utils.test.exception.NotAnHeightMapException;
import ij.ImageJ;
import io.scif.img.IO;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.display.imagej.ImageJFunctions;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RMSEAndCoverageComputation< T extends RealType< T > & NativeType< T >, R extends RealType< R > & NativeType< R > >
{

    private final static Logger LOGGER = LoggerFactory.getLogger( RMSEAndCoverageComputation.class );
    int index;
    private final RandomAccessibleInterval< T > groundTruth;
    private final RandomAccessibleInterval< R > projection;
    private double coverage = 0;
    private double RMSE;
    private double distance;
    private static int subtract = - 1;// value to subtract to the projection

    public RMSEAndCoverageComputation( RandomAccessibleInterval< T > groundTruth, int index,
                                       RandomAccessibleInterval< R > projection ) throws NotAnHeightMapException, DifferentReferenceTestedSizeException
    {
        inputCheck( groundTruth, projection );
        this.groundTruth = groundTruth;
        this.projection = projection;
        this.index = index;
    }

    public RMSEAndCoverageComputation( int index )
    {
        this.groundTruth = null;
        this.projection = null;
        this.index = index;
    }

    public static void setSubtract( int subtract )
    {
        RMSEAndCoverageComputation.subtract = subtract;
    }

    void inputCheck( RandomAccessibleInterval< T > groundTruth,
                     RandomAccessibleInterval< R > tested ) throws NotAnHeightMapException, DifferentReferenceTestedSizeException
    {
        if ( groundTruth != null && tested != null )
        {
            if ( groundTruth.numDimensions() > 2 )
            {
                LOGGER.error( "The reference height map has more than two dimension ! (It's not an height map)" );
                throw new NotAnHeightMapException();
            }
            if ( tested.numDimensions() > 2 )
            {
                LOGGER.error( "The tested height map has more than two dimension ! (It's not an height map)" );
                throw new NotAnHeightMapException();
            }
            if ( groundTruth.dimension( 0 ) != tested.dimension( 0 ) || groundTruth.dimension( 1 ) != tested.dimension( 1 ) )
            {
                LOGGER.error( "The reference and tested height maps have different size !" );
                throw new DifferentReferenceTestedSizeException();
            }
            LOGGER.debug( "Starting Reconstruction Error Computation." );
        }
        else if ( groundTruth == null )
        {
            LOGGER.error( "The reference height map is null" );
            throw new NullPointerException();
        }
        else
        {
            LOGGER.error( "The tested height map is null" );
            throw new NullPointerException();
        }
    }

    public double getCoverage()
    {
        return coverage;
    }

    public double getRMSE()
    {
        return RMSE;
    }


    public double getDistance()
    {
        return distance;
    }

    public void compute()
    {
        RandomAccess< T > groundTruthAccess = groundTruth.randomAccess();// the GT
        RandomAccess< R > testedAccess = projection.randomAccess(); // the projection
        int groundTruthPixelNumber = 0;
        int projectionPixelNumber = 0;
        double meanSquareError = 0;
        int count = 0;
        int count1 = 0;
        int count0 = 0;
        double tempDistance = 0; // metric to choose the projection corresponding to the GT
        for ( int x = 0; x < groundTruth.dimension( 0 ); x++ )
        {
            for ( int y = 0; y < groundTruth.dimension( 1 ); y++ )
            {
                double groundTruthValue = Utils.setPositionAndGet( groundTruthAccess, x, y ).getRealDouble();
                double reconstructedPixelValue = Utils.setPositionAndGet( testedAccess, x, y ).getRealDouble();
                if ( groundTruthValue != 0 ) // there is a pixel in the ref height map
                {
                    groundTruthPixelNumber++;

                    if ( reconstructedPixelValue != 0 )
                    {
                        projectionPixelNumber++;
                        double d = Math.abs( groundTruthValue - ( reconstructedPixelValue + subtract ) );
                        double toAdd = Math.pow( d, 2 );
                        if ( toAdd == 0 )
                        {
                            count0++;
                        }
                        else if ( toAdd == 1 )
                        {
                            count1++;
                        }
                        else
                        {
                            count++;
                        }
                        meanSquareError += toAdd;
                        tempDistance += Math.abs( d );
                    }
                }
            }
        }
        if ( projectionPixelNumber != 0 )
        {
            this.RMSE = Math.sqrt( meanSquareError / ( double ) projectionPixelNumber );

            this.coverage = ( double ) projectionPixelNumber / groundTruthPixelNumber;

        }
        double unreconstructedPixelNumber = groundTruthPixelNumber - projectionPixelNumber;
        this.distance = tempDistance / ( double ) groundTruthPixelNumber +
                10 * ( unreconstructedPixelNumber ) / ( double ) groundTruthPixelNumber
        ;
        LOGGER.info( "RMSE = {} ", this.RMSE );
        LOGGER.info( "Coverage = {} ", this.coverage );
        LOGGER.info( " Reconstruction Error Computation done." );
    }

    public int getIndex()
    {
        return index;
    }

    public RandomAccessibleInterval< T > getGroundTruth()
    {
        return groundTruth;
    }

    public RandomAccessibleInterval< R > getProjection()
    {
        return projection;
    }

    @SuppressWarnings( { "unchecked", "DuplicatedCode" } )
    public static void main( String[] args ) throws NotAnHeightMapException, DifferentReferenceTestedSizeException
    {
        String pathGT = "C:\\Users\\ctrebeau\\Desktop\\Zellige analysis\\files\\Mouche_c01_f0001_p013\\GT\\Ground truth Height map_4_crop.tif";
        String pathHM = "C:\\Users\\ctrebeau\\Desktop\\Figures mouche dif values\\blurxy10\\Raw_HM_7.tif";
        final SCIFIOImgPlus< ? > imgPlusRef = IO.openAll( pathGT ).get( 0 );
        Img< FloatType > GT = ( Img< FloatType > ) imgPlusRef.getImg();
        new ImageJ();
        ImageJFunctions.show( GT );

        final SCIFIOImgPlus< ? > imgPlus = IO.openAll( pathHM ).get( 0 );
        Img< FloatType > HM = ( Img< FloatType > ) imgPlus.getImg();
        ImageJFunctions.show( HM );
        RMSEAndCoverageComputation< FloatType, FloatType > computation = new RMSEAndCoverageComputation<>( GT, 0, HM );
        computation.compute();
        LOGGER.debug( "RMSE = " + computation.RMSE );
        LOGGER.debug( "Coverage = " + computation.coverage );
    }
}


