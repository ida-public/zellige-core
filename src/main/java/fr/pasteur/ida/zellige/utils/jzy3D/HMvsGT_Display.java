/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.jzy3D;

import fr.pasteur.ida.zellige.utils.Util;
import io.scif.img.IO;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import org.jzy3d.analysis.AbstractAnalysis;
import org.jzy3d.analysis.AnalysisLauncher;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HMvsGT_Display< T extends RealType< T > & NativeType< T >, R extends RealType< R > & NativeType< R > > extends AbstractAnalysis
{
    private final static Logger LOGGER = LoggerFactory.getLogger( HMvsGT_Display.class );
    private final RandomAccessibleInterval< T > groundTruth;
    private final RandomAccessibleInterval< R > projection;
    private final int subtract;

    public HMvsGT_Display( RandomAccessibleInterval< T > groundTruth,
                           RandomAccessibleInterval< R > heightMap, int subtract )
    {
        this.groundTruth = groundTruth;
        this.projection = heightMap;
        this.subtract = subtract;
        init();
    }

    /**
     * Displays the local maximums found using jzy3D package.
     *
     * @param heightMap   the height map to tested as a {@link RandomAccessibleInterval}
     * @param groundTruth the ground truth to test against
     * @param <T>         the height map type
     * @param <R>         the ground truth type
     */
    public static < T extends RealType< T > & NativeType< T >, R extends RealType< R > & NativeType< R > > void displayDiff( RandomAccessibleInterval< T > groundTruth,
                                                                                                                             RandomAccessibleInterval< R > heightMap, int subtract )
    {
        {
            try
            {
                HMvsGT_Display< T, R > display = new HMvsGT_Display<>( groundTruth, heightMap, subtract );
                AnalysisLauncher.open( display );
            }
            catch ( Exception e )
            {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings( "unchecked" )
    public static void main( String[] args )
    {
        String pathGT = "C:\\Users\\ctrebeau\\Desktop\\Zellige analysis\\files\\Organoid\\Organoid_GT_HM2.tif";
        String pathHM = "C:\\Users\\ctrebeau\\Desktop\\Zellige analysis\\Paper_012022\\Organoid\\Organoid_HM_0.tif";
        final SCIFIOImgPlus< ? > imgPlusRef = IO.openAll( pathGT ).get( 0 );
        Img< FloatType > GT = ( Img< FloatType > ) imgPlusRef.getImg();


        /* The raw image to test*/
        final SCIFIOImgPlus< ? > imgPlus = IO.openAll( pathHM ).get( 0 );
        Img< FloatType > HM = ( Img< FloatType > ) imgPlus.getImg();

        HMvsGT_Display.displayDiff( GT, HM, - 1 );
    }

    @Override
    public void init()
    {
        float[][] GT = Util.convert( groundTruth, 0 );
        float[][] HM = Util.convert( projection, 0 );
        int count = getCount( GT ) + getCount( HM ) + 2;
        Coord3d[] points = new Coord3d[ count ];
        Color[] colors = new Color[ count ];
        int index = 0;
        for ( int x = 0; x < GT[ 0 ].length; x++ )
        {
            for ( int y = 0; y < GT.length; y++ )
            {
                float GTValue = GT[ y ][ x ];
                float HMValue = HM[ y ][ x ];
                LOGGER.debug( "GT = {}, HM = {}", GTValue, HMValue );

                if ( GTValue != 0 )
                {
                    points[ index ] = new Coord3d( x, - y, - GTValue );
                    colors[ index ] = Color.BLUE.alphaSelf( 0.1f );
                    index++;
                }
                if ( HMValue != 0 )
                {
                    points[ index ] = new Coord3d( x, - y, - ( HMValue + subtract ) );
                    colors[ index ] = Color.GREEN.alpha( 0.05f );
                    index++;
                }
            }
        }
        points[ index ] = new Coord3d( 0, 0, 0 );
        colors[ index ] = Color.WHITE.alpha( 0.0f );
        points[ index + 1 ] = new Coord3d( 0, 0, - 11 );
        colors[ index + 1 ] = Color.WHITE.alpha( 0.0f );

        Scatter scatter = new Scatter( points, colors );
        scatter.setWidth( 2f );
        chart = AWTChartComponentFactory.chart( Quality.Advanced, "newt" );
//        chart.getScale().set
        chart.getScene().add( scatter );
    }

    public int getCount( float[][] map )
    {
        int count = 0;
        for ( int x = 0; x < map[ 0 ].length; x++ )
        {
            for ( float[] floats : map )
            {
                float mapValue = floats[ x ];
                if ( mapValue != 0 )
                {
                    count++;
                }
            }
        }
        return count;
    }
}
