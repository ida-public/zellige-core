/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.test;

import fr.pasteur.ida.zellige.ReferenceSurfaceExtraction;
import fr.pasteur.ida.zellige.steps.construction.rounds.ConstructionParameters;
import fr.pasteur.ida.zellige.steps.projection.ProjectionParameters;
import fr.pasteur.ida.zellige.steps.selection.classification.ClassificationParameters;
import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;
import fr.pasteur.ida.zellige.steps.selection.postTreatment.PostTreatmentParameters;
import fr.pasteur.ida.zellige.steps.selection.pretreatment.PretreatmentParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ZParameters
{

    private final static Logger LOGGER = LoggerFactory.getLogger( ZParameters.class );
    private final PretreatmentParameters pretreatmentParameters;
    private final ClassificationParameters classificationParameters;
    private final PostTreatmentParameters postTreatmentParameters;
    private final ConstructionParameters[] constructionParameters;
    private final ProjectionParameters projectionParameters;


    public ZParameters( String filter, int filterParameter,
                        int amplitudeThreshold, int otsuThreshold,
                        int ISConnexity, int ISSize,
                        double XYSmoothing, double ZSmoothing,
                        double startingThreshold1, int overlap1, double connexityRate1,
                        double startingThreshold2, int overlap2, double connexityRate2,
                        double surfaceMinSizeFactor, int delta ) throws DataValidationException
    {
        pretreatmentParameters = new PretreatmentParameters( filter, filterParameter );
        classificationParameters = new ClassificationParameters( amplitudeThreshold, otsuThreshold );
        postTreatmentParameters = new PostTreatmentParameters( XYSmoothing, ZSmoothing, ISSize, ISConnexity );

        constructionParameters = new ConstructionParameters[]{
                new ConstructionParameters( startingThreshold1, overlap1, connexityRate1, surfaceMinSizeFactor ),
                new ConstructionParameters( startingThreshold2, overlap2, connexityRate2, surfaceMinSizeFactor ) };
        projectionParameters = new ProjectionParameters( delta, "MIP" );
    }


    public PretreatmentParameters getPretreatmentParameters()
    {
        return pretreatmentParameters;
    }

    public ClassificationParameters getClassificationParameters()
    {
        return classificationParameters;
    }

    public PostTreatmentParameters getPostTreatmentParameters()
    {
        return postTreatmentParameters;
    }

    public ConstructionParameters[] getConstructionParameters()
    {
        return constructionParameters;
    }

    public ProjectionParameters getProjectionParameters()
    {
        return projectionParameters;
    }

    public void print()
    {
        LOGGER.debug( "method : " + pretreatmentParameters.getMethod() );
        LOGGER.debug( "parameter : " + pretreatmentParameters.getParameter() );
        LOGGER.debug( "amplitude  : " + classificationParameters.getAmplitudeThreshold() );
        LOGGER.debug( "threshold  : " + classificationParameters.getOtsuThreshold() );
        LOGGER.debug( "connexity : " + postTreatmentParameters.getConnexity() );
        LOGGER.debug( "island size : " + postTreatmentParameters.getIslandSize() );
        LOGGER.debug( "sigmaXY : " + postTreatmentParameters.getSigmaXY() );
        LOGGER.debug( "sigmaZ : " + postTreatmentParameters.getSigmaZ() );
        LOGGER.debug( "starting os size1 : " + constructionParameters[ 0 ].getStartingSizeThreshold() );
        LOGGER.debug( "overlap1 :" + constructionParameters[ 0 ].getOverlap() );
        LOGGER.debug( "percent1  :" + constructionParameters[ 0 ].getConnexityRate() );
        LOGGER.debug( "starting os size2 : " + constructionParameters[ 1 ].getStartingSizeThreshold() );
        LOGGER.debug( "overlap2 :" + constructionParameters[ 1 ].getOverlap() );
        LOGGER.debug( "percent2  :" + constructionParameters[ 1 ].getConnexityRate() );
        LOGGER.debug( "adequacy : " + ReferenceSurfaceExtraction.getAdequacy() );
        LOGGER.debug( " Delta : " + projectionParameters.getDelta() );
    }
}
