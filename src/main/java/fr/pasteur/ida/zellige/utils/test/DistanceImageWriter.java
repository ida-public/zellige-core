/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.utils.test;

import fr.pasteur.ida.zellige.utils.Util;
import ij.ImageJ;
import io.scif.img.IO;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.display.imagej.ImageJFunctions;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class DistanceImageWriter< T extends RealType< T > & NativeType< T >, R extends RealType< R > & NativeType< R > >
{

    private final static Logger LOGGER = LoggerFactory.getLogger( DistanceImageWriter.class );
    private final RandomAccessibleInterval< T > groundTruth;
    private final RandomAccessibleInterval< R > projection;
    private final String fileName;
    private final int subtract;

    public DistanceImageWriter( RandomAccessibleInterval< T > groundTruth,
                                RandomAccessibleInterval< R > heightMap, String fileName, int subtract )
    {
        this.groundTruth = groundTruth;
        this.projection = heightMap;
        this.fileName = fileName;
        this.subtract = subtract;
    }

    public static < T extends RealType< T > & NativeType< T >, R extends RealType< R > & NativeType< R > >
    void create( RandomAccessibleInterval< T > groundTruth,
                 RandomAccessibleInterval< R > heightMap, String fileName, int subtract ) throws IOException
    {
        DistanceImageWriter< T, R > writer = new DistanceImageWriter<>( groundTruth, heightMap, fileName, subtract );
        writer.run();
    }

    @SuppressWarnings( { "unchecked", "DuplicatedCode" } )
    public static void main( String[] args ) throws IOException
    {
        String pathGT = "C:\\Users\\ctrebeau\\Desktop\\Zellige analysis\\files\\Mouche_c01_f0001_p013\\GT\\Ground truth Height map_4_crop.tif";
        String pathHM = "C:\\Users\\ctrebeau\\Desktop\\Zellige analysis\\Paper_012022\\Mouche\\Mouche_HM_3.tif";
        final SCIFIOImgPlus< ? > imgPlusRef = IO.openAll( pathGT ).get( 0 );
        Img< FloatType > GT = ( Img< FloatType > ) imgPlusRef.getImg();
        new ImageJ();
        ImageJFunctions.show( GT );

        final SCIFIOImgPlus< ? > imgPlus = IO.openAll( pathHM ).get( 0 );
        Img< FloatType > HM = ( Img< FloatType > ) imgPlus.getImg();
        ImageJFunctions.show( HM );
        DistanceImageWriter.create( GT, HM, "Mouche_distance4_crop", - 1 );
    }

    public void run() throws IOException
    {
        CSVWriter writer = new CSVWriter( fileName );
        String[] header = { "x", "y", "distance" };
        writer.writeToFile( header );
        float[][] GT = Util.convert( groundTruth, 0 );
        float[][] HM = Util.convert( projection, 0 );
        double mse = 0;
        int count = 0;
        for ( int x = 0; x < GT[ 0 ].length; x++ )
        {
            for ( int y = 0; y < GT.length; y++ )
            {
                float GTValue = GT[ y ][ x ];
                float HMValue = HM[ y ][ x ];
                if ( GTValue != 0 && HMValue > 0 )
                {
                    double distance = Math.abs( GTValue - ( HMValue + subtract ) );
                    writer.writeToFile( new String[]{ String.valueOf( x ), String.valueOf( y ), String.valueOf( distance ) } );
                    mse += Math.pow( distance, 2 );
                    count++;
                }
                else
                {
                    writer.writeToFile( new String[]{ String.valueOf( x ), String.valueOf( y ), "NA" } );
                }
            }
        }
        double RMSE = Math.sqrt( mse / count );
        LOGGER.debug( "RMSE = {}", RMSE );
        writer.close();

    }
}
