/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige;


import fr.pasteur.ida.zellige.steps.construction.rounds.ConstructionParameters;
import fr.pasteur.ida.zellige.steps.projection.DisplayParameters;
import fr.pasteur.ida.zellige.steps.projection.ProjectionParameters;
import fr.pasteur.ida.zellige.steps.selection.classification.ClassificationParameters;
import fr.pasteur.ida.zellige.steps.selection.postTreatment.PostTreatmentParameters;
import fr.pasteur.ida.zellige.steps.selection.pretreatment.PretreatmentParameters;
import ij.IJ;
import ij.ImageJ;
import io.scif.img.IO;
import io.scif.img.SCIFIOImgPlus;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;


/**
 *
 */
public class Main
{

    private final static Logger LOGGER = LoggerFactory.getLogger( Main.class );

    @SuppressWarnings( "unchecked" )
    public static < T extends RealType< T > & NativeType< T > > void main( String[] args ) throws Exception
    {
        // Launch ImageJ.
        ImageJ ij = new ImageJ();


        // Input of the image.
        final String imagePath =// "C:\\Users\\ctrebeau\\Desktop\\Zellige analysis\\files\\SNR\\snr_000\\multiSurfaces\\phantoms_snr0.mat.tif";
                args[ 0 ]; /* The image path goes here !!!! */
        LOGGER.debug( imagePath );
        /* JY version for opening files. */
        final SCIFIOImgPlus< ? > imgPlus = IO.openAll( imagePath ).get( 0 );

        final Img< T > stackImage = ( Img< T > ) imgPlus.getImg();


        /* User Parameters AKA arguments to run the program*/
        int amplitude = Integer.parseInt( args[ 1 ] );
        int otsu = Integer.parseInt( args[ 2 ] );
        int islandSize = Integer.parseInt( args[ 3 ] );
        int connexity = Integer.parseInt( args[ 4 ] );
        double sigmaXY = Double.parseDouble( args[ 5 ] );
        double sigmaZ = Double.parseDouble( args[ 6 ] );
        double startingOsSize1 = Double.parseDouble( args[ 7 ] );
        int overlap1 = Integer.parseInt( args[ 8 ] );
        double connexityRate1 = Double.parseDouble( args[ 9 ] );
        double startingOsSize2 = Double.parseDouble( args[ 10 ] );
        int overlap2 = Integer.parseInt( args[ 11 ] );
        double connexityRate2 = Double.parseDouble( args[ 12 ] );
        double surfaceMinSizeFactor = Double.parseDouble( args[ 13 ] );
        int delta = Integer.parseInt( args[ 14 ] );
        /* End of parameters. */


        // Fixed parameters for now...
        String filter = "GaussianBlur";
        double filterParameter = 2;


        /* Print parameters.*/
        LOGGER.debug( "Input image : " + imagePath );
        LOGGER.debug( "filter : " + filter );
        LOGGER.debug( "filter parameter : " + filterParameter );
        LOGGER.debug( "amplitude  : " + amplitude );
        LOGGER.debug( "threshold  : " + otsu );
        LOGGER.debug( "connexity : " + connexity );
        LOGGER.debug( "island size : " + islandSize );
        LOGGER.debug( "sigmaXY : " + sigmaXY );
        LOGGER.debug( "sigmaZ : " + sigmaZ );
        LOGGER.debug( "starting os size1 : " + startingOsSize1 );
        LOGGER.debug( "overlap1 :" + overlap1 );
        LOGGER.debug( "connexityRate1  :" + connexityRate1 );
        LOGGER.debug( "starting os size2 : " + startingOsSize2 );
        LOGGER.debug( "overlap2 :" + overlap2 );
        LOGGER.debug( "connexityRate2  :" + connexityRate2 );
        LOGGER.debug( "surface Minimum size factor : " + surfaceMinSizeFactor );
        LOGGER.debug( "delta : " + delta );
        LOGGER.debug( System.lineSeparator() );
        /* End of  Print parameters.*/

        PretreatmentParameters pretreatmentParameters = new PretreatmentParameters( filter, filterParameter );
        ClassificationParameters classificationParameters = new ClassificationParameters( amplitude, otsu );
        PostTreatmentParameters postTreatmentParameters = new PostTreatmentParameters( sigmaXY, sigmaZ, islandSize, connexity );
        ProjectionParameters projectionParameters = new ProjectionParameters( delta, "MIP" );// no other method implemented yet.
        ConstructionParameters[] constructionParameters = new ConstructionParameters[]{
                new ConstructionParameters( startingOsSize1, overlap1, connexityRate1, surfaceMinSizeFactor ),
                new ConstructionParameters( startingOsSize2, overlap2, connexityRate2, surfaceMinSizeFactor ) };


        DisplayParameters displayParameters = new DisplayParameters(
                true,
                true,
                true,
                true,
                true, true );
        IJ.log( "Type: " + imgPlus.firstElement().getClass().toGenericString() );

        if ( stackImage.numDimensions() == 3 )// Is it a stack ?
        {
            ReferenceSurfaceExtraction< T > rse = new ReferenceSurfaceExtraction<>
                    ( stackImage, stackImage.factory() );
            rse.select( pretreatmentParameters, classificationParameters, postTreatmentParameters );
            rse.construct( constructionParameters );
            rse.project( projectionParameters, displayParameters );
            if ( ! GraphicsEnvironment.isHeadless() )
            {
                try
                {
                    rse.project( projectionParameters, displayParameters );
                }
                catch ( Exception e )
                {
                    LOGGER.debug( e.getMessage() );
                }
            }
        }
        else
        {
            LOGGER.debug( " This image has to be a z-stack ! " );
        }
    }
}
