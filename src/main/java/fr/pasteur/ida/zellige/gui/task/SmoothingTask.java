/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.task;

import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.steps.selection.postTreatment.PostTreatment;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import net.imglib2.img.Img;
import net.imglib2.type.logic.BitType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SmoothingTask extends AbstractTask< Pixels[][] >
{

    private final static Logger LOGGER = LoggerFactory.getLogger( SmoothingTask.class );
    private final SimpleObjectProperty< Img< BitType > > islandSearchImage;
    private final DoubleProperty sigmaXY;
    private final DoubleProperty sigmaZ;

    public SmoothingTask( SimpleObjectProperty< Img< BitType > > islandSearchImage, DoubleProperty sigmaXY, DoubleProperty sigmaZ )
    {
        this.islandSearchImage = islandSearchImage;
        this.sigmaXY = sigmaXY;
        this.sigmaZ = sigmaZ;
    }

    @Override
    protected Pixels[][] call() throws Exception
    {
        LOGGER.info( "Computing first smoothing..." );
        return PostTreatment.runSmoothing( islandSearchImage.getValue(), sigmaXY.intValue(), sigmaZ.intValue() );
    }
}
