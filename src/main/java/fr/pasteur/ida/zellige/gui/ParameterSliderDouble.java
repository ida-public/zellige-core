/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui;

import javafx.beans.NamedArg;
import javafx.beans.binding.Bindings;
import javafx.util.StringConverter;
import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParameterSliderDouble extends ParameterSlider
{

    private final static Logger LOGGER = LoggerFactory.getLogger( ParameterSliderDouble.class );
    public ParameterSliderDouble( @NamedArg( "name" ) String name,
                                  @NamedArg( "min" ) double min,
                                  @NamedArg( "max" ) double max,
                                  @NamedArg( "major" ) double majorTickUnit,
                                  @NamedArg( "minor" ) int minorTickUnit,
                                  @NamedArg( "increment" ) double increment,
                                  @NamedArg( "default" ) double defaultValue,
                                  @NamedArg( "interval" ) String interval )
    {
        super( name, min, max, majorTickUnit, minorTickUnit, increment, defaultValue, interval );
    }

    public void bindProperties()
    {
        Bindings.bindBidirectional( this.userValueProperty(), this.getSlider().valueProperty(), new StringConverter< Number >()
        {
            final double oldValue = getSlider().valueProperty().doubleValue();

            @Override
            public String toString( Number number )
            {
                LOGGER.debug( String.valueOf( Precision.round( number.doubleValue(), 1 ) ) );
                return String.valueOf( Precision.round( number.doubleValue(), 1 ) );
            }

            @Override
            public Number fromString( String s )
            {
                LOGGER.debug( "String value is {}", s );
                LOGGER.debug( "Number value is {}", Double.parseDouble( s ) );
                try
                {
                    return Double.parseDouble( s );
                }
                catch ( NumberFormatException e )
                {
                    return oldValue;// set the slider value at the old value
                }
            }
        } );
    }
}
