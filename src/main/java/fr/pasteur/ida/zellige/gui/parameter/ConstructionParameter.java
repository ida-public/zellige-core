/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.parameter;

import fr.pasteur.ida.zellige.gui.ZDoubleProperty;
import javafx.beans.property.IntegerProperty;

public class ConstructionParameter
{

    private final double c1;
    private final int r1;
    private final double st1;
    private final double c2;
    private final int r2;
    private final double st2;
    private final int surfaceSize;

    public ConstructionParameter( ZDoubleProperty c1, IntegerProperty r1, ZDoubleProperty st1, ZDoubleProperty c2, IntegerProperty r2, ZDoubleProperty st2, ZDoubleProperty surfaceSize )
    {
        this.c1 = c1.value();
        this.r1 = r1.intValue();
        this.st1 = st1.value();
        this.c2 = c2.value();
        this.r2 = r2.intValue();
        this.st2 = st2.value();
        this.surfaceSize = surfaceSize.intValue();
    }

    public double getC1()
    {
        return c1;
    }

    public int getR1()
    {
        return r1;
    }

    public double getSt1()
    {
        return st1;
    }

    public double getC2()
    {
        return c2;
    }

    public int getR2()
    {
        return r2;
    }

    public double getSt2()
    {
        return st2;
    }

    public int getSurfaceSize()
    {
        return surfaceSize;
    }
}
