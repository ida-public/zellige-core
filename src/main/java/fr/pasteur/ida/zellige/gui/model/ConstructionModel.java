/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.model;

import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.element.ReferenceSurface;
import fr.pasteur.ida.zellige.element.Surface;
import fr.pasteur.ida.zellige.gui.ZDoubleProperty;
import fr.pasteur.ida.zellige.gui.controller.MainController;
import fr.pasteur.ida.zellige.gui.task.AbstractTask;
import fr.pasteur.ida.zellige.gui.task.ConstructionCompletionTask;
import fr.pasteur.ida.zellige.gui.task.RunFirstConstructionTask;
import fr.pasteur.ida.zellige.gui.task.RunSecondConstructionTask;
import fr.pasteur.ida.zellige.steps.construction.exception.FirstRoundConstructionException;
import fr.pasteur.ida.zellige.steps.construction.exception.SecondRoundConstructionException;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.ImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class ConstructionModel< T extends RealType< T > & NativeType< T > >
{

    private final static Logger LOGGER = LoggerFactory.getLogger( ConstructionModel.class );

    private final ZDoubleProperty c1;
    private final ZDoubleProperty c2;
    private final IntegerProperty r1;
    private final IntegerProperty r2;
    private final ZDoubleProperty st1;
    private final ZDoubleProperty st2;
    private final ZDoubleProperty surfaceSize;
    private final SimpleObjectProperty< ArrayList< Surface > > firstRoundSurfaces = new SimpleObjectProperty<>();
    private final SimpleObjectProperty< ArrayList< Surface > > secondRoundSurfaces = new SimpleObjectProperty<>();
    private final SimpleObjectProperty< ArrayList< ReferenceSurface< T > > > referenceSurfaces = new SimpleObjectProperty<>();
    private final SimpleObjectProperty< Pixels[][] > maximums = new SimpleObjectProperty<>();
    private RandomAccessibleInterval< T > input;
    private ImgFactory< T > factory;

    public ConstructionModel()
    {
        c1 = new ZDoubleProperty();
        c2 = new ZDoubleProperty();
        r1 = new SimpleIntegerProperty();
        r2 = new SimpleIntegerProperty();
        st1 = new ZDoubleProperty();
        st2 = new ZDoubleProperty();
        surfaceSize = new ZDoubleProperty();
    }

    public void firstRound()
    {
        LOGGER.info( "Computing first round construction..." );
        AbstractTask< ArrayList< Surface > > firstRoundTask = new RunFirstConstructionTask( maximums.getValue(), st1.doubleValue(), r1.intValue(), c1.doubleValue(), surfaceSize.doubleValue() / 100.0 );
        firstRoundTask.setOnSucceeded( workerStateEvent ->
        {
            firstRoundSurfaces.setValue( firstRoundTask.getValue() );
            if ( firstRoundTask.getValue() == null )
            {
                LOGGER.info( "" ); //TODO write right message
                MainController.showError( new FirstRoundConstructionException() );
            }
        } );
        LOGGER.info( "First round construction done." );
        firstRoundTask.start();
    }


    public void secondRound()
    {
        LOGGER.info( "Computing second round construction..." );
        AbstractTask< ArrayList< Surface > > secondRoundTask = new RunSecondConstructionTask( firstRoundSurfacesProperty().getValue(), st2.getValue(), r2.intValue(), c2.doubleValue(), surfaceSize.doubleValue() / 100.0 );
        secondRoundTask.setOnSucceeded( workerStateEvent ->
        {
            secondRoundSurfacesProperty().setValue( secondRoundTask.getValue() );
            if ( secondRoundTask.getValue() == null )
            {
                LOGGER.info( "" ); //TODO write right message
                MainController.showError( new SecondRoundConstructionException() );
            }
        } );
        LOGGER.info( "Second round construction done." );
        secondRoundTask.start();
    }

    public void runConstruction()
    {
        LOGGER.debug( "Run construction" );
        if ( firstRoundSurfacesProperty().getValue() == null )
        {
            firstRound();
        }
        else if ( secondRoundSurfaces.getValue() == null )
        {
            secondRound();
        }
    }


    public void constructionCompletion()
    {
        AbstractTask< ArrayList< ReferenceSurface< T > > > constructionCompletionTask = new ConstructionCompletionTask<>( input, factory, secondRoundSurfaces.getValue() );
        constructionCompletionTask.setOnSucceeded( workerStateEvent ->
                referenceSurfacesProperty().setValue( constructionCompletionTask.getValue() ) );
        constructionCompletionTask.start();
    }


    public ZDoubleProperty c1Property()
    {
        return c1;
    }

    public ZDoubleProperty c2Property()
    {
        return c2;
    }

    public IntegerProperty r1Property()
    {
        return r1;
    }

    public IntegerProperty r2Property()
    {
        return r2;
    }

    public ZDoubleProperty st1Property()
    {
        return st1;
    }

    public ZDoubleProperty st2Property()
    {
        return st2;
    }

    public ZDoubleProperty surfaceSizeProperty()
    {
        return surfaceSize;
    }

    public SimpleObjectProperty< ArrayList< Surface > > firstRoundSurfacesProperty()
    {
        return firstRoundSurfaces;
    }

    public SimpleObjectProperty< ArrayList< Surface > > secondRoundSurfacesProperty()
    {
        return secondRoundSurfaces;
    }

    public SimpleObjectProperty< ArrayList< ReferenceSurface< T > > > referenceSurfacesProperty()
    {
        return referenceSurfaces;
    }

    public void setInput( RandomAccessibleInterval< T > input )
    {
        this.input = input;
    }


    public void setFactory( ImgFactory< T > factory )
    {
        this.factory = factory;
    }

    public double getC1()
    {
        return c1.get();
    }

    public RandomAccessibleInterval< T > getInput()
    {
        return input;
    }

    public ImgFactory< T > getFactory()
    {
        return factory;
    }

    public SimpleObjectProperty< Pixels[][] > maximumsProperty()
    {
        return maximums;
    }
}
