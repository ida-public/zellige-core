/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui;

import javafx.scene.image.ImageView;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import net.imglib2.Cursor;
import net.imglib2.IterableInterval;
import net.imglib2.type.logic.BitType;

public class ImageFXDisplay
{


    private final IterableInterval< BitType > input;
    private final ImageView[] imageViews;
    private final int width;
    private final int height;
    private final int depth;


    public ImageFXDisplay( IterableInterval< BitType > input )
    {
        this.input = input;

        width = ( int ) input.dimension( 0 );
        height = ( int ) input.dimension( 1 );
        depth = ( int ) input.dimension( 2 );
        this.imageViews = new ImageView[ depth ];

    }

    public void set()
    {
        int[] data2 = convert2( input );
        for ( int z = 0; z < depth; z++ )
        {
            WritableImage writableImage = new WritableImage( width, height );
            PixelWriter pixelWriter = writableImage.getPixelWriter();
            pixelWriter.setPixels( 0, 0, width, height, PixelFormat.getIntArgbInstance(), data2, height * width * z, width );
            this.imageViews[ z ] = new ImageView( writableImage );
            setImageViews( this.imageViews[ z ] );
        }

    }


    public int[] convert2( IterableInterval< BitType > input )
    {
        int[] data = new int[ height * width * depth ];
        Cursor< BitType > cursor = input.cursor();
        int index = 0;
        while ( cursor.hasNext() )
        {
            cursor.fwd();
            if ( cursor.get().get() )
            {
                data[ index++ ] = ( 0xFFFFFFFF );
            }
            else
            {
                data[ index++ ] = 0xFF000000;
            }


        }
        return data;
    }

    public ImageView[] getImageViews()
    {
        return this.imageViews;
    }

    public void setImageViews( ImageView imageView )
    {
        imageView.setPreserveRatio( true );
        setFit( imageView );

        imageView.setSmooth( true );
        imageView.setCache( true );
    }

    public void setFit( ImageView imageView )
    {
        if ( width >= height )
        {
            imageView.setFitWidth( 254 );
        }
        else
        {
            imageView.setFitHeight( 254 );
        }
    }


}
