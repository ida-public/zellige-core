/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.model;

import fr.pasteur.ida.zellige.element.Projection;
import fr.pasteur.ida.zellige.element.ReferenceSurface;
import fr.pasteur.ida.zellige.steps.projection.ReferenceSurfaceProjection;
import javafx.beans.property.*;
import net.imglib2.img.Img;
import net.imglib2.img.display.imagej.ImageJFunctions;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.UnsignedShortType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class ProjectionModel< T extends RealType< T > & NativeType< T > >
{
    private final static Logger LOGGER = LoggerFactory.getLogger( ProjectionModel.class );
    private final SimpleObjectProperty< ArrayList< ReferenceSurface< T > > > referenceSurfaces;
    private final SimpleBooleanProperty delta1Functions;
    private final SimpleBooleanProperty delta2Functions;
    private final IntegerProperty delta1;
    private final IntegerProperty delta2;

    private final StringProperty method;


    private final BooleanProperty rawHM;
    private final BooleanProperty projection;
    private final BooleanProperty extractedHM;
    private final BooleanProperty reduced3Dspace;
    private final BooleanProperty segmentedSurface;
    private final BooleanProperty segmentedSurfaceMask;

    private final BooleanProperty rawHMDisplay;
    private final BooleanProperty projectionDisplay;
    private final BooleanProperty extractedHMDisplay;
    private final BooleanProperty reduced3DspaceDisplay;
    private final BooleanProperty segmentedSurfaceDisplay;
    private final BooleanProperty segmentedSurfaceMaskDisplay;

    public ProjectionModel()
    {
        this.referenceSurfaces = new SimpleObjectProperty<>();
        this.delta1Functions = new SimpleBooleanProperty( false );
        this.delta2Functions = new SimpleBooleanProperty( false );
        this.method = new SimpleStringProperty();
        this.delta1 = new SimpleIntegerProperty();
        this.delta2 = new SimpleIntegerProperty();
        this.rawHM = new SimpleBooleanProperty();
        this.projection = new SimpleBooleanProperty();
        this.extractedHM = new SimpleBooleanProperty();
        this.reduced3Dspace = new SimpleBooleanProperty();
        this.segmentedSurface = new SimpleBooleanProperty();
        this.segmentedSurfaceMask = new SimpleBooleanProperty();
        this.rawHMDisplay = new SimpleBooleanProperty( true );
        this.projectionDisplay = new SimpleBooleanProperty( true );
        this.extractedHMDisplay = new SimpleBooleanProperty( true );
        this.reduced3DspaceDisplay = new SimpleBooleanProperty( true );
        this.segmentedSurfaceDisplay = new SimpleBooleanProperty( true );
        this.segmentedSurfaceMaskDisplay = new SimpleBooleanProperty( true );
    }

    public void showSegmentedSurfaceMask()
    {
        for ( ReferenceSurface< T > referenceSurface : referenceSurfaces.getValue() )
        {
            ImageJFunctions.show( referenceSurface.getProjection().getSegmentedSurfaceMask(), "Surface_mask_" + referenceSurface.getIndex() );
        }
    }

    public void showSegmentedSurface()
    {

        for ( ReferenceSurface< T > referenceSurface : referenceSurfaces.getValue() )
        {
            ImageJFunctions.show( referenceSurface.getProjection().getSegmentedSurface(), "Segmented_surface_" + referenceSurface.getIndex() );
        }
    }

    public void showReduced3DSpace()
    {

        for ( ReferenceSurface< T > referenceSurface : referenceSurfaces.getValue() )
        {
            ImageJFunctions.show( referenceSurface.getProjection().getReduced3DSpace(), "Reduced_3D_space_" + referenceSurface.getIndex() );
        }
    }

    public void showExtractedHeightMaps()
    {
        for ( ReferenceSurface< T > referenceSurface : referenceSurfaces.getValue() )
        {
            ImageJFunctions.show( referenceSurface.getProjection().getExtractedHeightMap(), "Extracted_HM_" + referenceSurface.getIndex() );
        }
    }

    public void showRawHeightMap()
    {
        for ( ReferenceSurface< T > referenceSurface : referenceSurfaces.getValue() )
        {
            ImageJFunctions.show( referenceSurface.getzMap(), "Raw_HM_" + referenceSurface.getIndex() );
        }
    }

    public void showProjections()
    {
        for ( ReferenceSurface< T > referenceSurface : referenceSurfaces.getValue() )
        {
            ImageJFunctions.show( referenceSurface.getProjection().get(), "Projection_" + referenceSurface.getIndex() );
        }
    }

    public void runProjection()
    {

        LOGGER.debug( "Starting projections ! " );
        if ( ! delta1Functions.getValue() )
        {
            LOGGER.debug( "Output with delta1 " );
            setOutputDelta1();
        }
        if ( ! delta2Functions.getValue() )
        {
            LOGGER.debug( "Output with delta2 " );
            setOutputDelta2();
        }
        showOutput();
    }

    public void showOutput()
    {
        if ( rawHM.get() && rawHMDisplay.getValue() )
        {
            showRawHeightMap();
            rawHMDisplay.setValue( false );
        }
        if ( projection.get() && projectionDisplay.get() )
        {
            showProjections();
            projectionDisplay.setValue( false );
        }

        if ( extractedHM.get() && extractedHMDisplay.get() )
        {
            showExtractedHeightMaps();
            extractedHMDisplay.setValue( false );
        }
        if ( segmentedSurface.get() && segmentedSurfaceDisplay.get() )
        {
            showSegmentedSurface();
            segmentedSurface.setValue( false );
        }

        if ( reduced3Dspace.get() && reduced3DspaceDisplay.get() )
        {
            showReduced3DSpace();
            reduced3DspaceDisplay.setValue( false );
        }
        if ( segmentedSurfaceMask.get() && segmentedSurfaceMaskDisplay.get() )
        {
            showSegmentedSurfaceMask();
            segmentedSurfaceMask.setValue( false );
        }

    }

    public void setOutputDelta1()
    {

        for ( ReferenceSurface< T > referenceSurface : referenceSurfaces.getValue() )
        {
            Projection< T > projection = referenceSurface.getProjection();
            Img< UnsignedShortType > extractedHeightMap = ReferenceSurfaceProjection.getProjectionHeightMap( referenceSurface, method.get(), delta1.intValue() );
            projection.setExtractedHeightMap( extractedHeightMap );
            projection.setProjection( ReferenceSurfaceProjection.projection1( referenceSurface, this.method.get() ) );

            projection.setSegmentedSurface( ReferenceSurfaceProjection.getSegmentedSurface( extractedHeightMap, referenceSurface.getInput(), referenceSurface.getFactory() ) );
            projection.setReduced3DSpace( ReferenceSurfaceProjection.getExtractedHeightMapSubStack( referenceSurface, delta1.intValue() ) );
        }
        delta1Functions.setValue( true );
    }

    public void setOutputDelta2()
    {

        for ( ReferenceSurface< T > referenceSurface : referenceSurfaces.getValue() )
        {

            Projection< T > projection = referenceSurface.getProjection();
            projection.setSegmentedSurfaceMask( ReferenceSurfaceProjection.getSegmentedSurfaceMask( projection.getSegmentedSurface(), delta2.intValue() ) );

        }
        delta2Functions.setValue( true );
    }


    public ArrayList< ReferenceSurface< T > > getReferenceSurfaces()
    {
        return referenceSurfaces.get();
    }

    public SimpleObjectProperty< ArrayList< ReferenceSurface< T > > > referenceSurfacesProperty()
    {
        return referenceSurfaces;
    }

    public SimpleBooleanProperty delta1FunctionsProperty()
    {
        return delta1Functions;
    }

    public SimpleBooleanProperty delta2FunctionsProperty()
    {
        return delta2Functions;
    }

    public IntegerProperty delta1Property()
    {
        return delta1;
    }

    public IntegerProperty delta2Property()
    {
        return delta2;
    }

    public BooleanProperty rawHMProperty()
    {
        return rawHM;
    }

    public boolean isProjection()
    {
        return projection.get();
    }

    public BooleanProperty projectionProperty()
    {
        return projection;
    }

    public BooleanProperty extractedHMProperty()
    {
        return extractedHM;
    }

    public BooleanProperty reduced3DspaceProperty()
    {
        return reduced3Dspace;
    }

    public BooleanProperty segmentedSurfaceProperty()
    {
        return segmentedSurface;
    }

    public BooleanProperty segmentedSurfaceMaskProperty()
    {
        return segmentedSurfaceMask;
    }

    public String getMethod()
    {
        return method.get();
    }

    public void setMethod( String method )
    {
        this.method.set( method );
    }

    public StringProperty methodProperty()
    {
        return method;
    }


    public BooleanProperty rawHMDisplayProperty()
    {
        return rawHMDisplay;
    }

    public BooleanProperty projectionDisplayProperty()
    {
        return projectionDisplay;
    }

    public BooleanProperty extractedHMDisplayProperty()
    {
        return extractedHMDisplay;
    }


    public BooleanProperty reduced3DspaceDisplayProperty()
    {
        return reduced3DspaceDisplay;
    }

    public BooleanProperty segmentedSurfaceDisplayProperty()
    {
        return segmentedSurfaceDisplay;
    }

    public BooleanProperty segmentedSurfaceMaskDisplayProperty()
    {
        return segmentedSurfaceMaskDisplay;
    }
}
