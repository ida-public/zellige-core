package fr.pasteur.ida.zellige.gui.exception;

public class TimeLapseException extends Exception
{

    @Override
    public String getMessage()
    {
        return "Zellige doesn't handle time-lapse images for now !";
    }
}
