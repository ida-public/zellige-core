/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui;

import javafx.beans.NamedArg;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import java.io.IOException;

public abstract class ParameterSlider extends GridPane
{

    private final String name;
    private final int minorTickUnit;
    private final double majorTickUnit, increment, defaultValue, min, max;
    @FXML
    private Label label;// Contains the parameter name
    @FXML
    private Slider slider;
    @FXML
    private TextField textField;
    private StringProperty userValue;
    private Number previousValue = 1;
    private final String interval;
    private final PseudoClass errorClass = PseudoClass.getPseudoClass( "error" );

    public ParameterSlider( @NamedArg( "name" ) String name,
                            @NamedArg( "min" ) double min,
                            @NamedArg( "max" ) double max,
                            @NamedArg( "major" ) double majorTickUnit,
                            @NamedArg( "minor" ) int minorTickUnit,
                            @NamedArg( "increment" ) double increment,
                            @NamedArg( "default" ) double defaultValue,
                            @NamedArg( "interval" ) String interval )
    {
        this.name = name;
        this.majorTickUnit = majorTickUnit;
        this.minorTickUnit = minorTickUnit;
        this.min = min;
        this.max = max;
        this.increment = increment;
        this.defaultValue = defaultValue;
        this.interval = interval;
        FXMLLoader fxmlLoader = new FXMLLoader( ParameterSlider.class.getClassLoader().getResource( "fr.pasteur.ida.zellige.gui.view/ParameterSlider.fxml" ) );
        fxmlLoader.setRoot( this );
        fxmlLoader.setController( this );
        try
        {
            fxmlLoader.load();
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
    }


    @FXML
    public void initialize()
    {
        this.label.setText( name );
        slider.setMin( min );
        slider.setMax( max );
        slider.setBlockIncrement( increment );
        slider.setValue( defaultValue );
        slider.setShowTickMarks( true );
        slider.setShowTickLabels( true );
        slider.setMajorTickUnit( majorTickUnit );
        slider.setMinorTickCount( minorTickUnit );
        slider.setSnapToTicks( true );
        userValue = textFieldProperty();
        textField.getStyleClass().add( "error" );
        bindProperties();
        listenerSetting();
    }


    /**
     * Allows the user to be notified if the input value is not in the slider range
     */
    public void listenerSetting()
    {
        this.userValueProperty().addListener( event ->
                this.getTextField().pseudoClassStateChanged(
                        this.getErrorClass(), ! this.getTextField().getText().matches( this.getInterval() )
                ) );
    }

    public abstract void bindProperties();


    public boolean hasChanged( Number newValue )
    {
        if ( ! getSlider().isValueChanging() && ! previousValue.equals( newValue ) )
        {
            previousValue = newValue;
            return true;
        }
        return false;
    }


    public void enable()
    {
        this.getSlider().setDisable( false );
        this.getTextField().setDisable( false );
    }

    public void disable()
    {
        this.getSlider().setDisable( true );
        this.getTextField().setDisable( true );
    }

    public StringProperty textFieldProperty()
    {
        return textField.textProperty();
    }


    public PseudoClass getErrorClass()
    {
        return errorClass;
    }

    public TextField getTextField()
    {
        return textField;
    }

    public Slider getSlider()
    {
        return slider;
    }

    public DoubleProperty sliderProperty()
    {
        return getSlider().valueProperty();
    }

    public StringProperty userValueProperty()
    {
        return userValue;
    }

    public String getInterval()
    {
        return interval;
    }

    public void setValue( double value )
    {
        this.sliderProperty().set( value );
    }
}



