/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.model;

import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.gui.ClassifiedImages;
import fr.pasteur.ida.zellige.gui.task.*;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.ImageView;
import net.imglib2.img.Img;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.real.FloatType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SelectionModel
{
    private final static Logger LOGGER = LoggerFactory.getLogger( SelectionModel.class );
    private final DoubleProperty amplitude;
    private final DoubleProperty otsu;
    private final DoubleProperty island;
    private final DoubleProperty xyBlur;
    private final DoubleProperty zBlur;
    private final SimpleBooleanProperty disableGUI = new SimpleBooleanProperty();
    private final SimpleObjectProperty< Img< BitType > > islandSearchImage = new SimpleObjectProperty<>();
    private final SimpleObjectProperty< ClassifiedImages< FloatType > > images = new SimpleObjectProperty<>();
    private final SimpleObjectProperty< Img< FloatType > > pretreatedImg = new SimpleObjectProperty<>();
    private final SimpleObjectProperty< Img< BitType > > selectedAmplitude = new SimpleObjectProperty<>();
    private final SimpleObjectProperty< Img< BitType > > selectedOtsu = new SimpleObjectProperty<>();
    private final SimpleObjectProperty< Img< BitType > > interClassifiedImage = new SimpleObjectProperty<>();
    private final SimpleObjectProperty< Pixels[][] > selectedPixels = new SimpleObjectProperty<>();
    private final SimpleObjectProperty< ImageView[] > imageViews = new SimpleObjectProperty<>();

    public SelectionModel()
    {
        amplitude = new SimpleDoubleProperty();
        otsu = new SimpleDoubleProperty();
        island = new SimpleDoubleProperty();
        xyBlur = new SimpleDoubleProperty();
        zBlur = new SimpleDoubleProperty();
    }

    public void runAmplitudeTask()
    {
        AmplitudeThresholdingTask amplitudeTask = new AmplitudeThresholdingTask( images, amplitude.getValue().intValue() );
        amplitudeTask.setOnSucceeded( workerStateEvent ->
        {
            selectedAmplitude.setValue( amplitudeTask.getValue() );
            disableGUI.setValue( false );
        } );
        amplitudeTask.start();
    }

    public void runOtsuTask()
    {
        OtsuThresholdingTask otsuTask = new OtsuThresholdingTask( pretreatedImg, images, otsu.getValue().intValue() );
        otsuTask.setOnSucceeded( workerStateEvent ->
        {
            selectedOtsu.setValue( otsuTask.getValue() );
            disableGUI.setValue( false );
        } );
        otsuTask.start();
    }

    public void runInterClassification()
    {
        InterClassificationTask interClassificationTask = new InterClassificationTask( selectedAmplitude, selectedOtsu );
        interClassificationTask.setOnSucceeded( event -> interClassifiedImage.setValue( interClassificationTask.getValue() ) );
        interClassificationTask.start();
    }


    public void runIslandSearch()
    {
        IslandSearchTask islandSearchTask = new IslandSearchTask( interClassifiedImage, island );
        islandSearchTask.setOnSucceeded( workerStateEvent ->
                islandSearchImage.setValue( islandSearchTask.getValue() ) );
        islandSearchTask.setOnFailed( workerStateEvent -> LOGGER.debug( "FAILED" ) );
        islandSearchTask.start();
    }

    public void runSmoothing()
    {
        SmoothingTask smoothingTask = new SmoothingTask( islandSearchImage, xyBlur, zBlur );
        smoothingTask.setOnSucceeded( event ->
        {
            selectedPixels.setValue( smoothingTask.getValue() );
            LOGGER.debug( "max is set" );
        } );
        smoothingTask.start();
    }

    public void runPixelSelection()
    {
        if ( islandSearchImage.getValue() == null )
        {
            LOGGER.debug( "Island search image is NULL" );
            runIslandSearch();
        }
        else if ( selectedPixelsProperty().getValue() == null )
        {
            LOGGER.debug( "Island search image is not NULL" );
            runSmoothing();
        }
    }

    public double getAmplitude()
    {
        return amplitude.get();
    }

    public void setAmplitude( double amplitude )
    {
        this.amplitude.set( amplitude );
    }

    public DoubleProperty amplitudeProperty()
    {
        return amplitude;
    }

    public double getOtsu()
    {
        return otsu.get();
    }

    public void setOtsu( double otsu )
    {
        this.otsu.set( otsu );
    }

    public DoubleProperty otsuProperty()
    {
        return otsu;
    }

    public double getIsland()
    {
        return island.get();
    }

    public void setIsland( double island )
    {
        this.island.set( island );
    }

    public DoubleProperty islandProperty()
    {
        return island;
    }

    public DoubleProperty xyBlurProperty()
    {
        return xyBlur;
    }

    public double getzBlur()
    {
        return zBlur.get();
    }


    public DoubleProperty zBlurProperty()
    {
        return zBlur;
    }

    public boolean isDisableGUI()
    {
        return disableGUI.get();
    }

    public void setDisableGUI( boolean disableGUI )
    {
        this.disableGUI.set( disableGUI );
    }

    public SimpleBooleanProperty disableGUIProperty()
    {
        return disableGUI;
    }

    public Pixels[][] getSelectedPixels()
    {
        return selectedPixels.get();
    }

    public void setSelectedPixels( Pixels[][] selectedPixels )
    {
        this.selectedPixels.set( selectedPixels );
    }

    public SimpleObjectProperty< Pixels[][] > selectedPixelsProperty()
    {
        return selectedPixels;
    }

    public ClassifiedImages< FloatType > getImages()
    {
        return images.get();
    }

    public void setImages( ClassifiedImages< FloatType > images )
    {
        this.images.set( images );
    }

    public SimpleObjectProperty< ClassifiedImages< FloatType > > imagesProperty()
    {
        return images;
    }

    public Img< BitType > getIslandSearchImage()
    {
        return islandSearchImage.get();
    }

    public void setIslandSearchImage( Img< BitType > islandSearchImage )
    {
        this.islandSearchImage.set( islandSearchImage );
    }

    public SimpleObjectProperty< Img< BitType > > islandSearchImageProperty()
    {
        return islandSearchImage;
    }

    public Img< FloatType > getPretreatedImg()
    {
        return pretreatedImg.get();
    }

    public void setPretreatedImg( Img< FloatType > pretreatedImg )
    {
        this.pretreatedImg.set( pretreatedImg );
    }

    public SimpleObjectProperty< Img< FloatType > > pretreatedImgProperty()
    {
        return pretreatedImg;
    }

    public Img< BitType > getSelectedAmplitude()
    {
        return selectedAmplitude.get();
    }

    public void setSelectedAmplitude( Img< BitType > selectedAmplitude )
    {
        this.selectedAmplitude.set( selectedAmplitude );
    }

    public SimpleObjectProperty< Img< BitType > > selectedAmplitudeProperty()
    {
        return selectedAmplitude;
    }

    public Img< BitType > getSelectedOtsu()
    {
        return selectedOtsu.get();
    }

    public void setSelectedOtsu( Img< BitType > selectedOtsu )
    {
        this.selectedOtsu.set( selectedOtsu );
    }

    public SimpleObjectProperty< Img< BitType > > selectedOtsuProperty()
    {
        return selectedOtsu;
    }

    public Img< BitType > getInterClassifiedImage()
    {
        return interClassifiedImage.get();
    }

    public void setInterClassifiedImage( Img< BitType > interClassifiedImage )
    {
        this.interClassifiedImage.set( interClassifiedImage );
    }

    public SimpleObjectProperty< Img< BitType > > interClassifiedImageProperty()
    {
        return interClassifiedImage;
    }

    public ImageView[] getImageViews()
    {
        return imageViews.get();
    }

    public SimpleObjectProperty< ImageView[] > imageViewsProperty()
    {
        return imageViews;
    }


}
