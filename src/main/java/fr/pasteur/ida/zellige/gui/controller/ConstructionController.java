/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.controller;

import fr.pasteur.ida.zellige.gui.ParameterSliderDouble;
import fr.pasteur.ida.zellige.gui.ParameterSliderInteger;
import fr.pasteur.ida.zellige.gui.model.ConstructionModel;
import fr.pasteur.ida.zellige.gui.parameter.ZelligeParameters;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class ConstructionController< T extends RealType< T > & NativeType< T > > implements Initializable
{
    private final static Logger LOGGER = LoggerFactory.getLogger( ConstructionController.class );

    private final SimpleBooleanProperty changedParameters = new SimpleBooleanProperty();
    private ConstructionModel< T > constructionModel;
    @FXML
    private ParameterSliderDouble c1;
    @FXML
    private ParameterSliderDouble c2;
    @FXML
    private ParameterSliderInteger r1;
    @FXML
    private ParameterSliderInteger r2;
    @FXML
    private ParameterSliderDouble st1;
    @FXML
    private ParameterSliderDouble st2;
    @FXML
    private ParameterSliderInteger surfaceSize;


    @Override
    public void initialize( URL url, ResourceBundle resourceBundle )
    {
        // model
        constructionModel = new ConstructionModel<>();

        //link Model with View
        constructionModel.c1Property().bind( c1.sliderProperty() );
        constructionModel.r1Property().bind( r1.sliderProperty() );
        constructionModel.st1Property().bind( st1.sliderProperty() );
        constructionModel.c2Property().bind( c2.sliderProperty() );
        constructionModel.r2Property().bind( r2.sliderProperty() );
        constructionModel.st2Property().bind( st2.sliderProperty() );
        constructionModel.surfaceSizeProperty().bind( surfaceSize.sliderProperty() );

        /* Component */
        ChangeListener< ? super Number > firstRoundListener = ( observable, oldValue, newValue ) ->
        {
            constructionModel.firstRoundSurfacesProperty().setValue( null );
            changedParameters.setValue( true );
        };
        c1.sliderProperty().addListener( firstRoundListener );
        r1.sliderProperty().addListener( firstRoundListener );
        st1.sliderProperty().addListener( firstRoundListener );
        surfaceSize.sliderProperty().addListener( firstRoundListener );
        ChangeListener< ? super Number > secondRoundListener = ( observable, oldValue, newValue ) ->
        {
            changedParameters.setValue( true );
            constructionModel.secondRoundSurfacesProperty().setValue( null );
        };
        c2.sliderProperty().addListener( secondRoundListener );
        r2.sliderProperty().addListener( secondRoundListener );
        st2.sliderProperty().addListener( secondRoundListener );
        surfaceSize.sliderProperty().addListener( secondRoundListener );

        /* Properties*/
        constructionModel.maximumsProperty().addListener( ( observable, oldValue, newValue ) ->
        {
            if ( newValue != null )
            {
                LOGGER.debug( "Max is not null" );
                constructionModel.firstRound();
            }
            else
            {
                LOGGER.debug( "MAX is NULL" );
                constructionModel.firstRoundSurfacesProperty().setValue( null );
            }
        } );
        constructionModel.firstRoundSurfacesProperty().addListener( ( observableValue, surfaces, t1 ) ->
        {
            if ( t1 == null )
            {
                LOGGER.debug( "FIRST_ROUND_SURFACE  is NULL" );
                constructionModel.secondRoundSurfacesProperty().setValue( null );
            }
            else
            {
                constructionModel.secondRound();
            }
        } );
        constructionModel.secondRoundSurfacesProperty().addListener( ( observableValue, surfaces, t1 ) ->
        {
            if ( t1 == null )
            {
                LOGGER.debug( "SECOND_ROUND_SURFACE  is NULL" );
                constructionModel.referenceSurfacesProperty().setValue( null );
            }
            else
            {
                constructionModel.constructionCompletion();
            }
        } );
    }


    public ConstructionModel< T > getConstructionModel()
    {
        return constructionModel;
    }

    public SimpleBooleanProperty changedParametersProperty()
    {
        return changedParameters;
    }

    public void setParameters( ZelligeParameters parameters )
    {
        st1.setValue( parameters.getSt1() );
        r1.setValue( parameters.getR1() );
        c1.setValue( parameters.getC1() );
        st2.setValue( parameters.getSt2() );
        r2.setValue( parameters.getR2() );
        c2.setValue( parameters.getC2() );
        surfaceSize.setValue( parameters.getSurfaceSize() );
    }
}
