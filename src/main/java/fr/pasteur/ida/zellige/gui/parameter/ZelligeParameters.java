/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.parameter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ZelligeParameters
{

    //selection
    private final int amplitude;
    private final int otsu;
    private final int island;
    private final int xyBlur;
    private final int zBlur;
    //construction
    private final double c1;
    private final int r1;
    private final double st1;
    private final double c2;
    private final int r2;
    private final double st2;
    private final int surfaceSize;
    //projection
    private final int delta1;
    private final int delta2;
    private final String method;


    public ZelligeParameters( SelectionParameter sp, ConstructionParameter cp, ProjectionParameter pp )
    {
        // selection
        amplitude = sp.getAmplitude();
        otsu = sp.getOtsu();
        island = sp.getIsland();
        xyBlur = sp.getXyBlur();
        zBlur = sp.getzBlur();
        // construction
        c1 = cp.getC1();
        r1 = cp.getR1();
        st1 = cp.getSt1();
        c2 = cp.getC2();
        r2 = cp.getR2();
        st2 = cp.getSt2();
        surfaceSize = cp.getSurfaceSize();
        // projection
        delta1 = pp.getDelta1();
        delta2 = pp.getDelta2();
        method = pp.getMethod();
    }


    public static void serialize( final ZelligeParameters parameters, final File file ) throws IOException
    {
        final Gson gson = new GsonBuilder().setPrettyPrinting().create();
        final String str = gson.toJson( parameters );
        System.out.println( str );
        try ( FileWriter writer = new FileWriter( file ) )
        {
            writer.append( str );
        }
    }

    public static ZelligeParameters deserialize( final File file
    ) throws IOException, SecurityException
    {
        try ( Stream< String > streams = Files.lines( Paths.get( file.getAbsolutePath() ) ) )
        {
            if ( streams != null )
            {
                String s = streams.collect( Collectors.joining( System.lineSeparator() ) );
                final Gson gson = new GsonBuilder().setPrettyPrinting().create();

                return ( s.isEmpty() )
                        ? null
                        : gson.fromJson( s, ZelligeParameters.class );
            }

            return null;
        }
    }

    public int getAmplitude()
    {
        return amplitude;
    }

    public int getOtsu()
    {
        return otsu;
    }

    public int getIsland()
    {
        return island;
    }

    public int getXyBlur()
    {
        return xyBlur;
    }

    public int getzBlur()
    {
        return zBlur;
    }

    public double getC1()
    {
        return c1;
    }

    public int getR1()
    {
        return r1;
    }

    public double getSt1()
    {
        return st1;
    }

    public double getC2()
    {
        return c2;
    }

    public int getR2()
    {
        return r2;
    }

    public double getSt2()
    {
        return st2;
    }

    public double getSurfaceSize()
    {
        return surfaceSize;
    }

    public String getMethod()
    {
        return method;
    }

    public int getDelta1()
    {
        return delta1;
    }

    public int getDelta2()
    {
        return delta2;
    }
}
