/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.controller;

import fr.pasteur.ida.zellige.gui.CheckBoxDisplay;
import fr.pasteur.ida.zellige.gui.model.ProjectionModel;
import fr.pasteur.ida.zellige.gui.parameter.ZelligeParameters;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class ProjectionController< T extends RealType< T > & NativeType< T > > implements Initializable
{

    private final static Logger LOGGER = LoggerFactory.getLogger( ProjectionController.class );
    private final SimpleBooleanProperty changedParameters = new SimpleBooleanProperty();
    @FXML
    private Spinner< Integer > delta1;
    @FXML
    private ComboBox< String > methodComboBox;
    @FXML
    private CheckBoxDisplay extractedHeightMap;
    @FXML
    private CheckBoxDisplay segmentedSurface;
    @FXML
    private CheckBoxDisplay reduced3DSpace;
    @FXML
    private CheckBoxDisplay segmentedSurfaceMask;
    @FXML
    private CheckBoxDisplay projection;
    @FXML
    private CheckBoxDisplay rawHeightMap;
    @FXML
    private Spinner< Integer > delta2;

    private ProjectionModel< T > model;

    @Override
    public void initialize( URL url, ResourceBundle resourceBundle )
    {
        // Model
        model = new ProjectionModel<>();

        //link Model with View
        model.methodProperty().bind( methodComboBox.valueProperty() );
        model.delta1Property().bind( delta1.valueProperty() );
        model.delta2Property().bind( delta2.valueProperty() );
        bindCheckBox( model.rawHMProperty(), model.rawHMDisplayProperty(), rawHeightMap );
        bindCheckBox( model.projectionProperty(), model.projectionDisplayProperty(), projection );
        bindCheckBox( model.extractedHMProperty(), model.extractedHMDisplayProperty(), extractedHeightMap );
        bindCheckBox( model.reduced3DspaceProperty(), model.reduced3DspaceDisplayProperty(), reduced3DSpace );
        bindCheckBox( model.segmentedSurfaceProperty(), model.segmentedSurfaceDisplayProperty(), segmentedSurface );
        bindCheckBox( model.segmentedSurfaceMaskProperty(), model.segmentedSurfaceMaskDisplayProperty(), segmentedSurfaceMask );


        /* Components*/
        SpinnerValueFactory< Integer > valueFactory =
                new SpinnerValueFactory.IntegerSpinnerValueFactory( 1, 30, 0 );
        delta1.setValueFactory( valueFactory );
        ObservableList< String > list = FXCollections.observableArrayList( "MIP", "Mean" );
        methodComboBox.setItems( list );
        methodComboBox.getSelectionModel().selectFirst();
        SpinnerValueFactory< Integer > valueFactory2 =
                new SpinnerValueFactory.IntegerSpinnerValueFactory( 1, 30, 0 );
        delta2.setValueFactory( valueFactory2 );

        delta1.valueProperty().addListener( ( observableValue, integer, t1 ) ->
        {
            model.delta1FunctionsProperty().setValue( false );
            resetDisplayMode();
            changedParameters.setValue( true );
        } );

        delta2.valueProperty().addListener( ( observableValue, integer, t1 ) ->
        {
            model.delta2FunctionsProperty().setValue( false );
            segmentedSurfaceMask.enable();
            changedParameters.setValue( true );
        } );


        projection.selectedProperty().addListener( ( observableValue, aBoolean, t1 ) ->
        {
            if ( model.delta1FunctionsProperty().getValue() && t1 && projection.isNotDisplayed() )
            {

                model.showProjections();
            }
        } );

        rawHeightMap.selectedProperty().addListener( ( observableValue, aBoolean, t1 ) ->
        {
            if ( model.delta1FunctionsProperty().getValue() && t1 && rawHeightMap.isNotDisplayed() )
            {
                model.showRawHeightMap();

            }
        } );
        extractedHeightMap.selectedProperty().addListener( ( observableValue, aBoolean, t1 ) ->
        {
            if ( model.delta1FunctionsProperty().getValue() && t1 && extractedHeightMap.isNotDisplayed() )
            {

                model.showExtractedHeightMaps();
            }
        } );
        reduced3DSpace.selectedProperty().addListener( ( observableValue, aBoolean, t1 ) ->
        {
            if ( model.delta1FunctionsProperty().getValue() && t1 && reduced3DSpace.isNotDisplayed() )
            {

                model.showReduced3DSpace();
            }
        } );
        segmentedSurface.selectedProperty().addListener( ( observableValue, aBoolean, t1 ) ->
        {
            if ( model.delta1FunctionsProperty().getValue() && t1 && segmentedSurface.isNotDisplayed() )
            {

                model.showSegmentedSurface();
            }
        } );
        segmentedSurfaceMask.selectedProperty().addListener( ( observableValue, aBoolean, t1 ) ->
        {
            if ( model.delta2FunctionsProperty().getValue() && t1 && segmentedSurfaceMask.isNotDisplayed() )
            {

                model.showSegmentedSurfaceMask();
            }
        } );

        // Model Properties
        model.referenceSurfacesProperty().addListener( ( observableValue, referenceSurfaces1, t1 ) ->
        {
            if ( t1 == null )
            {
                model.delta1FunctionsProperty().setValue( false );
                LOGGER.debug( "REFERENCE_SURFACES  is NULL" );
            }
            else
            {
                LOGGER.info( "Extraction of {} surfaces", model.referenceSurfacesProperty().getValue().size() );
                LOGGER.debug( "Compute output delta1" );
                model.setOutputDelta1();
                resetDisplayMode();
            }
        } );

        model.delta1FunctionsProperty().addListener( ( observableValue, aBoolean, t1 ) ->
        {
            if ( ! t1 )
            {
                LOGGER.debug( "DELTA1_FUNCTIONS is NULL" );
                model.delta2FunctionsProperty().setValue( false );
            }
            else
            {
                LOGGER.debug( "Compute output delta2" );
                model.setOutputDelta2();
            }
        } );

        model.delta2FunctionsProperty().addListener( ( observable, oldValue, newValue ) ->
        {
            if ( newValue )
            {
                model.showOutput();
            }
            else
            {
                LOGGER.debug( "DELTA2_FUNCTIONS is NULL" );
            }
        } );


    }


    private void resetDisplayMode()
    {
        extractedHeightMap.enable();
        projection.enable();
        segmentedSurface.enable();
        rawHeightMap.enable();
        reduced3DSpace.enable();
        segmentedSurfaceMask.enable();


    }

    public SimpleBooleanProperty changedParametersProperty()
    {
        return changedParameters;
    }

    public ProjectionModel< T > getModel()
    {
        return model;
    }


    private void bindCheckBox( BooleanProperty selected, BooleanProperty notDisplayed, CheckBoxDisplay checkBox )
    {
        selected.bind( checkBox.selectedProperty() );
        notDisplayed.bindBidirectional( checkBox.notDisplayedProperty() );
    }

    public void setParameters( ZelligeParameters parameters )
    {
        delta1.getValueFactory().setValue( parameters.getDelta1() );
        delta2.getValueFactory().setValue( parameters.getDelta2() );
        methodComboBox.getSelectionModel().select( parameters.getMethod() );
    }
}
