/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.controller;

import fr.pasteur.ida.zellige.gui.ParameterSliderInteger;
import fr.pasteur.ida.zellige.gui.ZSlicesSlider;
import fr.pasteur.ida.zellige.gui.model.SelectionModel;
import fr.pasteur.ida.zellige.gui.parameter.ZelligeParameters;
import fr.pasteur.ida.zellige.gui.task.ImageFXDisplayTask;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class SelectionController< T extends RealType< T > & NativeType< T > > implements Initializable
{
    private final static Logger LOGGER = LoggerFactory.getLogger( SelectionController.class );


    private final SimpleBooleanProperty changedParameters = new SimpleBooleanProperty();
    private final SimpleBooleanProperty disableGUI = new SimpleBooleanProperty();
    private SelectionModel selectionModel;

    @FXML
    private ParameterSliderInteger amplitude;
    @FXML
    private ParameterSliderInteger island;
    @FXML
    private ParameterSliderInteger otsu;
    @FXML
    private FlowPane stack;
    @FXML
    private ParameterSliderInteger xyBlur;
    @FXML
    private ParameterSliderInteger zBlur;
    @FXML
    private ZSlicesSlider zSlices;

    @Override
    public void initialize( URL url, ResourceBundle resourceBundle )
    {
        // model
        selectionModel = new SelectionModel();

        //link Model with View
        selectionModel.amplitudeProperty().bind( amplitude.sliderProperty() );
        selectionModel.otsuProperty().bind( otsu.sliderProperty() );
        selectionModel.islandProperty().bind( island.sliderProperty() );
        selectionModel.xyBlurProperty().bind( xyBlur.sliderProperty() );
        selectionModel.zBlurProperty().bind( zBlur.sliderProperty() );
        selectionModel.disableGUIProperty().bindBidirectional( disableGUI );
        /* JavaFx Components */

        /* Amplitude slider initialization */

        amplitude.sliderProperty().addListener( ( observableValue, number, newValue ) ->
        {
            if ( amplitude.hasChanged( newValue ) && selectionModel.selectedAmplitudeProperty().getValue() != null )
            {
                disableGUI.setValue( true );
                selectionModel.runAmplitudeTask();
                changedParameters.setValue( true );
            }
        } );

        /* Otsu slider initialization */
        otsu.sliderProperty().addListener( ( observableValue, number, newValue ) ->
        {
            if ( otsu.hasChanged( newValue ) && selectionModel.selectedOtsuProperty().getValue() != null )
            {
                disableGUI.setValue( true );
                selectionModel.runOtsuTask();
                changedParameters.setValue( true );
            }
        } );

        island.getSlider().valueProperty().addListener( ( observableValue, number, t1 ) ->
        {
            if ( island.hasChanged( t1 ) )
            {
                selectionModel.setIslandSearchImage( null );
                changedParameters.setValue( true );
            }
        } );

        xyBlur.sliderProperty().addListener( ( observable, oldValue, newValue ) ->
        {
            if ( xyBlur.hasChanged( newValue ) )
            {
                selectionModel.setSelectedPixels( null );
                changedParameters.setValue( true );
            }
        } );
        zBlur.sliderProperty().addListener( ( observable, oldValue, newValue ) ->
        {
            if ( zBlur.hasChanged( newValue ) )
            {
                selectionModel.setSelectedPixels( null );
                changedParameters.setValue( true );
            }
        } );
        stack.setAlignment( Pos.CENTER );

        // section Slider initialization
        zSlices.getSlider().valueProperty().addListener( ( observableValue, number, newValue ) ->
        {
            int newZValue = newValue.intValue();
            if ( zSlices.hasChanged( newZValue ) )
            {
                refreshDisplay( newZValue );
            }
        } );

        /* Properties */

        disableGUI.addListener( ( observable, oldValue, newValue ) ->
        {
            if ( newValue )
            {
                disableParameters();
            }
            else
            {
                enableParameters();
            }
        } );
        selectionModel.imagesProperty().addListener( ( observableValue, classifiedImages, t1 ) ->
        {
            if ( t1 != null )
            {
                selectionModel.runAmplitudeTask();
                selectionModel.runOtsuTask();
            }
            else
            {
                LOGGER.debug( "IMAGES is NULL" );
                selectionModel.selectedAmplitudeProperty().setValue( null );
                selectionModel.selectedOtsuProperty().setValue( null );
                selectionModel.interClassifiedImageProperty().setValue( null );
            }

        } );
        ChangeListener< Img< BitType > > listener = ( observableValue, bitTypeClassifiedImages, t1 ) ->
        {
            if ( selectionModel.selectedAmplitudeProperty().getValue() != null && selectionModel.selectedOtsuProperty().getValue() != null )
            {
                selectionModel.runInterClassification();
                LOGGER.debug( "Inter classification" );
            }
            else
            {
                LOGGER.debug( "SELECTED_AMPLITUDE or SELECTED_OTSU is NULL" );
                selectionModel.interClassifiedImageProperty().setValue( null );
            }

        };

        selectionModel.selectedAmplitudeProperty().addListener( listener );
        selectionModel.selectedOtsuProperty().addListener( listener );


        selectionModel.interClassifiedImageProperty().addListener( ( observable, oldValue, newValue ) ->
        {
            LOGGER.info( "Computing double classification..." );
            if ( newValue != null )
            {
                computeImageFXDisplay();
            }
            selectionModel.islandSearchImageProperty().setValue( null );
        } );

        selectionModel.islandSearchImageProperty().addListener( ( observableValue, bitTypes, t1 ) ->
        {
            if ( t1 == null )
            {
                LOGGER.debug( "ISLAND_SEARCH is NULL" );
                selectionModel.selectedPixelsProperty().setValue( null );
            }
            else
            {
                selectionModel.runSmoothing();
            }
        } );
    }


    public void computeImageFXDisplay()
    {
        ImageFXDisplayTask imageFXDisplayTask = new ImageFXDisplayTask( selectionModel.interClassifiedImageProperty() );
        imageFXDisplayTask.setOnSucceeded( workerStateEvent ->
        {
            LOGGER.info( "Double classification done." );
            selectionModel.imageViewsProperty().setValue( imageFXDisplayTask.getValue() );
            setDisplay();
        } );
        imageFXDisplayTask.start();
    }

    public void setDisplay()
    {
        stack.getChildren().clear();
        ImageView i = selectionModel.imageViewsProperty().getValue()[ zSlices.getUserValue() ];

        stack.getChildren().add( i );

        // setting of ZSlider
        this.setSliceNumber( ( int ) selectionModel.pretreatedImgProperty().getValue().dimension( 2 ) );
        int width = ( int ) i.boundsInParentProperty().get().getWidth();
        this.zSlices.getSlider().setMaxWidth( width );
        this.zSlices.getSlider().setVisible( true );
        disableGUI.setValue( false );
    }


    /**
     * Displays the slice of the classified stack according to the value of the Z Slider.
     *
     * @param value the z value
     */
    public void refreshDisplay( int value )
    {
        this.stack.getChildren().clear();// Removal of previous slice
        ImageView i = selectionModel.imageViewsProperty().getValue()[ value ];
        stack.getChildren().add( i ); // display of selected z slice
    }

    public void setSliceNumber( int depth )
    {
        zSlices.setWidth( depth - 1 );
    }

    public void set()
    {
        this.setSliceNumber( ( int ) selectionModel.pretreatedImgProperty().getValue().dimension( 2 ) );
    }

    public void enableParameters()
    {
        this.amplitude.enable();
        this.otsu.enable();

    }

    public void disableParameters()
    {
        this.amplitude.disable();
        this.otsu.disable();
    }


    public ParameterSliderInteger getAmplitude()
    {
        return amplitude;
    }

    public ParameterSliderInteger getIsland()
    {
        return island;
    }

    public ParameterSliderInteger getOtsu()
    {
        return otsu;
    }

    public ParameterSliderInteger getXyBlur()
    {
        return xyBlur;
    }

    public ParameterSliderInteger getzBlur()
    {
        return zBlur;
    }

    public boolean isChangedParameters()
    {
        return changedParameters.get();
    }

    public SimpleBooleanProperty changedParametersProperty()
    {
        return changedParameters;
    }

    public SimpleBooleanProperty disableGUIProperty()
    {
        return disableGUI;
    }

    public SelectionModel getSelectionModel()
    {
        return selectionModel;
    }

    public void setParameters( ZelligeParameters parameters )
    {
        amplitude.setValue( parameters.getAmplitude() );
        otsu.setValue( parameters.getOtsu() );
        island.setValue( parameters.getIsland() );
        xyBlur.setValue( parameters.getXyBlur() );
        zBlur.setValue( parameters.getzBlur() );
    }
}
