/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.parameter;

import javafx.beans.property.DoubleProperty;

public class SelectionParameter
{
    private final int amplitude;
    private final int otsu;
    private final int island;
    private final int xyBlur;
    private final int zBlur;

    public SelectionParameter( DoubleProperty amplitude, DoubleProperty otsu, DoubleProperty island, DoubleProperty xyBlur, DoubleProperty zBlur )
    {
        this.amplitude = amplitude.intValue();
        this.otsu = otsu.intValue();
        this.island = island.intValue();
        this.xyBlur = xyBlur.intValue();
        this.zBlur = zBlur.intValue();
    }

    public int getAmplitude()
    {
        return amplitude;
    }

    public int getOtsu()
    {
        return otsu;
    }

    public int getIsland()
    {
        return island;
    }

    public int getXyBlur()
    {
        return xyBlur;
    }

    public int getzBlur()
    {
        return zBlur;
    }
}
