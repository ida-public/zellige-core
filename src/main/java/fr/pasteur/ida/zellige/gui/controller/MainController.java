/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.controller;

import fr.pasteur.ida.zellige.gui.ClassifiedImages;
import fr.pasteur.ida.zellige.gui.LogAppender;
import fr.pasteur.ida.zellige.gui.MainAppFrame;
import fr.pasteur.ida.zellige.gui.exception.NoA3DStackException;
import fr.pasteur.ida.zellige.gui.exception.NoInputException;
import fr.pasteur.ida.zellige.gui.exception.TimeLapseException;
import fr.pasteur.ida.zellige.gui.model.ConstructionModel;
import fr.pasteur.ida.zellige.gui.model.MainModel;
import fr.pasteur.ida.zellige.gui.model.ProjectionModel;
import fr.pasteur.ida.zellige.gui.model.SelectionModel;
import fr.pasteur.ida.zellige.gui.parameter.ConstructionParameter;
import fr.pasteur.ida.zellige.gui.parameter.ProjectionParameter;
import fr.pasteur.ida.zellige.gui.parameter.SelectionParameter;
import fr.pasteur.ida.zellige.gui.parameter.ZelligeParameters;
import fr.pasteur.ida.zellige.gui.task.AbstractTask;
import fr.pasteur.ida.zellige.gui.task.ComputeClassificationImagesTask;
import fr.pasteur.ida.zellige.gui.task.DisplayDatasetChoicesTask;
import fr.pasteur.ida.zellige.gui.task.PretreatmentTask;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import net.imagej.Dataset;
import net.imagej.ImgPlus;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

public class MainController< T extends RealType< T > & NativeType< T > > implements Initializable
{

    private final static Logger LOGGER = LoggerFactory.getLogger( MainController.class );
    private final SimpleObjectProperty< Dataset > currentDataset = new SimpleObjectProperty<>();
    private final SimpleObjectProperty< ClassifiedImages< FloatType > > images = new SimpleObjectProperty<>();
    private final SimpleObjectProperty< Img< FloatType > > pretreatedImg = new SimpleObjectProperty<>();
    private final SimpleBooleanProperty disableGUI = new SimpleBooleanProperty();
    private final SimpleBooleanProperty changedParameters = new SimpleBooleanProperty();
    @FXML
    ComboBox< Dataset > activeDatasets;
    @FXML
    private Button runButton;
    @FXML
    private ConstructionController< T > constructionController;

    @FXML
    private Label logInfo;
    private MainAppFrame mainAppFrame;
    @FXML
    private ProjectionController< T > projectionController;
    @FXML
    private SelectionController< T > selectionController;


    MainModel< T > model;

    public static void showError( Exception exception )
    {
        Alert alert = new Alert( Alert.AlertType.ERROR );
        alert.setTitle( "Error alert" );
        alert.setHeaderText( exception.getMessage() );
        alert.showAndWait();
       // throw new RuntimeException(exception);
    }

    @Override
    public void initialize( URL url, ResourceBundle resourceBundle )
    {
        model = new MainModel<>( selectionController.getSelectionModel(), constructionController.getConstructionModel(), projectionController.getModel() );
        /* Control of the files comboBox*/
        activeDatasets.setOnMousePressed( event ->
                setAndDisplayDatasetChoices() );// Update of the list of opened Fiji images

        activeDatasets.getSelectionModel().selectedItemProperty().addListener( ( observableValue, oldValue, newValue ) ->
        {
            LOGGER.debug( "selectedItemProperty : old value = {}, new value = {} ",  oldValue, newValue );
            if(newValue!= null && isValid( newValue ))
            {

                    setCurrentDataset( newValue );
            }
            else
            {
                activeDatasets.getSelectionModel().selectLast();
            }
        } );

        currentDataset.addListener( ( observableValue, dataset, t1 ) ->
        {
            if ( t1 != null )
            {
                selectionController.disableParameters();
                disableGUI.setValue( true );
                runPretreatment( currentDataset.getValue() );
            }
            else
            {
                pretreatedImg.setValue( null );
                LOGGER.debug( "PRETREATED_IMAGE is NULL" );
            }
        } );

        pretreatedImg.addListener( ( observable, oldValue, newValue ) ->
        {
            if ( newValue != null )
            {
                computeClassifiedImages();
            }
            else
            {
                images.setValue( null );
                LOGGER.debug( "IMAGES is NULL" );
            }
        } );

        disableGUI.addListener( ( observable, oldValue, newValue ) ->
                runButton.setDisable( newValue ) );

        // run button initialization
        runButton.setOnAction( actionEvent ->
        {
            disableGUI.setValue( true );
            changedParameters.setValue( false );
            run2();
        } );

        /* Binding properties*/
        selectionController.getSelectionModel().pretreatedImgProperty().bind( pretreatedImg );
        selectionController.getSelectionModel().imagesProperty().bind( images );
        constructionController.getConstructionModel().maximumsProperty().bind( selectionController.getSelectionModel().selectedPixelsProperty() );
        projectionController.getModel().referenceSurfacesProperty().bind( constructionController.getConstructionModel().referenceSurfacesProperty() );
        changedParameters.bindBidirectional( selectionController.changedParametersProperty() );
        selectionController.changedParametersProperty().bindBidirectional( constructionController.changedParametersProperty() );
        constructionController.changedParametersProperty().bindBidirectional( projectionController.changedParametersProperty() );
        disableGUI.bindBidirectional( selectionController.disableGUIProperty() );

        changedParameters.addListener( ( observable, oldValue, newValue ) ->
        {
            LOGGER.debug( "Parameter change" );
            if ( newValue )
            {
                LOGGER.debug( "Active button" );
                disableGUI.setValue( false );

            }
        } );

        // Update of the program status using LOG.INFO outputs
        LogAppender.logMsgProperty().addListener( ( observable, oldValue, newValue ) ->
                Platform.runLater(
                        () ->
                                logInfo.setText( LogAppender.getLogMessage() ) ) );
    }

    public void initExtraction()
    {

        LOGGER.debug( "Init Extraction" );
        Dataset dataset = mainAppFrame.getImage().getActiveDataset();
        LOGGER.debug( dataset.getImgPlus().firstElement().getClass().toGenericString() );
        LOGGER.debug( "Function getFrames() :{}", dataset.getFrames() );
        LOGGER.debug( "Properties :{}", dataset.getImgPlus().getProperties() );
        if ( dataset == null )
        {
            showError( new NoInputException() );
            return;
        }
        activeDatasets.getItems().add( dataset );
        LOGGER.debug( "Init Extraction completed" );
    }


    public void run2()
    {

        if ( selectionController.getSelectionModel().selectedPixelsProperty().getValue() != null )
        {
            if ( constructionController.getConstructionModel().referenceSurfacesProperty().getValue() != null )
            {
                projectionController.getModel().runProjection();
            }
            else
            {
                constructionController.getConstructionModel().runConstruction();
            }
        }
        else
        {
            selectionController.getSelectionModel().runPixelSelection();
        }
        LOGGER.debug( "###########################################---NEW RUN---###########################################" );
    }

    private void runPretreatment( Dataset dataset )
    {
        AbstractTask< Img< FloatType > > task = new PretreatmentTask<>( dataset );
        task.setOnSucceeded( workerStateEvent ->
                pretreatedImg.setValue( task.getValue() ) );
        task.start();
    }

    public void setAndDisplayDatasetChoices()
    {
        DisplayDatasetChoicesTask displayTask = new DisplayDatasetChoicesTask( mainAppFrame.getImage() );
        displayTask.setOnSucceeded( workerStateEvent ->
        {
            LOGGER.debug( "Datasets displayed successfully" );
            ObservableList< Dataset > list = FXCollections.observableArrayList( displayTask.getValue() );
            list.add( null );
            activeDatasets.setItems( list );

            LOGGER.debug( "item selected : list size = " + list.size() );
            if ( list.isEmpty() )
            {
                currentDataset.setValue( null );
                LOGGER.debug( "CURRENT_DATASET is NULL" );
                showError( new NoInputException() );
            }
        } );
        displayTask.start();
    }

    @SuppressWarnings( "unchecked" )
    public void setCurrentDataset( Dataset dataset )
    {
                currentDataset.setValue( dataset );
                Img< T > input = ( ImgPlus< T > ) dataset.getImgPlus();
                constructionController.getConstructionModel().setInput( input );
                constructionController.getConstructionModel().setFactory( input.factory() );

    }

    public void computeClassifiedImages()
    {
        ComputeClassificationImagesTask classifiedImagesTask = new ComputeClassificationImagesTask( pretreatedImg.getValue() );
        classifiedImagesTask.setOnSucceeded( workerStateEvent -> images.set( classifiedImagesTask.getValue() ) );
        classifiedImagesTask.start();
    }

    public void setMainApp( MainAppFrame mainAppFrame )
    {
        this.mainAppFrame = mainAppFrame;
    }

    public int getAmplitude()
    {
        return ( int ) selectionController.getAmplitude().getSlider().getValue();
    }

    public int getOtsu()
    {
        return ( int ) selectionController.getOtsu().getSlider().getValue();
    }

    public int getIsland()
    {
        return ( int ) selectionController.getIsland().getSlider().getValue();
    }

    @FXML
    public void saveParameters() throws IOException
    {
        SimpleDateFormat dtf = new SimpleDateFormat( "yyyyMMdd_HHmmss" );
        FileChooser fileChooser = chooseAFile( dtf.format( new Date() ) + "_zellige_param", "Sava Parameters..." );
        File file = fileChooser.showSaveDialog( null );
        if ( file != null )
        {
            SelectionParameter sp = build( model.getSelectionModel() );
            ConstructionParameter cp = build( model.getConstructionModel() );
            ProjectionParameter pp = build( model.getProjectionModel() );

            ZelligeParameters.serialize( new ZelligeParameters( sp, cp, pp ), file );
        }
    }


    private SelectionParameter build( SelectionModel model )
    {
        return new SelectionParameter( model.amplitudeProperty(), model.otsuProperty(), model.islandProperty(), model.xyBlurProperty(), model.zBlurProperty() );
    }

    private ConstructionParameter build( ConstructionModel< T > model )
    {
        return new ConstructionParameter( model.c1Property(), model.r1Property(), model.st1Property(), model.c2Property(), model.r2Property(), model.st2Property(), model.surfaceSizeProperty() );
    }

    private ProjectionParameter build( ProjectionModel< T > model )
    {
        return new ProjectionParameter( model.delta1Property(), model.delta2Property(), model.methodProperty() );
    }

    @FXML
    public void loadParameters() throws IOException
    {
        File f = chooseFile();
        LOGGER.debug( "The file has been choose" );
        if ( f != null )
        {
            ZelligeParameters parameters = ZelligeParameters.deserialize( f );
            setParameters( parameters );
        }
    }

    private File chooseFile()
    {
        FileChooser fileChooser = chooseAFile( null, "Select a Zellige parameter file"
        );
        return fileChooser.showOpenDialog( null );
    }

    private FileChooser chooseAFile( String initialFileNme, String title )
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle( title );
        fileChooser.setInitialFileName( initialFileNme );
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter( "ZelligeParameters", "*.json" );
        fileChooser.getExtensionFilters().add( extFilter );
        return fileChooser;
    }

    private void setParameters( ZelligeParameters parameters )
    {
        selectionController.setParameters( parameters );
        constructionController.setParameters( parameters );
        projectionController.setParameters( parameters );
    }

    private boolean isValid(Dataset dataset)
    {
        int numZ = ( int ) dataset.getDepth();
        int numFrames = ( int ) dataset.getFrames();
        LOGGER.debug( "NUmber of dimensions: {}", dataset.numDimensions() );
        LOGGER.debug( "NUmber of channels: {}", dataset.getChannels() );
        LOGGER.debug( "Number of frames: {}", dataset.getFrames() );
        if ( numFrames > 1 )
        {
            LOGGER.debug( "TimeLapseException" );
            showError( new TimeLapseException() );
            return false;
        }
        if ( numZ == 1 )
        {
            LOGGER.debug( "NoA3DStackException" );
            showError( new NoA3DStackException() );
            return false;
        }
        return true;
    }
}

