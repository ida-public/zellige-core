/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.task;

import fr.pasteur.ida.zellige.element.Surface;
import fr.pasteur.ida.zellige.steps.construction.exception.NoSurfaceFoundException;
import fr.pasteur.ida.zellige.steps.construction.rounds.SecondRoundConstruction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class RunSecondConstructionTask extends AbstractTask< ArrayList< Surface > >
{
    private final static Logger LOGGER = LoggerFactory.getLogger( RunSecondConstructionTask.class );

    private final ArrayList< Surface > surfaces;
    private final double startingOsSize2;
    private final int overlap2;
    private final double connexityRate2;
    private final double surfaceMinSizeFactor;

    public RunSecondConstructionTask( ArrayList< Surface > surfaces, double startingOsSize2, int overlap2, double connexityRate2, double surfaceMinSizeFactor )
    {
        this.surfaces = surfaces;
        this.startingOsSize2 = startingOsSize2;
        this.overlap2 = overlap2;
        this.connexityRate2 = connexityRate2;
        this.surfaceMinSizeFactor = surfaceMinSizeFactor;
    }

    @Override
    protected ArrayList< Surface > call()
    {
        LOGGER.info( "Computing second round construction..." );
        SecondRoundConstruction step2 = new SecondRoundConstruction( surfaces, startingOsSize2, overlap2, connexityRate2, surfaceMinSizeFactor );
        try
        {
            step2.process();
        }
        catch ( NoSurfaceFoundException e )
        {

            return null;
        }
        return step2.getSurfaces();
    }


}
