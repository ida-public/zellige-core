/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.task;

import fr.pasteur.ida.zellige.gui.ClassifiedImages;
import fr.pasteur.ida.zellige.steps.selection.classification.AmplitudeClassification;
import fr.pasteur.ida.zellige.steps.selection.classification.OtsuClassification;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.numeric.real.FloatType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ComputeClassificationImagesTask extends AbstractTask< ClassifiedImages< FloatType > >
{

    private final static Logger LOGGER = LoggerFactory.getLogger( ComputeClassificationImagesTask.class );
    private final Img< FloatType > pretreatedImg;


    public ComputeClassificationImagesTask( Img< FloatType > pretreatedImg )
    {
        this.pretreatedImg = pretreatedImg;
    }

    @Override
    protected ClassifiedImages< FloatType > call() throws Exception
    {
        LOGGER.info( "Computing classified images..." );
        ImgFactory< FloatType > factory = new ArrayImgFactory<>( new FloatType() );
        ClassifiedImages< FloatType > images = new ClassifiedImages<>();
        images.setAmplitudeImg( AmplitudeClassification.computeAmplitudeImage( pretreatedImg, factory ) );
        images.setOtsuImg( OtsuClassification.computeOtsuImage( pretreatedImg, factory ) );
        LOGGER.debug( "Classification images computed" );
        return images;
    }
}
