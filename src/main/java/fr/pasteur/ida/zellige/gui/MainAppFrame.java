/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import fr.pasteur.ida.zellige.gui.controller.MainController;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import net.imagej.ImageJ;
import net.imagej.display.ImageDisplayService;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import org.scijava.Context;
import org.scijava.log.LogService;
import org.scijava.plugin.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;

public class MainAppFrame extends JFrame
{

    private final static Logger LOGGER = LoggerFactory.getLogger( MainAppFrame.class );
    private ImageJ ij;
    private final ImageDisplayService image;
    @Parameter
    private final LogService logService;
    private JFXPanel fxPanel;
    private Scene scene;

    private static final List< Image > ICONS;

    static
    {
        ICONS = new ArrayList<>();
        final int[] sizes = new int[]{ 16, 32, 64, 128, 256 };
        final ImageIcon imageIcon = new ImageIcon( Objects.requireNonNull( MainAppFrame.class.getClassLoader().getResource( "icons/Zellige_logo.png" ) ) );
//        final ImageIcon imageIcon = new ImageIcon( "src/main/resources/icons/Zellige_logo.png" );
        ICONS.add( imageIcon.getImage() );
        for ( final int size : sizes )
        {
            ICONS.add( scaleImage( imageIcon, size, size ).getImage() );
        }
    }


    /**
     * Creates a new LogAppender to display the program state in the GUI.
     */
    public void setLogStatus()
    {
        LoggerContext lc = ( LoggerContext ) LoggerFactory.getILoggerFactory();
        PatternLayoutEncoder ple = new PatternLayoutEncoder();
        ple.setPattern( "%msg%n" );
        ple.setContext( lc );
        ple.start();
        LogAppender appender = new LogAppender();

        appender.setContext( lc );
        appender.start();
        ch.qos.logback.classic.Logger logbackLogger =
                ( ch.qos.logback.classic.Logger ) LoggerFactory.getLogger( Logger.ROOT_LOGGER_NAME );
        logbackLogger.addAppender( appender );
    }

    public MainAppFrame( Context context )
    {
//        this.ij = context.;
        this.image = context.getService( ImageDisplayService.class );
        this.logService = context.getService( LogService.class );
        this.setIconImages( ICONS );
    }

    public static ImageIcon scaleImage( final ImageIcon icon, final int w, final int h )
    {
        int nw = icon.getIconWidth();
        int nh = icon.getIconHeight();

        if ( icon.getIconWidth() > w )
        {
            nw = w;
            nh = ( nw * icon.getIconHeight() ) / icon.getIconWidth();
        }

        if ( nh > h )
        {
            nh = h;
            nw = ( icon.getIconWidth() * nh ) / icon.getIconHeight();
        }
        return new ImageIcon( icon.getImage().getScaledInstance( nw, nh, Image.SCALE_SMOOTH ) );
    }


    public ImageDisplayService getImage()
    {
        return image;
    }

    public Scene getScene()
    {
        return scene;
    }

    /**
     * Create the JFXPanel that make the link between Swing (IJ) and JavaFX
     * plugin.
     */
    public void init()
    {

        this.fxPanel = new JFXPanel();
        this.add( this.fxPanel );
        this.setVisible( true );
        setLogStatus();
        // The call to runLater() avoid a mix between JavaFX thread and Swing thread.
        Platform.runLater( this::initFX );
    }

    public < T extends RealType< T > & NativeType< T > > void initFX()
    {
        // Init the root layout
        try
        {
            ResourceBundle bundle = ResourceBundle.getBundle( "gui" );// access to properties f
            FXMLLoader loader = new FXMLLoader( MainAppFrame.class.getClassLoader().getResource( "fr.pasteur.ida.zellige.gui.view/Main.fxml" ), bundle );

            VBox rootLayout = loader.load();

            // Get the controller and add an ImageJ context to it.
            MainController< T > controller = loader.getController();
            controller.setMainApp( this );


            // Show the scene containing the root layout.
            scene = new Scene( rootLayout );
            this.fxPanel.setScene( scene );

            // Resize the JFrame to the JavaFX scene
            this.setSize( ( int ) scene.getWidth() + 20, ( int ) scene.getHeight() + 50 );
            controller.initExtraction();
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
    }


}
