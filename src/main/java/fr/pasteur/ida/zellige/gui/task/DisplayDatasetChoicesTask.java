/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.task;

import net.imagej.Dataset;
import net.imagej.display.ImageDisplay;
import net.imagej.display.ImageDisplayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class DisplayDatasetChoicesTask extends AbstractTask< ArrayList< Dataset > >
{
    private final static Logger LOGGER = LoggerFactory.getLogger( DisplayDatasetChoicesTask.class );
    private final ImageDisplayService imageDisplayService;

    public DisplayDatasetChoicesTask( ImageDisplayService imageDisplayService )
    {
        this.imageDisplayService = imageDisplayService;
    }

    @Override
    protected ArrayList< Dataset > call()
    {
        ArrayList< ImageDisplay > imageDisplays = ( ArrayList< ImageDisplay > ) imageDisplayService.getImageDisplays();
        ArrayList< Dataset > tempDatasets = new ArrayList<>( imageDisplays.size() );
        for ( ImageDisplay imageDisplay : imageDisplays )
        {
            tempDatasets.add( imageDisplayService.getActiveDataset( imageDisplay ) );
        }
        LOGGER.debug( "Available datasets updated." );
        return tempDatasets;
    }
}
