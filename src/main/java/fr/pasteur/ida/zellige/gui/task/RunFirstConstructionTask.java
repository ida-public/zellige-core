/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.gui.task;

import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.element.Surface;
import fr.pasteur.ida.zellige.steps.construction.exception.NoSurfaceFoundException;
import fr.pasteur.ida.zellige.steps.construction.rounds.FirstRoundConstruction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class RunFirstConstructionTask extends AbstractTask< ArrayList< Surface > >
{
    private final static Logger LOGGER = LoggerFactory.getLogger( RunFirstConstructionTask.class );
    private final Pixels[][] maximums;
    private final double startingOsSize1;
    private final int overlap1;
    private final double connexityRate1;
    private final double surfaceMinSizeFactor;

    public RunFirstConstructionTask( Pixels[][] maximums, double startingOsSize1, int overlap1, double connexityRate1, double surfaceMinSizeFactor )
    {
        this.maximums = maximums;
        this.startingOsSize1 = startingOsSize1;
        this.overlap1 = overlap1;
        this.connexityRate1 = connexityRate1;
        this.surfaceMinSizeFactor = surfaceMinSizeFactor;
    }

    @Override
    protected ArrayList< Surface > call()
    {
        LOGGER.info( "Computing first round construction..." );
        FirstRoundConstruction step1 = new FirstRoundConstruction( maximums, startingOsSize1, overlap1, connexityRate1, surfaceMinSizeFactor );
        try
        {
            step1.process();
        }
        catch ( NoSurfaceFoundException e )
        {

            return null;
        }
        return step1.getSurfaces();

    }


}
