/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.construction.rounds.ose;

import fr.pasteur.ida.zellige.element.Coordinate;
import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.element.ose.AbstractOSE;
import fr.pasteur.ida.zellige.element.ose.OSEList;
import fr.pasteur.ida.zellige.element.ose.OSEListArray;
import fr.pasteur.ida.zellige.element.ose.OseYZ;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class OseConstructionYZ extends OseConstruction
{
    private final static Logger LOGGER = LoggerFactory.getLogger( OseConstructionYZ.class );
    public OseConstructionYZ( Pixels[][] maximums, double threshold )
    {
        super( maximums, threshold );
    }


    public static OSEListArray run( Pixels[][] maximums, double threshold )
    {
        LOGGER.debug( "Starting YZ construction" );
        OseConstructionYZ constructionYZ = new OseConstructionYZ( maximums, threshold );
        constructionYZ.reset();
        constructionYZ.processAllSection();

        return constructionYZ.getOseListArray();
    }

    @Override
    public OSEList findSimplePaths( ArrayList< Coordinate > startingCoordinates, Pixels[] rawCoordinates )
    {
        Queue< Coordinate > firstQueue = new LinkedList<>( startingCoordinates );// Add the coordinates with no left coordinates in
        // the queue
        OSEList smallPath = new OSEList();
        while ( ! firstQueue.isEmpty() )
        {
            Coordinate current = firstQueue.remove();
            AbstractOSE ose = new OseYZ( this.getStartingStatus() );
            ose.add( current );
            findSimplePaths( rawCoordinates, smallPath, ose, current, firstQueue );
        }
        return smallPath;
    }

    /**
     * Resets the parameters of each {@link Coordinate } for a given {@link Pixels} array
     * before the Y dimension reconstruction ( rightsNumber , leftsNumber, noLeft, queue ).
     *
     */
    private  void reset()
    {

        for ( Pixels [] pixelLine : this.getMaximums() )
        {
            for ( Pixels pixels : pixelLine )
            {
                if ( pixels != null )
                {
                    pixels.resetSideCoordinate();
                }
            }
        }
        LOGGER.debug( "Ose reset" );
    }
}
