/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps;

import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessible;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.gauss3.Gauss3;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;



/**
 * Some "utils" methods.
 */
public class Utils
{

    /**
     * Method to apply a two-dimensional Gaussian filter to a {@link RandomAccessibleInterval}.
     * The parameter sigma gives the standard deviation for each dimension.
     * The filtered {@link RandomAccessibleInterval} is given as a parameter.
     *
     * @param <T>    - source and output type
     * @param input  -  the source image as a {@link RandomAccessibleInterval}
     * @param output - the filtered image as a {@link RandomAccessibleInterval}
     * @param sigma  - an array containing the standard deviations for each dimension.
     */
    public static < T extends RealType< T > & NativeType< T > > void
    gaussianConvolution( RandomAccessibleInterval< T > input, RandomAccessibleInterval< T > output,
                         double[] sigma )
    {
        RandomAccessible< T > infiniteInput = Views.extendValue( input, input.randomAccess().get().getRealFloat() );
        Gauss3.gauss( sigma, infiniteInput, output );
    }


    public static < T extends RealType< T > & NativeType< T > > void setPosition( RandomAccess< T > randomAccess, int u, int v )
    {
        randomAccess.setPosition( u, 0 );
        randomAccess.setPosition( v, 1 );
    }

    public static < T extends RealType< T > & NativeType< T > > void setPosition( RandomAccess< T > randomAccess, int u, int v, int z )
    {
        setPosition( randomAccess, u, v );
        randomAccess.setPosition( z, 2 );
    }


    public static < T extends RealType< T > & NativeType< T > > T setPositionAndGet( RandomAccess< T > randomAccess, int u, int v )
    {
        setPosition( randomAccess, u, v );
        return randomAccess.get();
    }

    public static < T extends RealType< T > & NativeType< T > > T setPositionAndGet( RandomAccess< T > randomAccess, int u, int v, int z )
    {
        setPosition( randomAccess, u, v, z );
        return randomAccess.get();
    }
}
