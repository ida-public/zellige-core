/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.construction.rounds.surface;

import fr.pasteur.ida.zellige.element.Surface;
import fr.pasteur.ida.zellige.element.ose.AbstractOSE;
import fr.pasteur.ida.zellige.element.ose.OSEList;
import fr.pasteur.ida.zellige.element.ose.OSEListArray;
import fr.pasteur.ida.zellige.element.surfaceLine.SurfaceLine;


public class SurfaceConstruction
{

    private final OSEListArray oseLists;
    private final AbstractOSE firstOSE;
    private final Surface surface;
    private final int overlap;
    private final double connexity;


    public SurfaceConstruction( OSEListArray oseListArray, AbstractOSE firstOSE, int width, int overlap, double connexity )
    {
        this.oseLists = oseListArray;
        this.firstOSE = firstOSE;
        int height = oseListArray.getLength();
        this.surface = new Surface( width, height );
        this.overlap = overlap;
        this.connexity = connexity;
    }

    public static Surface run( OSEListArray oseListArray, AbstractOSE firstOSE, int width, int overlap, double connexity )
    {
        SurfaceConstruction construction = new SurfaceConstruction( oseListArray, firstOSE, width, overlap, connexity );
        construction.set();
        construction.build();
        return construction.surface;
    }

    private void set()
    {
        firstOSE.setFirstOSE( surface );
    }

    private void build()
    {
        /* Sweep until no more OSE can be added to the current surface.*/
        int size = 0;
        while ( size != surface.getSize() )
        {
            size = surface.getSize();
            for ( int i = surface.getHeight() - 1; i > 0; i-- )
            {
                if ( surface.get( i ) != null )
                {
                    searchBackward( surface.get( i ) );
                }
            }
            for ( int i = 0; i <= surface.getHeight() - 2; i++ )
            {
                if ( surface.get( i ) != null )
                {
                    searchForward( surface.get( i ) );
                }
            }
        }
    }


    /**
     * Returns the SurfaceLine matching the current in a specific direction
     *
     * @param next        - the OSList to search into
     * @param currentLine - the {@link SurfaceLine } to match.
     * @param j           - the direction of the search (1 or -1)
     */
    private void search( OSEList next, SurfaceLine currentLine, int j )
    {
        for ( AbstractOSE os : next )
        {
            if ( ! os.isVisited() ) // The OS is already added to the referenceSurface
            {
                SurfaceLine line = currentLine.match( os, j, overlap, connexity );
                if ( line != null && line.isInAdequacyWith( surface.get( currentLine.getLine() + j ) ) )
                {
                    os.setVisited();
                    surface.set( currentLine.getLine() + j, line );// the line is directly added to the surface
                }
            }
        }
    }


    /**
     * Returns the SurfaceLine generated from the specified OSList array matching the current SurfaceLine
     *
     * @param current - the {@link SurfaceLine } to match
     */
    private void searchForward( SurfaceLine current )
    {
        if ( current.getLine() + 1 <= oseLists.getLength() - 1 )
        {
            search( oseLists.get( current.getLine() + 1 ), current, 1 );
        }
    }

    /**
     * @param current - the current SurfaceLine to match with.
     */
    private void searchBackward( SurfaceLine current )
    {
        if ( current.getCoordinatesNumber() != 0 && current.getLine() > 0 )
        {
            search( oseLists.get( current.getLine() - 1 ), current, - 1 );
        }
    }
}
