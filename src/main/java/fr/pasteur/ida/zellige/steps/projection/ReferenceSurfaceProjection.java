/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.projection;

import fr.pasteur.ida.zellige.element.ReferenceSurface;
import net.imglib2.Cursor;
import net.imglib2.IterableInterval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.img.display.imagej.ImageJFunctions;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.UnsignedShortType;

import java.util.Arrays;

import static fr.pasteur.ida.zellige.steps.Utils.setPosition;

/**
 * @author ctrebeau
 * Methods to extract the reference surface from a stack with the height map obtained with the program.
 */
@SuppressWarnings( "Duplicates" )
public class ReferenceSurfaceProjection< T extends RealType< T > & NativeType< T > >
{

    public static < T extends RealType< T > & NativeType< T > > void run( ReferenceSurface< T > referenceSurface,
                                                                          ProjectionParameters projectionParameters,
                                                                          DisplayParameters displayParameters ) throws NoPossibleDisplayException
    {
        ReferenceSurfaceProjection< T > referenceSurfaceProjection = new ReferenceSurfaceProjection<>();
        referenceSurfaceProjection.set( referenceSurface, projectionParameters );
        referenceSurfaceProjection.display( referenceSurface, projectionParameters, displayParameters );
    }

    public static < T extends RealType< T > & NativeType< T > > void run( ReferenceSurface< T > referenceSurface,
                                                                          int delta, String projectionType,
                                                                          boolean projectionDisplay,
                                                                          boolean rawHeightMapDisplay,
                                                                          boolean extractedHeightMapDisplay,
                                                                          boolean reduced3DSpaceDisplay,
                                                                          boolean segmentedSurfaceDisplay,
                                                                          boolean segmentedSurfaceMaskDisplay,
                                                                          int delta2 ) throws NoPossibleDisplayException
    {
        ReferenceSurfaceProjection< T > referenceSurfaceProjection = new ReferenceSurfaceProjection<>();
        referenceSurfaceProjection.set( referenceSurface, delta, projectionType, delta2 );
        referenceSurfaceProjection.display( referenceSurface, delta, projectionType,
                projectionDisplay,
                rawHeightMapDisplay,
                extractedHeightMapDisplay,
                reduced3DSpaceDisplay,
                segmentedSurfaceDisplay,
                segmentedSurfaceMaskDisplay, delta2 );
    }


    /**
     * @param referenceSurface the considered referenceSurface
     * @param method           the desired user projection method
     * @param delta            the desired user volume value
     * @param <T>              the type of the input image
     * @return the height map projection
     */
    public static < T extends RealType< T > & NativeType< T > > Img< UnsignedShortType > getProjectionHeightMap
    ( ReferenceSurface< T > referenceSurface, String method, int delta )
    {
        RandomAccessibleInterval< T > input = referenceSurface.getInput();
        RandomAccessibleInterval< UnsignedShortType > zMap = referenceSurface.getzMap();
        ImgFactory< UnsignedShortType > imgFactory = new ArrayImgFactory<>( new UnsignedShortType() );
        Img< UnsignedShortType > heightMap =
                imgFactory.create( input.dimension( 0 ), input.dimension( 1 ) );
        RandomAccess< T > stackAccess = input.randomAccess();
        RandomAccess< UnsignedShortType > heightmapAccess = heightMap.randomAccess();
        RandomAccess< UnsignedShortType > zMapAccess = zMap.randomAccess();

        if ( method.equals( "MIP" ) )
        {
            for ( int x = 0; x <= input.dimension( 0 ) - 1; x++ )
            {
                for ( int y = 0; y <= input.dimension( 1 ) - 1; y++ )
                {

                    setPosition( zMapAccess, x, y );
                    int z = zMapAccess.get().getInteger() - 1;
                    if ( z != - 1 )
                    {
                        setPosition( stackAccess, x, y, z );
                        int finalZ = findZ( stackAccess, ( int ) input.dimension( 2 ) - 1, delta );
                        setPosition( heightmapAccess, x, y );
                        heightmapAccess.get().set( finalZ + 1 );
                    }
                }
            }
        }
        //TODO implement other projection method (average intensity, min intensity, sum slices,
        // standard deviation, and median)
        return heightMap;
    }

    /**
     * @param referenceSurface the considered reference surface
     * @param delta            the desired user volume value
     * @param <T>              the type of the reference surface
     * @return the extracted height map subStack
     */
    public static < T extends RealType< T > & NativeType< T > > Img< T > getExtractedHeightMapSubStack
    ( ReferenceSurface< T > referenceSurface,
      int delta )
    {
        RandomAccessibleInterval< T > input = referenceSurface.getInput();
        ImgFactory< T > factory = referenceSurface.getFactory();
        RandomAccessibleInterval< UnsignedShortType > zMap = referenceSurface.getzMap();
        //the stack slice number depends on the user "delta" parameter.
        Img< T > heightMapStack = factory.create( input.dimension( 0 ), input.dimension( 1 ), ( delta * 2L ) + 1 );
        RandomAccess< T > stackAccess = input.randomAccess();
        RandomAccess< T > heightMapAccess = heightMapStack.randomAccess();
        RandomAccess< UnsignedShortType > zMapAccess = zMap.randomAccess();

        for ( int x = 0; x <= input.dimension( 0 ) - 1; x++ )
        {
            zMapAccess.setPosition( x, 0 );
            for ( int y = 0; y <= input.dimension( 1 ) - 1; y++ )
            {
                zMapAccess.setPosition( y, 1 );
                int zMapValue = zMapAccess.get().getInteger() - 1;// ImgLib2 can not display negative values.
                if ( zMapValue != - 1 )
                {
                    int startIndex = Math.max( zMapValue - delta, 0 );
                    int stopIndex = Math.min( ( zMapValue + delta ), ( int ) input.dimension( 2 ) - 1 );
                    //first loop for minus delta
                    int zHeight = delta;
                    for ( int z = zMapValue; z >= startIndex; z-- )
                    {
                        setPosition( heightMapAccess, x, y, zHeight );
                        setPosition( stackAccess, x, y, z );
                        heightMapAccess.get().set( stackAccess.get() );
                        zHeight--;
                    }
                    //second loop for plus delta
                    zHeight = delta + 1;
                    for ( int z = zMapValue + 1; z <= stopIndex; z++ )
                    {
                        setPosition( heightMapAccess, x, y, zHeight );
                        setPosition( stackAccess, x, y, z );
                        heightMapAccess.get().set( stackAccess.get() );
                        zHeight++;
                    }
                }
                else
                {
                    for ( int z = 0; z <= heightMapStack.dimension( 2 ) - 1; z++ )
                    {
                        setPosition( heightMapAccess, x, y, z );
                        heightMapAccess.get().setReal( 0 );
                    }
                }
            }
        }
        return heightMapStack;
    }


    public static < T extends RealType< T > & NativeType< T > > Img< BitType > getSegmentedSurfaceMask
            ( IterableInterval< T > segmentedSurface, int delta )
    {
        Img< BitType > segmentedSurfaceMask = new ArrayImgFactory<>( new BitType() ).create( segmentedSurface );
        Cursor< T > surfaceAccess = segmentedSurface.cursor();
        RandomAccess< BitType > mapAccess = segmentedSurfaceMask.randomAccess();
        while ( surfaceAccess.hasNext() )
        {
            surfaceAccess.fwd();
            if ( surfaceAccess.get().getRealFloat() > 0 )// there is a surface pixel
            {
                int x = surfaceAccess.getIntPosition( 0 );
                int y = surfaceAccess.getIntPosition( 1 );
                int z = surfaceAccess.getIntPosition( 2 );
                int startIndex = Math.max( z - delta, 0 );
                int stopIndex = Math.min( ( int ) segmentedSurface.dimension( 2 ) - 1, ( z + delta ) );
                for ( int Z = startIndex; Z <= stopIndex; Z++ )
                {
                    mapAccess.setPosition( surfaceAccess );
                    setPosition( mapAccess, x, y, Z );
                    mapAccess.get().set( true );
                }
            }
        }
//        ImageJFunctions.show( segmentedSurfaceMask, "Mask with " + delta );
        return segmentedSurfaceMask;
    }


    public static < T extends RealType< T > & NativeType< T > > Img< T > getSegmentedSurface
    ( Img< UnsignedShortType > extractedHeightMap, RandomAccessibleInterval< T > originalInput, ImgFactory< T > factory )
    {
        Img< T > segmentedSurface = factory.create( originalInput );
        RandomAccess< T > surfaceAccess = segmentedSurface.randomAccess();
        RandomAccess< T > inputAccess = originalInput.randomAccess();
        Cursor< UnsignedShortType > heightMapCursor = extractedHeightMap.cursor();
        while ( heightMapCursor.hasNext() )
        {
            heightMapCursor.fwd();
            if ( heightMapCursor.get().getInteger() > 0 )
            {
                int x = heightMapCursor.getIntPosition( 0 );
                int y = heightMapCursor.getIntPosition( 1 );
                setPosition( surfaceAccess, x, y, heightMapCursor.get().getInteger() - 1 );
                setPosition( inputAccess, x, y, heightMapCursor.get().getInteger() - 1 );
                surfaceAccess.get().set( inputAccess.get() );
            }
        }

        return segmentedSurface;
    }


    /**
     * * Generates the projection from a specified height map and original image.
     *
     * @param referenceSurface the considered reference surface
     * @param projectionType   the type of the projection
     * @param <T>              the original stack image type.
     * @return the projection of the original stack image according to yhe height map.
     */
    public static < T extends RealType< T > & NativeType< T > > Img< T > projection1(
            ReferenceSurface< T > referenceSurface, String projectionType )
    {
        RandomAccessibleInterval< T > input = referenceSurface.getInput();
        ImgFactory< T > factory = referenceSurface.getFactory();
        RandomAccessibleInterval< UnsignedShortType > projectionHeightMap = referenceSurface.getProjection().getExtractedHeightMap();
        if ( projectionType.equals( "MIP" ) )
        {
            Img< T > projection = factory.create( input.dimension( 0 ), input.dimension( 1 ) );
            RandomAccess< T > stackAccess = input.randomAccess();
            RandomAccess< T > projectionAccess = projection.randomAccess();
            RandomAccess< UnsignedShortType > heightmapAccess = projectionHeightMap.randomAccess();
            UnsignedShortType minus = new UnsignedShortType();
            minus.setZero();
            for ( int x = 0; x <= input.dimension( 0 ) - 1; x++ )
            {
                for ( int y = 0; y <= input.dimension( 1 ) - 1; y++ )
                {
                    setPosition( heightmapAccess, x, y );
                    final int z = heightmapAccess.get().getInteger();
                    if ( z == 0 )
                    {
                        projectionAccess.get().setReal( 0 );
                    }
                    else
                    {
                        setPosition( stackAccess, x, y, z - 1 );
                        // " - 1" because by convention the z values start at 1, 0 is for unassigned values.
                        setPosition( projectionAccess, x, y );
                        projectionAccess.get().set( stackAccess.get() );
                    }
                }
            }
            return projection;
        }
        return null;
    }

    /**
     * @param referenceSurface the considered reference surface
     * @param projectionType   the user projection method
     * @param <T>              the reference surface type
     * @return a projection according to the specified parameters.
     */
    public static < T extends RealType< T > & NativeType< T > > Img< T > projection2(
            ReferenceSurface< T > referenceSurface, String projectionType )
    {

        ImgFactory< T > factory = referenceSurface.getFactory();
        RandomAccessibleInterval< T > extractedHeightMapSubStack = referenceSurface.getProjection().getReduced3DSpace();
        Img< T > projection = factory.create( extractedHeightMapSubStack.dimension( 0 ), extractedHeightMapSubStack.dimension( 1 ) );
        if ( projectionType.equals( "median" ) )// sumSlices, median, standardDeviation
        {
            medianProjection( extractedHeightMapSubStack, projection );
            return projection;
        }
        return null;
    }

    /**
     * Find the z value of the pixel with the maximum intensity at
     * the specified RandomAccess position plus or minus delta
     *
     * @param stackAccess -  the {@link RandomAccess} of original stack
     * @param stop        - the maximum value that z can take.
     * @param delta       - the delta to which the search will occur.
     * @param <T>         - the type of the {@link RandomAccess}
     * @return - the z value for which the pixel intensity is maximal.
     */
    private static < T extends RealType< T > & NativeType< T > > int findZ(
            RandomAccess< T > stackAccess, int stop, int delta )
    {
        T max = stackAccess.get().createVariable();
        max.setZero();
        int Z = 0;
        int xValue = stackAccess.getIntPosition( 0 );
        int yValue = stackAccess.getIntPosition( 1 );
        int zValue = stackAccess.getIntPosition( 2 );

        int startIndex = Math.max( zValue - delta, 0 );
        int stopIndex = Math.min( stop, ( zValue + delta ) );

        for ( int z = startIndex; z <= stopIndex; z++ )
        {
            setPosition( stackAccess, xValue, yValue, z );
            if ( stackAccess.get().compareTo( max ) >= 0 )
            {
                max = stackAccess.get().copy();
                Z = z;
            }
        }
        return Z;
    }

    public static < T extends RealType< T > & NativeType< T > > void medianProjection
            ( RandomAccessibleInterval< T > input, RandomAccessibleInterval< T > output )
    {
        RandomAccess< T > inputAccess = input.randomAccess();
        RandomAccess< T > outputAccess = output.randomAccess();
        for ( int x = 0; x <= input.dimension( 0 ) - 1; x++ )
        {
            inputAccess.setPosition( x, 0 );
            for ( int y = 0; y <= input.dimension( 1 ) - 1; y++ )
            {
                inputAccess.setPosition( y, 1 );
                double[] column = new double[ ( int ) input.dimension( 2 ) ];
                for ( int z = 0; z <= input.dimension( 2 ) - 1; z++ )
                {
                    inputAccess.setPosition( z, 2 );
                    column[ z ] = inputAccess.get().getRealFloat();
                }
                setPosition( outputAccess, x, y );
                outputAccess.get().setReal( median( column ) );
            }
        }
    }

    static double median( double[] a )
    {
        double[] sorted = a.clone();
        Arrays.sort( sorted );
        int middle = a.length / 2;

        if ( ( a.length & 1 ) == 0 ) //even
        {
            return ( sorted[ middle - 1 ] + sorted[ middle ] ) / 2f;
        }
        else
        {
            return sorted[ middle ];
        }
    }

    public void set( ReferenceSurface< T > referenceSurface, ProjectionParameters projectionParameters )
    {

        int delta = projectionParameters.getDelta();
        String projectionType = projectionParameters.getProjectionType();


        if ( projectionType.equals( "MIP" ) ||
                projectionType.equals( "Minimum Intensity" ) )
        {
            referenceSurface.getProjection().setExtractedHeightMap( getProjectionHeightMap( referenceSurface, projectionType, delta ) );
            referenceSurface.getProjection().setReduced3DSpace( getExtractedHeightMapSubStack( referenceSurface, delta ) );
            referenceSurface.getProjection().setProjection( projection1( referenceSurface, projectionType ) );
            referenceSurface.getProjection().setSegmentedSurface( getSegmentedSurface( referenceSurface.getProjection().getExtractedHeightMap(),
                    referenceSurface.getInput(),
                    referenceSurface.getFactory() ) );
        }
        else
        {
            referenceSurface.getProjection().setReduced3DSpace( getExtractedHeightMapSubStack( referenceSurface, delta ) );
            referenceSurface.getProjection().setProjection( projection2( referenceSurface, projectionType ) );
        }

    }

    public void set( ReferenceSurface< T > referenceSurface, int delta, String projectionType, int delta2 )
    {


        if ( projectionType.equals( "MIP" ) ||
                projectionType.equals( "Minimum Intensity" ) )
        {
            referenceSurface.getProjection().setExtractedHeightMap( getProjectionHeightMap( referenceSurface, projectionType, delta ) );
            referenceSurface.getProjection().setReduced3DSpace( getExtractedHeightMapSubStack( referenceSurface, delta ) );
            referenceSurface.getProjection().setProjection( projection1( referenceSurface, projectionType ) );
            referenceSurface.getProjection().setSegmentedSurface( getSegmentedSurface( referenceSurface.getProjection().getExtractedHeightMap(),
                    referenceSurface.getInput(),
                    referenceSurface.getFactory() ) );
            referenceSurface.getProjection().setSegmentedSurfaceMask( getSegmentedSurfaceMask( referenceSurface.getProjection().getSegmentedSurface(), delta2 ) );
        }
        else
        {
            referenceSurface.getProjection().setReduced3DSpace( getExtractedHeightMapSubStack( referenceSurface, delta ) );
            referenceSurface.getProjection().setProjection( projection2( referenceSurface, projectionType ) );
        }

    }

    public void display( ReferenceSurface< T > referenceSurface, ProjectionParameters projectionParameters, DisplayParameters displayParameters ) throws NoPossibleDisplayException
    {
        int delta = projectionParameters.getDelta();
        String projectionType = projectionParameters.getProjectionType();
        if ( referenceSurface.getProjection() != null )
        {
            if ( displayParameters.isProjectionDisplay() )
            {
                ImageJFunctions.show( referenceSurface.getProjection().get(),
                        "Projection with " + projectionType + " RS n°" + delta );

            }
            if ( displayParameters.iszMapDisplay() )
            {
                ImageJFunctions.show( referenceSurface.getzMap(), "Raw height map" + "RS n°" + delta );
            }
            if ( displayParameters.isExtractedHeightMapSubStackDisplay() )

            {
                if ( delta >= 0 )
                {
                    ImageJFunctions.show( referenceSurface.getProjection().getReduced3DSpace(),
                            "Extracted surface subStack (delta = " + delta + ")" + "RS n°" + delta );
                }
                else
                {
                    throw new NoPossibleDisplayException( "The value of parameter delta equals 0 !" );
                }
            }
            if ( displayParameters.isSurfaceSubVolumeDisplay() )
            {
                if ( delta > 0 )
                {
                    ImageJFunctions.show( referenceSurface.getProjection().getSegmentedSurface(),
                            "RS n°" + delta + ": segmented surface (delta = " + delta + ")" + "RS n°" + delta );
                }
                else
                {
                    throw new NoPossibleDisplayException( "The value of parameter delta equals 0 !" );
                }
            }
            if ( displayParameters.isHeightMapSubVolumeMask() )
            {
                ImageJFunctions.show( referenceSurface.getProjection().getSegmentedSurfaceMask(),
                        "RS n° " + delta + " :  segmented surface mask mask (delta = " + delta );
            }
            if ( ( projectionType.equals( "MIP" ) || projectionType.equals( "Min" ) ) && displayParameters.isProjectionHeightMapDisplay() )
            {
                if ( delta >= 0 )
                {
                    ImageJFunctions.show( referenceSurface.getProjection().getExtractedHeightMap(), "Extracted height Map with " +
                            projectionType + " RS n°" + delta );
                }
                else
                {
                    throw new NoPossibleDisplayException( "This method does not produce a projection height map !" );
                }
            }


        }
    }

    public void display( ReferenceSurface< T > referenceSurface, int delta, String projectionType,
                         boolean projectionDisplay,
                         boolean rawHeightMapDisplay,
                         boolean extractedHeightMapDisplay,
                         boolean reduced3DSpaceDisplay,
                         boolean segmentedSurfaceDisplay,
                         boolean segmentedSurfaceMaskDisplay ) throws NoPossibleDisplayException
    {

        if ( referenceSurface.getProjection() != null )
        {
            if ( projectionDisplay )// the projection
            {
                ImageJFunctions.show( referenceSurface.getProjection().get(),
                        "Projection with " + projectionType + " RS n°" + delta );
            }
            if ( rawHeightMapDisplay )// the raw height map
            {
                ImageJFunctions.show( referenceSurface.getzMap(), "Zellige generated height map" + "RS n°" + delta );
            }

            if ( reduced3DSpaceDisplay )
            {
                if ( delta != 0 )
                {
                    ImageJFunctions.show( referenceSurface.getProjection().getReduced3DSpace(),
                            "Reduced 3D space (delta = " + delta + ")" + "RS n°" + delta );
                }
                else
                {
                    throw new NoPossibleDisplayException( "The value of parameter delta equals 0 !" );
                }
            }

            if ( ( projectionType.equals( "MIP" ) || projectionType.equals( "Min" ) ) && extractedHeightMapDisplay ) // the extracted height map
            {
                if ( delta != 0 )
                {
                    ImageJFunctions.show( referenceSurface.getProjection().getExtractedHeightMap(), "Extracted height Map with " +
                            projectionType + " RS n°" + delta );
                }
                else
                {
                    throw new NoPossibleDisplayException( "This method does not produce a extracted height map !" );
                }
            }
            if ( segmentedSurfaceDisplay )
            {
                if ( delta != 0 )
                {
                    ImageJFunctions.show( referenceSurface.getProjection().getSegmentedSurface(),
                            "Height map subVolume mask (delta = " + delta + ")" + "RS n°" + delta );
                }
                else
                {
                    throw new NoPossibleDisplayException( "The value of parameter delta equals 0 !" );
                }
            }
            if ( segmentedSurfaceMaskDisplay ) // the segmented surface mask
            {
                ImageJFunctions.show( referenceSurface.getProjection().getSegmentedSurfaceMask(),
                        "RS n° " + delta + " :  SubVolume mask (delta = " + delta );
            }
        }

    }

    public void display( ReferenceSurface< T > referenceSurface, int delta, String projectionType,
                         boolean projectionDisplay,
                         boolean rawHeightMapDisplay,
                         boolean extractedHeightMapDisplay,
                         boolean reduced3DSpaceDisplay,
                         boolean segmentedSurfaceDisplay,
                         boolean segmentedSurfaceMaskDisplay,
                         int delta2 ) throws NoPossibleDisplayException
    {
        if ( referenceSurface.getProjection() != null )
        {
            if ( projectionDisplay )// the projection
            {
                ImageJFunctions.show( referenceSurface.getProjection().get(),
                        "Projection with " + projectionType + " RS n°" + delta );
            }
            if ( rawHeightMapDisplay )// the raw height map
            {
                ImageJFunctions.show( referenceSurface.getzMap(), "Raw Height Map" + "RS n°" + delta );
            }

            if ( reduced3DSpaceDisplay )// the reduced 3D stack of size (X, Y, delta*2 +1)
            {
                if ( delta != 0 )
                {
                    ImageJFunctions.show( referenceSurface.getProjection().getReduced3DSpace(),
                            "Reduced 3D space (delta = " + delta + ")" + "RS n°" + delta );
                }
                else
                {
                    throw new NoPossibleDisplayException( "The value of parameter delta equals 0 !" );
                }
            }

            if ( ( projectionType.equals( "MIP" ) || projectionType.equals( "Min" ) ) && extractedHeightMapDisplay ) // the extracted height map
            {
                if ( delta != 0 )
                {
                    ImageJFunctions.show( referenceSurface.getProjection().getExtractedHeightMap(), "Extracted height Map with " +
                            projectionType + " RS n°" + delta );
                }
                else
                {
                    throw new NoPossibleDisplayException( "This method does not produce a extracted height map !" );
                }
            }
            if ( segmentedSurfaceDisplay )
            {
                if ( delta > 0 )
                {
                    ImageJFunctions.show( referenceSurface.getProjection().getSegmentedSurface(),
                            "Segmented surface (delta = " + delta + ")" + "RS n°" + delta );
                }
                else
                {
                    throw new NoPossibleDisplayException( "The value of parameter delta equals 0 !" );
                }
            }
            if ( segmentedSurfaceMaskDisplay ) // the segmented surface mask
            {
                ImageJFunctions.show( referenceSurface.getProjection().getSegmentedSurfaceMask(),
                        "RS n° " + delta + " : Segmented surface mask (delta = " + delta2 );
            }
        }

    }
}



