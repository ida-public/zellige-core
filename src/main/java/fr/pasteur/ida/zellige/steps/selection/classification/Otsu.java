/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.classification;

import net.imglib2.Cursor;
import net.imglib2.IterableInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;

import java.util.Map;
import java.util.TreeMap;

public class Otsu
{

    public static < T extends RealType< T > & NativeType< T > > T getThreshold( IterableInterval< T > source )
    {
        OtsuOperator< T > otsuOperator = new OtsuOperator<>( source );
        otsuOperator.process();
        T threshold = source.firstElement().createVariable();
        threshold.setReal( otsuOperator.getThreshold() );
        return threshold;
    }

    private static class OtsuOperator< T extends RealType< T > & NativeType< T > >
    {

        private final IterableInterval< T > source;
        private double threshold;

        /**
         * Java implementation of Jacques Boutet de Monvel's Matlab code
         *
         * @param source - the image to be thresholded as an {@link IterableInterval}
         */
        public OtsuOperator( IterableInterval< T > source )
        {
            this.source = source;
        }

        public static int[] makeCumulativeSum( int[] input )
        {
            int[] output = new int[ input.length ];
            int sum = 0;
            for ( int i = 0; i <= input.length - 1; i++ )
            {
                sum += input[ i ];
                output[ i ] = sum;
            }
            return output;
        }

        public static double[] makeCumulativeWeightedSum( int[] count, double[] intensities )
        {
            double[] output = new double[ count.length ];
            double sum = 0;
            for ( int i = 0; i <= count.length - 1; i++ )
            {
                sum += ( count[ i ] * intensities[ i ] );
                output[ i ] = sum;
            }
            return output;
        }

        public static double[] getP0( int[] n0, int N )
        {
            double[] output = new double[ n0.length ];

            for ( int i = 0; i <= n0.length - 1; i++ )
            {
                output[ i ] = ( double ) n0[ i ] / ( double ) N;
            }
            return output;
        }


        public static double[] getLittleM0( double[] M0, int[] n0 )
        {
            double[] output = new double[ M0.length ];
            for ( int i = 0; i <= M0.length - 1; i++ )
            {
                output[ i ] = M0[ i ] / n0[ i ];
            }
            return output;
        }

        public static double[] getM1( int N, double mTot, double[] M0, int[] n0 )
        {
            double[] output = new double[ M0.length ];
            for ( int i = 0; i <= M0.length - 1; i++ )
            {
                output[ i ] = ( N * mTot - M0[ i ] ) / ( N - n0[ i ] );
            }
            return output;
        }

        public static double[] getVB( double mTot, double[] m0, double[] m1, double[] p0, double[] p1 )
        {
            double[] vb = new double[ m0.length ];
            for ( int i = 0; i <= m0.length - 1; i++ )
            {
                vb[ i ] = p0[ i ] * Math.pow( ( m0[ i ] - mTot ), 2 ) + p1[ i ] * Math.pow( ( m1[ i ] - mTot ), 2 );
            }
            return vb;
        }

        public static int findKMax( double[] vb )
        {
            double max = 0;
            int k = 0;
            for ( int i = 0; i <= vb.length - 1; i++ )
            {
                if ( vb[ i ] > max )
                {
                    max = vb[ i ];
                    k = i;
                }
            }
            return k;
        }


        public static double[] getP1( double[] p0 )
        {
            double[] p1 = new double[ p0.length ];
            for ( int i = 0; i <= p0.length - 1; i++ )
            {
                p1[ i ] = 1 - p0[ i ];
            }
            return p1;
        }

        private void process()
        {
            // Use of TreeMap : keys and values are automatically sorted.
            Map< Double, Integer > hm = new TreeMap<>();
            Cursor< T > cursor = this.source.cursor();
            while ( cursor.hasNext() )
            {
                cursor.fwd();
                final double value = cursor.get().getRealDouble();
                Integer j = hm.get( value );
                hm.put( value, ( j == null ) ? 1 : j + 1 );

            }


            // Settings of the intensities array, histogram and mean
            double[] intensities = new double[ hm.size() ];
            int[] histogram = new int[ hm.size() ];
            int index = 0;
            double sum = 0;

            for ( Map.Entry< Double, Integer > set : hm.entrySet() )
            {
                double intensity = set.getKey();
                int count = set.getValue();
                intensities[ index ] = intensity;
                histogram[ index ] = count;
                sum = ( intensity * count ) + sum;
                index++;
            }

            // Setting of mean and number of pixels

            int N = 1;
            for ( int d = 0; d <= source.numDimensions() - 1; d++ )
            {
                N = N * ( int ) source.dimension( d );
            }
            double mTot = sum / N;

            // Cumulative arrays
            int[] n0 = makeCumulativeSum( histogram );
            double[] M0 = makeCumulativeWeightedSum( histogram, intensities );
            double[] m0 = getLittleM0( M0, n0 );
            double[] p0 = getP0( n0, N );
            double[] m1 = getM1( N, mTot, M0, n0 );
            double[] p1 = getP1( p0 );
            double[] vb = getVB( mTot, m0, m1, p0, p1 );
            int k = findKMax( vb );
            threshold = intensities[ k ];


        }

        public double getThreshold()
        {
            return threshold;
        }

        public double sum( double[] array )
        {
            double sum = 0;
            for ( double d : array )
            {
                sum = d + sum;
            }
            return sum;
        }
    }
}
