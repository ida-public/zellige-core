/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.classification;

import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;
import fr.pasteur.ida.zellige.steps.selection.exception.NoClassificationException;
import net.imglib2.Cursor;
import net.imglib2.IterableInterval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.img.display.imagej.ImageJFunctions;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;


/**
 * This class allows the double classification of a 3D image using in parallel a classification based on the amplitude
 * of the image local maxima and a classification based on the local intensity. The final classification is the
 * intersection of both. Each classification is parametrized by a value provided by the user.
 *
 * @param <T> the input type
 */
public class Classification< T extends RealType< T > & NativeType< T > >
{
    public static final int ZERO = 0;
    private final static Logger LOGGER = LoggerFactory.getLogger( Classification.class );
    private final RandomAccessibleInterval< T > input;
    private final ImgFactory< T > factory;
    private Img< BitType > output;


    private Classification( RandomAccessibleInterval< T > input, ImgFactory< T > factory )
    {
        this.input = input;
        this.factory = factory;
    }

    /**
     * Static method to provide a 2-part classification on a 3D image
     *
     * @param <T>        the image type
     * @param input      the 3D image
     * @param factory    the input's {@link ImgFactory}.
     * @param parameters - the parameters for the 2-parts classification
     * @return a binary image
     * @throws NoClassificationException if the binary image is empty.
     * @throws DataValidationException   if the value of parameter amplitudeThreshold is not valid.
     */
    public static < T extends RealType< T > & NativeType< T > > Img< BitType >
    run( RandomAccessibleInterval< T > input, ImgFactory< T > factory, ClassificationParameters parameters ) throws NoClassificationException, DataValidationException
    {
        Classification< T > classification = new Classification<>( input, factory );
        classification.process( parameters );
        return classification.output;
    }

    /**
     * @param parameters - the user parameters to set the classifications
     * @throws NoClassificationException if both classifications are set to Zero
     * @throws DataValidationException   if one of the user thresholds is incorrect
     */
    void process( ClassificationParameters parameters ) throws NoClassificationException, DataValidationException
    {
        LOGGER.debug( "Starting classification..." );
        int amplitudeThreshold = parameters.getAmplitudeThreshold();
        int otsuThreshold = parameters.getOtsuThreshold();
        if ( amplitudeThreshold != ZERO && otsuThreshold != ZERO )
        {
            processBothClassification( factory, amplitudeThreshold, otsuThreshold );
        }
        else if ( amplitudeThreshold != ZERO )
        {
            processAmplitudeClassification( factory, amplitudeThreshold );
        }
        else if ( otsuThreshold != ZERO )
        {
            processOtsuClassification( factory, otsuThreshold );
        }
        else
        {
            throw new NoClassificationException();
        }
        LOGGER.debug( "Classification complete" );
    }


    void processAmplitudeClassification( ImgFactory< T > factory, int amplitudeThreshold ) throws DataValidationException
    {
        /* Classification according to maximum amplitude */
        output = AmplitudeClassification.run( input, factory, amplitudeThreshold );
    }

    void processOtsuClassification( ImgFactory< T > factory, int otsuThreshold )
    {
        /* Classification according to local intensity*/
        output = OtsuClassification.run( input, factory, otsuThreshold );
    }

    void processBothClassification( ImgFactory< T > factory, int amplitudeThreshold, int otsuThreshold ) throws DataValidationException
    {
        /* Classification according to maximum amplitude */
        Img< BitType > amplitudeImg = AmplitudeClassification.run( input, factory, amplitudeThreshold );

        ImageJFunctions.show( amplitudeImg );
        /* Classification according to local intensity*/

        Img< BitType > otsuImg = OtsuClassification.run( input, factory, otsuThreshold );
        ImageJFunctions.show( Objects.requireNonNull( otsuImg ) );
        /* Intersection of both classification */
        this.output = interClassification( amplitudeImg, amplitudeImg.factory(), otsuImg );
    }

    /**
     * @param amplitude  an {@link BitType}{@link Img}
     * @param factory    the same  {@link BitType} {@link ImgFactory} as amplitude
     * @param thresholds another {@link BitType}{@link Img}
     * @return the intersection of both image
     */
    public static Img< BitType > interClassification( IterableInterval< BitType > amplitude,
                                                      ImgFactory< BitType > factory,
                                                      RandomAccessibleInterval< BitType > thresholds )
    {
        Img< BitType > output = factory.create( thresholds );
        Cursor< BitType > amplitudeCursor = amplitude.localizingCursor();
        RandomAccess< BitType > thresholdAccess
                = thresholds.randomAccess();
        RandomAccess< BitType > outputAccess = output.randomAccess();
        while ( amplitudeCursor.hasNext() )
        {
            amplitudeCursor.fwd();
            if ( amplitudeCursor.get().getRealDouble() != 0 )
            {
                thresholdAccess.setPosition( amplitudeCursor );
                if ( thresholdAccess.get().get() )
                {
                    outputAccess.setPosition( amplitudeCursor );
                    outputAccess.get().setOne();
                }
            }
        }
        return output;
    }

}
