/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.util;

import net.imglib2.RandomAccessible;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.gradient.PartialDerivative;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

public class Derivative
{

    public static final String CENTRAL = "central";
    public static final String FORWARD = "forward";
    public static final String BACKWARD = "backward";

    /**
     * Method to apply the specified partial derivative function to a {@link RandomAccessibleInterval}
     * in the second dimension of a {@link RandomAccessibleInterval}.
     * The resulted output {@link RandomAccessibleInterval} is given as a parameter.
     *
     * @param input  - the source image as a {@link RandomAccessibleInterval}
     * @param output - the output image as a {@link RandomAccessibleInterval}
     * @param method - the method to use for the derivative computation
     * @param <T>    the image and output type
     */
    public static < T extends RealType< T > & NativeType< T > > void run(
            final RandomAccessibleInterval< T > input, RandomAccessibleInterval< T > output, String method )
    {
        switch ( method )
        {
            case CENTRAL:
                gradientCentral( input, output );
                break;
            case FORWARD:
                forward( input, output );
                break;
            case BACKWARD:
                backWard( input, output );
                break;
        }
    }

    /**
     * Method to apply a centered partial derivative function to a {@link RandomAccessibleInterval}
     * in the second dimension of a {@link RandomAccessibleInterval}.
     * The resulted output {@link RandomAccessibleInterval} is given as a parameter.
     *
     * @param input  - the source image as a {@link RandomAccessibleInterval}
     * @param output - the output image as a {@link RandomAccessibleInterval}
     */
    private static < T extends RealType< T > & NativeType< T > > void
    gradientCentral( final RandomAccessibleInterval< T > input,
                     RandomAccessibleInterval< T > output )
    {
        RandomAccessible< T > infiniteInput = Views.extendMirrorDouble( input );
        PartialDerivative.gradientCentralDifference2( infiniteInput, output, 1 );
    }

    /**
     * Method to apply a forward partial derivative function to a {@link RandomAccessibleInterval}
     * in the second dimension of a {@link RandomAccessibleInterval}.
     * The resulted output {@link RandomAccessibleInterval} is given as a parameter.
     *
     * @param input  - the source image as a {@link RandomAccessibleInterval}
     * @param output - the output image as a {@link RandomAccessibleInterval}
     */
    private static < T extends RealType< T > & NativeType< T > > void
    forward( final RandomAccessibleInterval< T > input,
             RandomAccessibleInterval< T > output )
    {
        RandomAccessible< T > infiniteInput = Views.extendMirrorDouble( input );
        PartialDerivative.gradientForwardDifference( infiniteInput, output, 1 );
    }

    /**
     * Method to apply a backward partial derivative function to a {@link RandomAccessibleInterval}
     * in the second dimension of a {@link RandomAccessibleInterval}.
     * The resulted output {@link RandomAccessibleInterval} is given as a parameter.
     *
     * @param input  - the source image as a {@link RandomAccessibleInterval}
     * @param output - the output image as a {@link RandomAccessibleInterval}
     */
    private static < T extends RealType< T > & NativeType< T > > void
    backWard( final RandomAccessibleInterval< T > input,
              RandomAccessibleInterval< T > output )
    {
        RandomAccessible< T > infiniteInput = Views.extendMirrorDouble( input );
        PartialDerivative.gradientBackwardDifference( infiniteInput, output, 1 );
    }
}
