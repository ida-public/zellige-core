/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.classification;

import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassificationParameters
{
    private final static Logger LOGGER = LoggerFactory.getLogger( ClassificationParameters.class );

    private final int amplitudeThreshold, otsuThreshold;

    public ClassificationParameters( int amplitudeThreshold, int otsuThreshold ) throws DataValidationException
    {
        classificationParametersValidationCheck( amplitudeThreshold, otsuThreshold );
        this.amplitudeThreshold = amplitudeThreshold;
        this.otsuThreshold = otsuThreshold;
        LOGGER.debug( "Classification parameters are valid" );
    }

    /**
     * @param name  the parameter name to display in the error message.
     * @param value the value of the parameter tested.
     * @throws DataValidationException if the value is inferior to zero.
     */
    public static void valueValidationCheck( String name, int value ) throws DataValidationException
    {
        if ( value < 0 )
        {
            throw new DataValidationException( "The value of parameter " + name + " has to be superior to zero !" );
        }
        if ( value > 255 )
        {
            throw new DataValidationException( "The value of parameter " + name + " has to be inferior or equals to 50!" );
        }


    }


    /**
     * Checks the four parameters validity.
     *
     * @param amplitudeThreshold - the value choose as amplitude threshold.
     * @param otsuThreshold      - the value choose as otsu threshold.
     * @throws DataValidationException if at least one parameter is not valid.
     */
    public void classificationParametersValidationCheck( int amplitudeThreshold, int otsuThreshold ) throws DataValidationException
    {
        valueValidationCheck( "Amplitude Threshold", amplitudeThreshold );
        valueValidationCheck( "Otsu Threshold", otsuThreshold );
    }


    public int getAmplitudeThreshold()
    {
        return amplitudeThreshold;
    }

    public int getOtsuThreshold()
    {
        return otsuThreshold;
    }
}
