/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.util;

import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExtremaDetection< T extends RealType< T > & NativeType< T > >
{


    public static final String MAX = "max";
    public static final String MIN = "min";
    private final static Logger LOGGER = LoggerFactory.getLogger( ExtremaDetection.class );
    private final RandomAccessibleInterval< T > input;
    private final ImgFactory< T > factory;
    private final Img< T > extrema;
    private final String type;
    private static final String method = "forward"; //TODO same method for an entire run !!!


    public ExtremaDetection( RandomAccessibleInterval< T > input, ImgFactory< T > factory, String type ) throws DataValidationException
    {
        parameterCheck( input, factory, type, method );
        this.input = input;
        this.factory = factory;
        this.extrema = factory.create( input );
        this.type = type;
        LOGGER.debug( "Partial derivative is : {}", method );
    }

    /**
     * @param input   the image containing the extrema
     * @param factory the input factory
     * @param <T>     the input type
     * @return an image with extrema with original value and other pixels at zero value
     * @throws DataValidationException if the input image is not valid
     */
    public static < T extends RealType< T > & NativeType< T > > Img< T > findMinima( RandomAccessibleInterval< T > input,
                                                                                     ImgFactory< T > factory ) throws DataValidationException
    {
        LOGGER.debug( "Finding minima..." );
        return ExtremaDetection.run( input, factory, MIN );
//        return findExtrema( input, factory, "min" );
    }

    /**
     * @param input   the image containing the extrema
     * @param factory the input factory
     * @param <T>     the input type
     * @return an image with extrema with original value and other pixels at zero value
     * @throws DataValidationException if the input image is not valid
     */
    public static < T extends RealType< T > & NativeType< T > > Img< T > findMaxima( RandomAccessibleInterval< T > input,
                                                                                     ImgFactory< T > factory ) throws DataValidationException
    {
        LOGGER.debug( "Finding maxima..." );
        return ExtremaDetection.run( input, factory, MAX );
//        return findExtrema( input, factory, "max" );
    }

    /**
     * @param input   the image containing the extrema
     * @param factory the input factory
     * @param <T>     the input type
     * @param type    min or max
     * @return a filtered image containing only extrema along the z axis
     * @throws DataValidationException if the input image is not valid
     */
    public static < T extends RealType< T > & NativeType< T > > Img< T > run( RandomAccessibleInterval< T > input, ImgFactory< T > factory, String type ) throws DataValidationException
    {
        ExtremaDetection< T > detection3D = new ExtremaDetection<>( input, factory, type );
        detection3D.run();
        return detection3D.getExtrema();
    }

    public void run()
    {
        LOGGER.debug( "Starting detection..." );
        if ( input.numDimensions() == 2 )
        {
            ExtremaDetection2D.run( input, factory, extrema, type, method );
        }
        else
        {
            // For each individual orthogonal XZ sections.*/
            for ( int x = 0; x <= input.dimension( 0 ) - 1; x++ )// iteration on dimension X
            {
                try
                {
                    IntervalView< T > section = Views.hyperSlice( this.input, 0, x );
                    IntervalView< T > extremaSection = Views.hyperSlice( extrema, 0, x );
                    computeExtrema( section, extremaSection );
                }
                catch ( Exception e )
                {
                    e.printStackTrace();
                }
            }
        }
        LOGGER.debug( "Extrema detected." );
    }


    public void computeExtrema( RandomAccessibleInterval< T > section, RandomAccessibleInterval< T > extremaSection )
    {
        ExtremaDetection2D.run( section, factory, extremaSection, type, method );
    }


    void parameterCheck( RandomAccessibleInterval< T > input, ImgFactory< T > factory, String type, String method ) throws DataValidationException
    {
        if ( input.numDimensions() != 2 && input.numDimensions() != 3 )
        {
            throw new DataValidationException( "The input for the LocalExtremaDetection2D has to be a 2D or 3D image, this input is a " + input.numDimensions() + "D image" );
        }
        if ( ! input.getAt( 0, 0, 0 ).getClass().toGenericString().equals( factory.type().getClass().toGenericString() ) )
        {
            throw new DataValidationException( "The input and the factory don't have the same type" );
        }
        if ( ! type.equals( MAX ) && ! type.equals( MIN ) )
        {
            throw new DataValidationException( "The type parameter has to be 'min' or 'max'" );
        }
        if ( ! method.equals( "forward" ) && ! method.equals( "backward" ) && ! method.equals( "central" ) )
        {
            throw new DataValidationException( "The method " + method + "is not implemented. Please choose among 'forward', 'backward' and 'central'" );
        }
    }

    public Img< T > getExtrema()
    {
        return extrema;
    }


}
