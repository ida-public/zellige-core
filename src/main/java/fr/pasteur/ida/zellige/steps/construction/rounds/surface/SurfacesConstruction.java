/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.construction.rounds.surface;


import fr.pasteur.ida.zellige.element.Surface;
import fr.pasteur.ida.zellige.element.ose.AbstractOSE;
import fr.pasteur.ida.zellige.element.ose.OSEListArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;


/**
 *
 */
public class SurfacesConstruction
{

    private final static Logger LOGGER = LoggerFactory.getLogger( SurfacesConstruction.class );

    private final ArrayList< Surface > constructedSurfaces = new ArrayList<>();
    private final OSEListArray oseListArray;
    private final int width;
    private final double connexity;
    private final int overlap;
    private int smallSurfaceCount;
    private final double surfaceMinSizeFactor;

    public SurfacesConstruction( OSEListArray oseLists, int width, int overlap, double connexity, double surfaceMinSizeFactor )
    {
        this.oseListArray = oseLists;
        this.width = width;
        this.connexity = connexity;
        this.overlap = overlap;
        this.surfaceMinSizeFactor = surfaceMinSizeFactor;
    }

    public static ArrayList< Surface > run( OSEListArray oseListArray, int surfaceLineLength, int overlap, double connexity, double surfaceMinSizeFactor )
    {
        SurfacesConstruction reconstruction = new SurfacesConstruction( oseListArray, surfaceLineLength, overlap, connexity, surfaceMinSizeFactor );
        reconstruction.buildAllSurfaces();
        return reconstruction.constructedSurfaces;
    }


    public void buildAllSurfaces()
    {
        while ( true )
        {
            // All the OSLists are set to "not visited".
            AbstractOSE startingOse = oseListArray.getAStartingOse();
            if ( startingOse != null )
            {
                Surface surface = SurfaceConstruction.run( oseListArray, startingOse, width, overlap, connexity );
                checkSurface( surface );
            }
            else
            {
                break;
            }
        }
        LOGGER.debug( " Number of small surfaces found = {}", smallSurfaceCount );
    }


    /**
     * Surface validation check. if the current surface is valid it will be added to the surfaces list.
     *
     * @param surface the surface to check.
     */
    private void checkSurface( Surface surface )
    {
        if ( surface.getSize() >= width * oseListArray.getLength() * surfaceMinSizeFactor )
        {
            constructedSurfaces.add( surface );
        }
        else
        {
            smallSurfaceCount++;
        }
    }

    public ArrayList< Surface > getConstructedSurfaces()
    {
        return constructedSurfaces;
    }

    public int getSmallSurfaceCount()
    {
        return smallSurfaceCount;
    }
}
