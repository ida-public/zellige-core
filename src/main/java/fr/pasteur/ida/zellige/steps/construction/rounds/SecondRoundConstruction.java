/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.construction.rounds;

import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.element.Surface;
import fr.pasteur.ida.zellige.element.ose.OSEListArray;
import fr.pasteur.ida.zellige.element.surfaceLine.SurfaceLine;
import fr.pasteur.ida.zellige.steps.construction.exception.NoSurfaceFoundException;
import fr.pasteur.ida.zellige.steps.construction.exception.SecondRoundConstructionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class SecondRoundConstruction extends ConstructionRound
{
    private final static Logger LOGGER = LoggerFactory.getLogger( SecondRoundConstruction.class );
    private final ArrayList< Surface > tempSurfaces;

    public SecondRoundConstruction( ArrayList< Surface > tempSurfaces, ConstructionParameters constructionParameters )
    {
        super( constructionParameters );
        this.tempSurfaces = tempSurfaces;
    }

    public SecondRoundConstruction( final ArrayList< Surface > tempSurfaces, final double startingSizeThreshold,
                                    final int overlap,
                                    final double connexityRate,
                                    final double surfaceMinSizeFactor )
    {
        super( startingSizeThreshold, overlap, connexityRate, surfaceMinSizeFactor );
        this.tempSurfaces = tempSurfaces;
    }

    /**
     * Rebuilds the double pixel array from the {@link SurfaceLine} array
     * of a {@link Surface} for a construction in the height dimension.
     *
     * @param surface - the {@link SurfaceLine}  array of a {@link Surface}
     * @return a {@link Surface} specific double pixel array.
     */
    private Pixels[][] rebuildMaximums( Surface surface )
    {
        LOGGER.debug( "Rebuilding surface..." );
        int width = surface.getHeight();
        int height = surface.getWidth();
        Pixels[][] tempCoordinates = new Pixels[ height ][ width ]; // Transposed array
        for ( int i = 0; i <= width - 1; i++ )
        {
            for ( int j = 0; j <= height - 1; j++ )
            {
                if ( surface.get( i ) != null )
                {
                    tempCoordinates[ j ][ i ] = surface.get( i ).get( j );
                }
            }
        }
        LOGGER.debug( "Surface rebuild." );
//        displayMaximums(tempCoordinates);
        return tempCoordinates;
    }

    public void process() throws NoSurfaceFoundException
    {
        long tempOSTime = 0;
        long tempSurfaceTime = 0;
        LOGGER.debug( "Second step : starting process..." );
        LOGGER.debug( "Second round construction for {} surfaces", tempSurfaces.size() );
        for ( Surface surface : tempSurfaces )
        {
            if ( surface.hasDuplicate() )// CoordinateList instead of Coordinate
            {
                LOGGER.debug( "Surface with duplicates to process." );
                Pixels[][] maximums = rebuildMaximums( surface );
                /* OSE construction according to the dimension considered */
                long startOseConstructionTime = System.currentTimeMillis();
                OSEListArray oseLists = constructOSE( maximums );
                long stopOseConstructionTime = System.currentTimeMillis();
                tempOSTime += stopOseConstructionTime - startOseConstructionTime;

                int lineLength = maximums[ 0 ].length;
                long startSurfacesConstructionTime = System.currentTimeMillis();
                ArrayList< Surface > temps = constructSurfaces( oseLists, lineLength );
                long stopSurfacesConstructionTime = System.currentTimeMillis();
                tempSurfaceTime += stopSurfacesConstructionTime - startSurfacesConstructionTime;
                if ( temps.isEmpty() )
                {
                    LOGGER.debug( "Only small surfaces in second round." );
                }
                else
                {
                    this.getSurfaces().addAll( temps );
                }
            }
            else
            {
                LOGGER.debug( "Surface without duplicates added." );
                this.getSurfaces().add( surface.transpose() );
                if ( this.getSurfaces().isEmpty() )
                {
                    throw new SecondRoundConstructionException();
                }
            }
            this.setOSConstructionProcessingTime( tempOSTime );
            this.setConstructionProcessingTime( tempSurfaceTime );
        }
        finalizeSurface( this.getSurfaces() );
        LOGGER.debug( "Process completed." );
    }
}
