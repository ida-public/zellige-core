/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.pretreatment;

import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;

public class PretreatmentParameters
{
    public static final String GAUSSIAN_BLUR = "GaussianBlur";
    public static final String NOT_IMPLEMENTED = "not implemented";
    private final String method;
    private final double parameter;


    public PretreatmentParameters( String method, double parameter ) throws DataValidationException
    {
        this.checkParameters( method, parameter );
        this.method = method;
        this.parameter = parameter;
    }

    /**
     * Checks if the method end the arguments methods are valid
     *
     * @param method    the method choose
     * @param parameter the method arguments
     * @throws DataValidationException if at least one is not valid
     */
    private void checkParameters( String method, double parameter ) throws DataValidationException
    {
        checkDenoisingMethod( method );
        checkParametersLength( parameter );
    }

    /**
     * Checks the length of the parameters according to the method choose
     *
     * @param parameter the method arguments
     * @throws DataValidationException if at the parameters' length (the number of arguments) is not valid according to the method choose
     */
    private void checkParametersLength( double parameter ) throws DataValidationException
    {
        if ( parameter < 0 || parameter > 10 )
        {
            throw new DataValidationException( "The parameter value has to be comprise between 0 and 10" );
        }

        //TODO different thresholds values for methods ?
    }

    /**
     * Check if the denoising method is valid.
     * @param denoisingMethod the input denoising method
     * @throws DataValidationException if the method is not valid
     */
    public void checkDenoisingMethod( String denoisingMethod ) throws DataValidationException
    {
        if ( ! denoisingMethod.equals( GAUSSIAN_BLUR ) )
        {
            throw new DataValidationException( "The denoising method " + denoisingMethod + " is " + NOT_IMPLEMENTED + " in the program" );
        }
    }

    public String getMethod()
    {
        return method;
    }

    public double getParameter()
    {
        return parameter;
    }
}
