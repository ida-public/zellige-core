/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.postTreatment.islandSearch;


import java.util.LinkedList;
import java.util.Queue;

public class World
{

    private final Sand[][][] world;
    private final int islandSize;
    private final int depth;
    private final int width;
    private final int height;
    private final int connectivityType;

    public World( int width, int height, int depth, int islandSize, int connectivityType )
    {
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.world = new Sand[ depth ][ height ][ width ];
        this.islandSize = islandSize;
        this.connectivityType = connectivityType;

    }

    public void run()
    {
        for ( int z = 0; z < world.length; z++ )
        {
            for ( int y = 0; y < world[0].length; y++ )
            {
                for( int x = 0; x < world[0][0].length ; x++)

                {
                    Sand sand = getSand( x, y, z );
                    if ( sand != null && sand.getIslandStatus() == -1 )
                    {
                        setIslandStatus( sand );
                    }
                }
            }
        }
    }

    private Island getNeighbour( Sand sand)
    {
        if (connectivityType == 4)
        {
            return getFourConnected( sand );
        }
        else {
            return getEightConnected( sand );
        }
    }

    public Island getFourConnected( Sand sand )
    {
        sand.setNotVisited( false );
        Island neighbours = new Island();
        int x = sand.getX();
        int y = sand.getY();
        int z = sand.getZ();
        Sand n1 = getSand( x, y - 1, z );
        Sand n2 = getSand( x, y + 1, z );
        Sand n3 = getSand( x - 1, y, z );
        Sand n4 = getSand( x + 1, y, z );
        addSandToIsland( n1, neighbours );
        addSandToIsland( n2, neighbours );
        addSandToIsland( n3, neighbours );
        addSandToIsland( n4, neighbours );
        return neighbours;
    }

    public Island getEightConnected( Sand sand )
    {
        sand.setNotVisited( false );
        Island neighbours = new Island();
        int x = sand.getX();
        int y = sand.getY();
        int z = sand.getZ();
        Sand n1 = getSand( x - 1,y - 1, z );
        Sand n2 = getSand( x - 1, y, z );
        Sand n3 = getSand( x - 1,y - 1, z );
        Sand n4 = getSand( x, y - 1, z );
        Sand n5 = getSand( x, y + 1, z );
        Sand n6 = getSand( x + 1,y - 1, z );
        Sand n7 = getSand( x + 1, y, z );
        Sand n8 = getSand( x + 1,y + 1, z );

        addSandToIsland( n1, neighbours );
        addSandToIsland( n2, neighbours );
        addSandToIsland( n3, neighbours );
        addSandToIsland( n4, neighbours );
        addSandToIsland( n5, neighbours );
        addSandToIsland( n6, neighbours );
        addSandToIsland( n7, neighbours );
        addSandToIsland( n8, neighbours );

        return neighbours;
    }

    public Sand getSand( int x, int y, int z )
    {
        try
        {
            return world[ z ][ y ][ x ];
        }
        catch ( ArrayIndexOutOfBoundsException out )
        {
            return null;
        }
    }

    public void setSand(int x, int y, int z)
    {
        Sand sand = new Sand( x, y, z );
        world[z][y][x] = sand;
    }

    private void addSandToIsland( Sand sand, Island island )
    {
        if ( sand != null )
        {
            island.add( sand );
        }
    }

    public Island buildIsland( Sand sand )
    {
        Island island = new Island();
        island.add( sand );
        Queue< Sand > queue = new LinkedList<>();
        queue.add( sand );
        while ( ! queue.isEmpty() )
        {
            Sand s0 = queue.remove();
            Island temp = getNeighbour( s0 );
            for ( Sand s : temp )
            {
                if ( s.isNotVisited() )
                {
                    queue.add( s );
                }
                island.add( s );
            }
            if ( island.size() > islandSize  )
            {
                island.reset();
                return island;
            }
        }
        island.reset();
        return island;
    }

    public void setIslandStatus( Sand sand )
    {

        Island island = buildIsland( sand );
        if ( island.size() > islandSize )
        {
            island.forEach( sand1 -> sand1.setIslandStatus( 1 ) );
        }
        else if ( sand.getZ() < depth && sand.getZ() > 0 )
        {
            int sizeUp = getExtendedIslandSize( island, 1 );
            if ( sizeUp == 0 )
            {
                int sizeDown = getExtendedIslandSize( island, - 1 );
                setExtendedIslandSize( sizeDown, island );
            }
            else
            {
                island.forEach( sand1 -> sand1.setIslandStatus( 1 ) );
            }
        }

        else if ( sand.getZ() == depth )
        {
            int sizeDown = getExtendedIslandSize( island, - 1 );
            setExtendedIslandSize( sizeDown, island );
        }
        else
        {
            int sizeUp = getExtendedIslandSize( island, 1 );
            setExtendedIslandSize( sizeUp, island );
        }
    }

    public Island getIslandImage( Island island, int level )
    {
        Island islandImage = new Island();
        for ( Sand sand : island )
        {
            islandImage.add( new Sand( sand.getX(), sand.getY(), sand.getZ() + level ) );
        }
        return islandImage;
    }


    public Island extendIsland( Island imageIsland )
    {
        Island extendIsland = new Island();
        for ( Sand sand : imageIsland )
        {
            extendIsland.addAll( getNeighbour( sand ) );
        }
        return extendIsland;
    }

    private int getExtendedIslandSize( Island island, int level )
    {
        Island upIsland = getIslandImage( island, level );
        Island extendedIsland = extendIsland( upIsland );
        return extendedIsland.size();
    }


    private void setExtendedIslandSize( int size, Island island )
    {
        if ( size == 0 )
        {
            island.forEach( sand1 -> sand1.setIslandStatus( 0 ) );
        }
        else
        {
            island.forEach( sand1 -> sand1.setIslandStatus( 1 ) );
        }
    }

    public int getDepth()
    {
        return depth;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }
}
