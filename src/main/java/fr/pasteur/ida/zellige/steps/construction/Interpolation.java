/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.construction;

import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.numeric.integer.UnsignedShortType;
import net.imglib2.util.ImgUtil;


/**
 * @author ctrebeau
 * Methods to perform 2D linear interpolation on a 2D image.
 */
public class Interpolation
{

    public static Img< UnsignedShortType > run( RandomAccessibleInterval< UnsignedShortType > input )
    {
        InterpolationOperator operator = new InterpolationOperator( input );
        return operator.getOutput();
    }

    public static class InterpolationOperator
    {
        private final RandomAccessibleInterval< UnsignedShortType > input;
        private Img< UnsignedShortType > output;


        public InterpolationOperator( RandomAccessibleInterval< UnsignedShortType > input )
        {
            this.input = input;
            execute();
        }

        /**
         * Returns an interpolated version of the specified image.
         * The interpolation is the mean of the interpolated image in both dimensions dimension.
         */
        private void execute()
        {
//            ImageJFunctions.show( input, "zMap" );
            Img< UnsignedShortType > interpolated0 = execute( input, 0 );
//        ImageJFunctions.show( interpolated0, "interpolated 0" );
            Img< UnsignedShortType > interpolated1 = execute( input, 1 );
//        ImageJFunctions.show( interpolated1, "interpolated 1" );
            Cursor< UnsignedShortType > cursor0 = interpolated0.cursor();
            Cursor< UnsignedShortType > cursor1 = interpolated1.cursor();

            while ( cursor0.hasNext() )
            {
                cursor0.fwd();
                cursor1.fwd();
                int type0 = cursor0.get().getInteger();
                int type1 = cursor1.get().getInteger();
                if ( type0 != 0 )
                {
                    if ( type1 != 0 )
                    {
                        int mean = ( int ) ( Math.round( type0 + type1 ) / 2.0 );
                        cursor0.get().set( mean );
                    }
                }
                else if ( type1 != 0 )
                {
                    cursor0.get().set( cursor1.get() );
                }
            }
            this.output = interpolated0;
        }

        /**
         * Determines the slope of the linear equation y = ax + b given 2 points P1(x1, y1) and P2 (x2, y2).
         *
         * @param x1 - x coordinates for the first point.
         * @param x2 - x coordinates for the second point.
         * @param z1 - z coordinates for the first point.
         * @param z2 - z coordinates for the second point.
         * @return the slope of the linear equation y = ax + b.
         */
        private double getA( int x1, int x2, double z1, double z2 )
        {
            return ( z2 - z1 ) / ( double ) ( x2 - x1 );
        }

        /**
         * Determines the constant b of the linear equation y = ax + b given one point P1(x1, y1) and the slope 'a'.
         *
         * @param a  - the slope 'a' of the linear equation y = ax + b.
         * @param x1 - x coordinates for the first point.
         * @param z1 - z coordinates for the first point.
         * @return the b constant of the linear equation y = ax + b.
         */
        private double getB( double a, int x1, double z1 )
        {
            return z1 - ( a * x1 );
        }


        /**
         * Determines the unknown z coordinate of a point P given its x coordinate,
         * the slope and the constant of the linear equation y = ax + b.
         *
         * @param a - the slope 'a' of the linear equation y = ax + b.
         * @param b - the b constant of the linear equation y = ax + b.
         * @param x - the x coordinate of a point with unknown z coordinate.
         * @return - the z coordinate corresponding to the x coordinate.
         */
        private int executeInt( double a, double b, int x )
        {
            return ( int ) Math.round( a * x + b );
        }


        /**
         * Determines the unknown z coordinate of a point P using the linear equation y = ax + b,
         * given its x coordinate and  2 points P1(x1, y1) and P2 (x2, y2).
         *
         * @param x1 - x coordinates for the first point.
         * @param x2 - x coordinates for the second point.
         * @param z1 - z coordinates for the first point.
         * @param z2 - z coordinates for the second point.
         * @param x  - the x coordinate of a point with unknown z coordinate.
         * @return * @return - the z coordinate corresponding to the x coordinate.
         */
        private int executeInt( int x1, int x2, double z1, double z2, int x )
        {
            double a = getA( x1, x2, z1, z2 );
            double b = getB( a, x1, z1 );
            return executeInt( a, b, x );
        }

        /**
         * Returns an interpolated version of the specified image in the specified dimension.
         *
         * @param input     the image to interpolate.
         * @param dimension the dimension of the interpolation
         * @return the interpolated image in the specified dimension
         */
        private Img< UnsignedShortType > execute( RandomAccessibleInterval< UnsignedShortType > input, int dimension )
        {
            ImgFactory< UnsignedShortType > factory = new ArrayImgFactory<>( new UnsignedShortType() );
            Img< UnsignedShortType > interpolated = factory.create( input );
            ImgUtil.copy( input, interpolated );
            RandomAccess< UnsignedShortType > interpolatedAccess = interpolated.randomAccess();
            RandomAccess< UnsignedShortType > zMapAccess = input.randomAccess();
            int secondDimension = Math.abs( dimension - 1 );
            int indexMax = ( int ) input.dimension( secondDimension ) - 1;
            for ( int u = 0; u <= input.dimension( dimension ) - 1; u++ )
            {
                zMapAccess.setPosition( u, dimension );
                interpolatedAccess.setPosition( u, dimension );
                int X1;
                int X2;
                for ( int v = 0; v <= input.dimension( secondDimension ) - 1; v++ )
                {

                    zMapAccess.setPosition( v, secondDimension );

                    interpolatedAccess.setPosition( v, secondDimension );
                    if ( zMapAccess.get().getInteger() == 0 )// there no value at this position
                    {
                        X1 = v - 1;// the previous index.
                        if ( X1 >= 0 )
                        {
                            zMapAccess.setPosition( X1, secondDimension );
                            int Y1 = zMapAccess.get().copy().getInteger();
                            if ( Y1 != 0 )
                            {
                                X2 = searchX2( zMapAccess, v, indexMax, secondDimension );
                                if ( X2 < indexMax && X2 - X1 <= 50 ) //an interpolation can only be done for a distance of less or equals to 10 pixels.
                                {
                                    zMapAccess.setPosition( X2, secondDimension );
                                    int Y2 = zMapAccess.get().copy().getInteger();
                                    for ( int i = v; i <= X2; i++ )// interpolation between the interval [X1, X2].
                                    {
                                        int result = executeInt( X1, X2, Y1, Y2, i );
                                        interpolatedAccess.setPosition( i, secondDimension );
                                        interpolatedAccess.get().set( result );
                                    }
                                }
                                v = X2;// we don't need to search in the interval [X1, X2].
                            }
                        }
                    }
                }
            }
            return interpolated;
        }


        /**
         * This method returns the first position  where the pixel value is different from 0 or the maximum index otherwise.
         *
         * @param randomAccess the {@link RandomAccess} of the image to interpolate.
         * @param x            the starting index.
         * @param max          the maximum index value.
         * @param dim          - the dimension to search in.
         * @return the index at which there is a value > 0 or the maximum index value otherwise.
         */
        private int searchX2( RandomAccess< UnsignedShortType > randomAccess, int x, int max, int dim )
        {
            while ( x + 1 <= max )
            {
                randomAccess.setPosition( x + 1, dim );
                if ( randomAccess.get().copy().getInteger() != 0 )
                {
                    return x + 1;
                }
                x++;
            }
            return max;
        }

        public Img< UnsignedShortType > getOutput()
        {
            return output;
        }
    }


}
