/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.construction.rounds;

import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.element.ose.OSEListArray;
import fr.pasteur.ida.zellige.steps.construction.exception.NoSurfaceFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FirstRoundConstruction extends ConstructionRound
{
    private final static Logger LOGGER = LoggerFactory.getLogger( FirstRoundConstruction.class );

    private final Pixels[][] maximums;

    public FirstRoundConstruction( Pixels[][] maximums, ConstructionParameters constructionParameters )
    {
        super( constructionParameters );
        this.maximums = maximums;
    }

    public FirstRoundConstruction( Pixels[][] maximums, final double startingSizeThreshold,
                                   final int overlap,
                                   final double connexityRate,
                                   final double surfaceMinSizeFactor )
    {
        super( startingSizeThreshold, overlap, connexityRate, surfaceMinSizeFactor );
        this.maximums = maximums;
    }

    public void process() throws NoSurfaceFoundException
    {
        LOGGER.debug( "Processing..." );
        /* OSE construction according to the dimension considered */
        long startOseConstructionTime = System.currentTimeMillis();
        OSEListArray oseLists = constructOSE( maximums );
        long stopOseConstructionTime = System.currentTimeMillis();
        this.setOSConstructionProcessingTime( stopOseConstructionTime - startOseConstructionTime );
        /* Construction of surfaces*/
        LOGGER.debug( "Starting surface construction..." );
        int lineLength = maximums[ 0 ].length;
        long startSurfacesConstructionTime = System.currentTimeMillis();
        this.getSurfaces().addAll( constructSurfaces( oseLists, lineLength ) );
        long stopSurfacesConstructionTime = System.currentTimeMillis();
        this.setConstructionProcessingTime( stopSurfacesConstructionTime - startSurfacesConstructionTime );
        LOGGER.debug( "Finalizing  : {} surfaces", this.getSurfaces().size() );
        this.finalizeSurface( this.getSurfaces() );
        LOGGER.debug( "Round completed : {} surfaces found", this.getSurfaces().size() );
    }
}
