/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.classification;

import fr.pasteur.ida.zellige.steps.Utils;
import net.imglib2.*;
import net.imglib2.algorithm.util.Grids;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class OtsuClassification< T extends RealType< T > & NativeType< T > >
{

    private final static Logger LOGGER = LoggerFactory.getLogger( OtsuClassification.class );
    private static long processingTime;
    private final RandomAccessibleInterval< T > input;
    private final Img< T > grid;
    private Img< BitType > output;
    private int userThreshold;

    /**
     * @param input         the input image as a {@link RandomAccessibleInterval}
     * @param factory       the input factory
     * @param output        the resulting binary image as a {@link Img<BitType> }
     * @param userThreshold the user's intensity threshold
     */
    private OtsuClassification( final RandomAccessibleInterval< T > input, final ImgFactory< T > factory, final Img< BitType > output, int userThreshold )
    {
        this.input = input;
        this.output = output;
        this.userThreshold = userThreshold;
        this.grid = factory.create( input );
    }

    /**
     * @param input   the input image as a {@link RandomAccessibleInterval}
     * @param factory the input factory
     */
    private OtsuClassification( final RandomAccessibleInterval< T > input, final ImgFactory< T > factory )
    {
        this.input = input;
        this.grid = factory.create( input );
    }

    /**
     * @param input         the input image as a {@link RandomAccessibleInterval}
     * @param factory       the {@link ImgFactory} of the input image
     * @param userThreshold the percentage of global otsu value
     * @param <T>           the type on the input
     * @return a 3D binary image
     */
    public static < T extends RealType< T > & NativeType< T > > Img< BitType > run( final RandomAccessibleInterval< T > input, ImgFactory< T > factory, int userThreshold )
    {
        if ( userThreshold > 0 )
        {
            long start = System.currentTimeMillis();
            // Prepare output.
            final ImgFactory< BitType > bitTypeImgFactory = net.imglib2.util.Util.getArrayOrCellImgFactory( input, new BitType() );
            Img< BitType > binary = bitTypeImgFactory.create( input );
            OtsuClassification< T > classification = new OtsuClassification<>( input, factory, binary, userThreshold );
            classification.run();
            long stop = System.currentTimeMillis();
            LOGGER.debug( "Otsu classification done in {} s", ( ( stop - start ) / 1000.0 ) );
            classification.setProcessingTime( ( stop - start ) );
            return classification.getOutput();
        }
        else
        {
            return null;
        }
    }

    /**
     * @param input   the input image as a {@link RandomAccessibleInterval}
     * @param factory the {@link ImgFactory} of the input image
     * @param <T>     the type on the input
     * @return an {@link Img}
     */
    public static < T extends RealType< T > & NativeType< T > > Img< T > computeOtsuImage( final RandomAccessibleInterval< T > input, ImgFactory< T > factory )
    {
        OtsuClassification< T > classification = new OtsuClassification<>( input, factory );
        classification.computeLocalThreshold( input, classification.grid );
        return classification.getGrid();
    }


    /**
     * @param input         the input image as a {@link RandomAccessibleInterval}
     * @param grid          the local thresholds image as a {@link RandomAccessibleInterval}
     * @param userThreshold the user threshold
     * @param <T>           the type on the input
     * @return the classified image as an {@link Img}
     */
    public static < T extends RealType< T > & NativeType< T > > Img< BitType >
    applyLocalThreshold( IterableInterval< T > input, IterableInterval< T > grid, int userThreshold )
    {
        long start = System.currentTimeMillis();
        final ImgFactory< BitType > bitTypeImgFactory = net.imglib2.util.Util.getArrayOrCellImgFactory( input, new BitType() );
        Img< BitType > output = bitTypeImgFactory.create( input );
        Cursor< T > sourceCursor = input.localizingCursor();
        Cursor< T > gridCursor = grid.cursor();
        RandomAccess< BitType > randomAccess = output.randomAccess();
        while ( sourceCursor.hasNext() )
        {
            sourceCursor.fwd();
            gridCursor.fwd();
            final double intensityValue = sourceCursor.get().getRealDouble();
            final double localThreshold = gridCursor.get().getRealDouble();
            {
                if ( intensityValue >= Math.max( localThreshold, userThreshold ) )// background subtraction
                {
                    randomAccess.setPosition( sourceCursor );
                    randomAccess.get().set( true );
                }
            }
        }
        long stop = System.currentTimeMillis();
        LOGGER.debug( "Threshold applied in {} s.", ( ( stop - start ) / 1000.0 ) );
        return output;
    }


    /**
     * @param input the input {@link Img}
     * @param grid  the image containing the grid of local thresholds
     */
    private void
    computeLocalThreshold( RandomAccessibleInterval< T > input, RandomAccessibleInterval< T > grid )
    {
        long start = System.currentTimeMillis();
        long width = input.dimension( 0 );
        long height = input.dimension( 1 );
        long depth = input.dimension( 2 );
        List< Interval > intervals = Grids.collectAllContainedIntervals( new long[]{ width, height, depth },
                new int[]{ 40, 40, 3 } );

        for ( Interval interval : intervals )
        {
            computeLocalThreshold( input, grid, interval );
        }
        Utils.gaussianConvolution( grid, grid, new double[]{ 5, 5, 1 } );
        long stop = System.currentTimeMillis();
        LOGGER.debug( "Local thresholds computed in {} s.", ( ( stop - start ) / 1000.0 ) );

    }

    /**
     * This method applies an Otsu threshold on a specified interval on a given image.
     *
     * @param input    the input image as a {@link RandomAccessibleInterval}
     * @param grid     the local thresholds image as a {@link RandomAccessibleInterval}
     * @param interval the considered interval of the image as a {@link Interval}
     */

    private void
    computeLocalThreshold( RandomAccessibleInterval< T > input, RandomAccessibleInterval< T > grid, Interval interval )
    {
        IntervalView< T > viewSource = Views.offsetInterval( input, interval );
        IntervalView< T > viewGrid = Views.offsetInterval( grid, interval );
        T threshold = Otsu.getThreshold( viewSource );
        viewGrid.forEach( pixel -> pixel.set( threshold ) );
    }

    /**
     *
     */
    private void run()
    {
        computeLocalThreshold( input, grid );

        output = applyLocalThreshold( Views.iterable( input ), Views.iterable( grid ), userThreshold );
        LOGGER.debug( "Local thresholds computed with user value  = {}.", this.userThreshold );

    }

    /**
     * @return the image containing the grid of local thresholds
     */
    public Img< BitType > getOutput()
    {
        return output;
    }

    private Img< T > getGrid()
    {
        return grid;
    }


    public static long getProcessingTime()
    {
        return processingTime;
    }

    public void setProcessingTime( long time )
    {
        processingTime = time;
    }
}






