/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.projection;

import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;

public class ProjectionParameters
{

    private final int delta;
    private final String projectionMethod;


    public ProjectionParameters( int delta, String projectionMethod ) throws DataValidationException
    {
        projectionParametersValidationCheck( delta, projectionMethod );
        this.delta = delta;
        this.projectionMethod = projectionMethod;
    }

    /**
     * Checks if the delta value is superior or equal to zero and throws an {@link DataValidationException} otherwise.
     * @param value the delta value choose by the user. //TODO javadoc : extracted volume defined as delta x 2 + 1
     * @throws DataValidationException - if the value is under zero.
     */
    private void deltaValidationCheck( int value ) throws DataValidationException
    {
        if ( value < 0 )
        {
            throw new DataValidationException( "The value of parameter Delta has to be superior to zero !" );
        }
    }

    /**
     *
     * @param projectionMethod the method choose to projection the extracted surface.
     * @throws DataValidationException if the method is not implemented (implemented methods are MIP, Mean, Median and MinIP).
     */
    private void projectionMethodValidationCheck( String projectionMethod ) throws DataValidationException
    {
        if ( ! projectionMethod.equals( "MIP" ) && ! projectionMethod.equals( "Mean" ) && ! projectionMethod.equals( "MinIP" ) && ! projectionMethod.equals( "Median" ) )
        {
            throw new DataValidationException( String.format( "The method %s is not implemented in Zellige !", projectionMethod ) );
        }
    }

    /**
     * Checks the validity of both parameters.
     *
     * @param delta            the extracted stack parameters
     * @param projectionMethod the method choose to projection the extracted surface.
     * @throws DataValidationException if at least one parameter is not valid.
     */
    private void projectionParametersValidationCheck( int delta, String projectionMethod ) throws DataValidationException
    {
        deltaValidationCheck( delta );
        projectionMethodValidationCheck( projectionMethod );
    }

    public int getDelta()
    {
        return delta;
    }

    public String getProjectionType()
    {
        return projectionMethod;
    }
}
