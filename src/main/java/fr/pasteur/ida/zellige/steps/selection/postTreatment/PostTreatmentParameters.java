/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.postTreatment;

import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;

public class PostTreatmentParameters
{

    public static final String SIGMA_XY = "Sigma XY";
    public static final String SIGMA_Z = "Sigma Z";
    public static final String ISLAND_SIZE = "Island size";
    public static final String CONNEXITY = "connexity";
    private final double sigmaXY, sigmaZ;
    private final int islandSize, connexity;


    public PostTreatmentParameters( double sigmaXY, double sigmaZ, int islandSize, int connexity ) throws DataValidationException
    {
        postTreatmentParametersValidationCheck( sigmaXY, sigmaZ , islandSize);
        this.sigmaXY = sigmaXY;
        this.sigmaZ = sigmaZ;
        this.islandSize = islandSize;
        this.connexity = connexity;
    }


    /**
     * Checks the four parameters validity.
     *
     * @param sigmaXY    the value choose for the XY image dilatation.
     * @param sigmaZ     the value choose for the Z image dilatation.
     * @param islandSize the minimum size of the island search
     * @throws DataValidationException if at least one parameter is not valid.
     */
    public static void postTreatmentParametersValidationCheck( double sigmaXY, double sigmaZ, int islandSize ) throws DataValidationException
    {
        valueValidationCheck( SIGMA_XY, sigmaXY );
        valueValidationCheck( SIGMA_Z, sigmaZ );
        valueValidationCheck( ISLAND_SIZE, islandSize );
    }

    /**
     * @param name  the parameter name to display in the error message.
     * @param value the value of the parameter tested.
     * @throws DataValidationException if the value is inferior to zero.
     */
    private static void valueValidationCheck( String name, double value ) throws DataValidationException
    {
        if ( name.equals( CONNEXITY ) )
        {
            if ( value != 4 && value != 8 )
            {
                throw new DataValidationException( "The value of parameter " + name + " has to be 4 or 8 !" );
            }
        }
        else
        {
            if ( value < 0 )
            {
                throw new DataValidationException( "The value of parameter " + name + " has to be superior to zero !" );
            }

        }
    }

    public double getSigmaXY()
    {
        return sigmaXY;
    }

    public double getSigmaZ()
    {
        return sigmaZ;
    }

    public int getIslandSize()
    {
        return islandSize;
    }

    public int getConnexity()
    {
        return connexity;
    }
}
