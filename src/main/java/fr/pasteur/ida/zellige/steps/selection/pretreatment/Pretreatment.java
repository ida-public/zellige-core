/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.pretreatment;

import net.imglib2.RandomAccessible;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.gauss3.Gauss3;
import net.imglib2.algorithm.stats.Normalize;
import net.imglib2.converter.Converters;
import net.imglib2.converter.readwrite.RealFloatSamplerConverter;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import net.imglib2.util.ImgUtil;
import net.imglib2.view.Views;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Pretreatment< T extends RealType< T > & NativeType< T > >
{
    private final static Logger LOGGER = LoggerFactory.getLogger( Pretreatment.class );

    private final RandomAccessibleInterval< T > input;
    //    private final PretreatmentParameters pretreatmentParameters;
    private final double radius;
    private Img< FloatType > output;


    Pretreatment( RandomAccessibleInterval< T > input, double radius )
    {
        this.input = input;
        this.radius = radius;
    }


    public static < T extends RealType< T > & NativeType< T > > Img< FloatType >
    run( RandomAccessibleInterval< T > input, double radius )
    {
        LOGGER.debug( "Starting process..." );
        Pretreatment< T > pretreatment = new Pretreatment<>( input, radius );
        pretreatment.run();
        LOGGER.debug( "Process complete." );
        return pretreatment.getOutput();
    }

    public static < T extends RealType< T > & NativeType< T > > Img< FloatType >
    run( RandomAccessibleInterval< T > input )
    {
        LOGGER.debug( "Starting process..." );
        Pretreatment< T > pretreatment = new Pretreatment<>( input, 2 );
        pretreatment.run();
        LOGGER.debug( "Process complete." );
        return pretreatment.getOutput();
    }


    /**
     * Normalizes the pixel values of an Img between 0 and 255.
     *
     * @param <T>     - the type of the input Img.
     * @param image   - the input Img.
     * @param factory - the image factory.
     * @return a new Img with normalize pixel intensity values.
     */
    private static < T extends RealType< T > & NativeType< T > > Img< T > normalizeImage(
            Img< T > image, ImgFactory< T > factory )
    {
        Img< T > normalizedImage = factory.create( image );
        ImgUtil.copy( image, normalizedImage );
        T min = normalizedImage.firstElement().createVariable();
        min.setReal( 0 );
        T max = normalizedImage.firstElement().copy().createVariable();
        max.setReal( 255 );
        Normalize.normalize( normalizedImage, min, max );
        return normalizedImage;
    }

    /**
     * Applies the denoising step on the specified input according to the specified method
     *
     * @param input the noisy image as a {@link RandomAccessibleInterval}
     * @return a denoised image as a  {@link Img}
     */
    Img< FloatType > denoisingStep( RandomAccessibleInterval< FloatType > input )
    {
        LOGGER.debug( "Starting denoising step..." );
        RandomAccessibleInterval< FloatType > converted = Converters.convert( input, new RealFloatSamplerConverter<>() );

        LOGGER.debug( "Denoising step complete, with gaussian filter" );
        return gaussianBlurFilterDenoising( converted );

    }

    /**
     * @param input   -  the source image as a {@link RandomAccessibleInterval}
     * @param factory - the source and output factory
     * @param sigma   - an array containing the standard deviations for each dimension.
     * @param <T>     - source and output type
     * @return - the filtered image as a {@link RandomAccessibleInterval}
     */
    public static < T extends RealType< T > & NativeType< T > > Img< T >
    gaussianConvolution( RandomAccessibleInterval< T > input, ImgFactory< T > factory, double[] sigma )
    {
        Img< T > output = factory.create( input );
        RandomAccessible< T > infiniteInput = Views.extendValue( input, input.randomAccess().get().getRealFloat() );
        Gauss3.gauss( sigma, infiniteInput, output );
        return output;
    }

    void run()
    {
        // The input is converted into FloatType
        RandomAccessibleInterval< FloatType > converted = Converters.convert( input, new RealFloatSamplerConverter<>() );
        LOGGER.debug( "Input converted." );
        //The choose method of denoising is applied
        Img< FloatType > denoised = denoisingStep( converted );
        // The denoised image is normalized between 0 and 255
        this.output = normalizeImage( denoised, denoised.factory() );
        LOGGER.debug( "Input normalized." );

    }

    /**
     * Applies a gaussian filter to a noisy image
     *
     * @param input the noisy image as a {@link RandomAccessibleInterval}
     * @return a denoised image as a  {@link Img}
     */
    Img< FloatType > gaussianBlurFilterDenoising( RandomAccessibleInterval< FloatType > input )
    {
        ImgFactory< FloatType > factory = new ArrayImgFactory<>( new FloatType() );
        return gaussianConvolution( input, factory, new double[]{ radius, radius, 1 } );
    }

    public RandomAccessibleInterval< T > getInput()
    {
        return input;
    }


    public Img< FloatType > getOutput()
    {
        return output;
    }
}
