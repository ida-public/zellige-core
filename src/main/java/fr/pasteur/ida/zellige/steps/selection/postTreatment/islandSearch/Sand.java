/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.postTreatment.islandSearch;

public class Sand
{
    /**
     * The position in dimension X.
     */
    private final int x;
    /**
     * The position in dimension Y.
     */
    private final int y;
    /**
     * The position in dimension Z.
     */
    private final int z;

    /**
     * If the Sand belongs to an island or not.
     */
    private int islandStatus = -1;

    private boolean noVisited = true;

    /**
     * Constructor
     *
     * @param x - the coordinates for the first dimension.
     * @param y - the coordinates for the second dimension.
     * @param z - the coordinates for the third dimension.
     */
    public Sand( int x, int y, int z )
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     *
     * @return -1, if the status as not been set yet, 1 if it's not an island and 0 otherwise
     */
    public int getIslandStatus()
    {
        return islandStatus;
    }

    public void setIslandStatus( int islandStatus )
    {
        this.islandStatus = islandStatus;
    }

    /**
     * Returns the value of x.
     *
     * @return the value of x
     *
     */
    public int getX()
    {
        return x;
    }

    /**
     * Returns the value of y.
     * @return the value of y
     */
    public int getY()
    {
        return y;
    }

    /**
     * Returns the z value of this object.
     *
     * @return the value of z
     */
    public int getZ()
    {
        return z;
    }

    public boolean isNotVisited()
    {
        return noVisited;
    }

    public void setNotVisited( boolean visited )
    {
        this.noVisited = visited;
    }

    @Override
    public boolean equals( Object obj )
    {
        if ( obj instanceof Sand )
        {
            Sand sand = ( Sand ) obj;

            return sand.x == this.x && sand.y == this.y && sand.z == this.z;

        }
        return false;
    }


}
