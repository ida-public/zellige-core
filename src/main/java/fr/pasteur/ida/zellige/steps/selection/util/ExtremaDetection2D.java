/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.util;

import fr.pasteur.ida.zellige.steps.Utils;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.ImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

import static fr.pasteur.ida.zellige.steps.selection.util.ExtremaDetection.MAX;

public class ExtremaDetection2D< T extends RealType< T > & NativeType< T > >
{

    private final RandomAccessibleInterval< T > inputSection;
    private final ImgFactory< T > factory;
    private final RandomAccessibleInterval< T > extrema;
    private final String type;
    private final String method;


    private ExtremaDetection2D( RandomAccessibleInterval< T > inputSection, ImgFactory< T > factory, RandomAccessibleInterval< T > extrema, String type, String method )
    {
        this.inputSection = inputSection;
        this.factory = factory;
        this.extrema = extrema;
        this.type = type;
        this.method = method;
    }

    /**
     * Sets the maxima using partial derivative on an 2D image.
     *
     * @param inputSection the 2D image from which the extrema need to be detected
     * @param factory      the factory of the input section
     * @param extrema      the 2D image containing the maxima
     * @param type         the type of extrema detection : minima detection or maxima detection ('min' or 'max')
     * @param method       the method to used for the partial derivative computation
     * @param <T>          the input section type
     */
    public static < T extends RealType< T > & NativeType< T > > void run(
            RandomAccessibleInterval< T > inputSection, ImgFactory< T > factory,
            RandomAccessibleInterval< T > extrema, String type, String method )
    {
        ExtremaDetection2D< T > detection2D = new ExtremaDetection2D<>( inputSection, factory, extrema, type, method );
        detection2D.computeExtrema();
    }

    private void computeExtrema()
    {
        {
            // Output for partial derivative.
            RandomAccessibleInterval< T > sectionDerivative = factory.create( inputSection );
            // Partial derivative computation */
            Derivative.run( inputSection, sectionDerivative, method );
            computeExtrema( inputSection, sectionDerivative );
        }
    }


    /**
     * @param section           the section considered
     * @param sectionDerivative the corresponding partial derivative section of the input image as a {@link RandomAccessibleInterval}
     */
    private void
    computeExtrema
    ( RandomAccessibleInterval< T > section, RandomAccessibleInterval< T > sectionDerivative
    )
    {
        RandomAccess< T > derivativeAccess = Views.extendMirrorSingle( sectionDerivative ).randomAccess();
        RandomAccess< T > extremaAccess = extrema.randomAccess();
        RandomAccess< T > oriAccess = section.randomAccess();

        for ( int u = 0; u <= extrema.dimension( 0 ) - 1; u++ )
        {
            derivativeAccess.setPosition( u, 0 );
            for ( int v = 0; v < extrema.dimension( 1 ) - 1; v++ )
            {
                if ( isALocalExtrema( derivativeAccess, v ) )
                {
                    Utils.setPosition( oriAccess, u, v + 1 );
                    Utils.setPosition( extremaAccess, u, v + 1 );
                    extremaAccess.get().set( oriAccess.get() );
                }
            }
        }
    }

    /**
     * Method to find if a pixel of a given stack is a local maximum.
     * <p>
     * (X or Y for respectively XZ sections and YZ sections).
     *
     * @param derivative - the first {@link RandomAccess} for the partial derivative image
     *                   set at index v for dimension 0.
     * @param z          - the index of the Z dimension
     * @return - true if the position (v, z) is a local maximum false otherwise.
     */
    private boolean isALocalExtrema( RandomAccess< T > derivative, int z ) //throws Exception
    {
        derivative.setPosition( z, 1 );
        final double tZero = derivative.get().getRealDouble();
        derivative.setPosition( ( z + 1 ), 1 );
        final double tOne = derivative.get().getRealDouble();
        if ( type.equals( MAX ) )
        {
            return ( tZero >= 0 && tOne < 0 );
        }
        else
        {
            return ( tZero < 0 && tOne >= 0 );
        }
    }
}
