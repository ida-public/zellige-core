/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.construction;

import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.element.ReferenceSurface;
import fr.pasteur.ida.zellige.element.Surface;
import fr.pasteur.ida.zellige.element.surfaceLine.SurfaceLine;
import net.imglib2.Dimensions;
import net.imglib2.FinalDimensions;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.UnsignedShortType;
import net.imglib2.util.Util;

import java.util.ArrayList;


/**
 * This class allows to get a height map from a {@link Surface} by averaging the Z values or discarding them if not possible (getZMap())
 * and by interpolating when possible if there is any missing internal values (processZMap()).
 *
 * @param <T> the input type
 */
public class ConstructionCompletion< T extends RealType< T > & NativeType< T > >
{
    private final ArrayList< ReferenceSurface< T > > referenceSurfaces;


    public ConstructionCompletion()
    {

        this.referenceSurfaces = new ArrayList<>();
    }

    public static < T extends RealType< T > & NativeType< T > > ArrayList< ReferenceSurface< T > > run(
            RandomAccessibleInterval< T > input, ImgFactory< T > factory, ArrayList< Surface > surfaces )
    {
        ConstructionCompletion< T > completion = new ConstructionCompletion<>();
        completion.referenceSurfaceInstantiation( input, factory, surfaces );
        return completion.getReferenceSurfaces();
    }

    private void referenceSurfaceInstantiation( RandomAccessibleInterval< T > input,
                                                ImgFactory< T > factory, ArrayList< Surface > finalSurfaces )
    {
        int index = 0;
        for ( Surface surface : finalSurfaces )
        {
            Img< UnsignedShortType > processedZMap = processedZMap( surface );

            ReferenceSurface< T > referenceSurface = new ReferenceSurface<>( input, factory, processedZMap, index++ );
            referenceSurfaces.add( referenceSurface );
        }
    }

    public Img< UnsignedShortType > processedZMap( Surface surface )
    {
        Img< UnsignedShortType > zMap = getZMap( surface );
        return Interpolation.run( zMap );
    }

    public Img< UnsignedShortType > getZMap( Surface surface )
    {
        Dimensions dim = FinalDimensions.wrap( new long[]{ surface.getHeight(), surface.getWidth() } );
        ImgFactory< UnsignedShortType > factory = Util.getArrayOrCellImgFactory( dim, new UnsignedShortType() );
        Img< UnsignedShortType > zMap = factory.create( dim );
        RandomAccess< UnsignedShortType > randomAccess = zMap.randomAccess();
        for ( SurfaceLine surfaceLine : surface.get() )
        {
            if ( surfaceLine != null )
            {
                randomAccess.setPosition( surfaceLine.getLine(), 0 );
                for ( int i = 0; i <= surfaceLine.getDimension().length - 1; i++ )
                {
                    Pixels pixel = surfaceLine.get( i );
                    if ( pixel != null && pixel.size() <= 2 )// only one or two Coordinates
                    {
                        if ( pixel.size() > 1 )
                        {
                            int z = averageIntZ( pixel );
                            if ( z != - 1 )
                            {
//                                LOGGER.debug( "only two : " + pixel );
                                randomAccess.setPosition( i, 1 );
                                randomAccess.get().set( new UnsignedShortType( z + 1 ) );
                            }
                        }
                        else
                        {
                            int z = pixel.get( 0 ).getZ();
                            randomAccess.setPosition( i, 1 );
                            randomAccess.get().set( new UnsignedShortType( z + 1 ) );
                        }
                    }
                }
            }
        }
        return zMap;
    }

    /**
     * Returns the Z mean of a specified {@link Pixels} of size 2.
     *
     * @param pixels the list of Coordinates
     * @return the mean Z of a specified coordinateList
     */
    private int averageIntZ( Pixels pixels )
    {
        int z1 = pixels.get( 0 ).getZ();
        int z2 = pixels.get( 1 ).getZ();
        if ( Math.abs( z1 - z2 ) <= 2 )
        {
            return ( z1 + z2 ) / 2;
        }
        return - 1;
    }

    public ArrayList< ReferenceSurface< T > > getReferenceSurfaces()
    {
        return referenceSurfaces;
    }
}
