/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection;

import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.steps.selection.classification.AmplitudeClassification;
import fr.pasteur.ida.zellige.steps.selection.classification.Classification;
import fr.pasteur.ida.zellige.steps.selection.classification.ClassificationParameters;
import fr.pasteur.ida.zellige.steps.selection.classification.OtsuClassification;
import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;
import fr.pasteur.ida.zellige.steps.selection.exception.NoClassificationException;
import fr.pasteur.ida.zellige.steps.selection.postTreatment.PostTreatment;
import fr.pasteur.ida.zellige.steps.selection.postTreatment.PostTreatmentParameters;
import fr.pasteur.ida.zellige.steps.selection.postTreatment.islandSearch.IslandSearch;
import fr.pasteur.ida.zellige.steps.selection.pretreatment.Pretreatment;
import fr.pasteur.ida.zellige.steps.selection.pretreatment.PretreatmentParameters;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class allows to get a set of classified and localised Pixels objects as a double array.
 */
public class Selection< T extends RealType< T > & NativeType< T > >
{
    private final static Logger LOGGER = LoggerFactory.getLogger( Selection.class );

    private final double radius;
    private final ClassificationParameters classificationParameters;
    private final PostTreatmentParameters postTreatmentParameters;
    private final RandomAccessibleInterval< T > input;
    private Pixels[][] output;
    private static long amplitudePT;
    private static long otsuPT;
    private static long islandSearchPT;


    public Selection( PretreatmentParameters pretreatmentParameters, ClassificationParameters classificationParameters, PostTreatmentParameters postTreatmentParameters,
                      RandomAccessibleInterval< T > input )
    {
        this.radius = pretreatmentParameters.getParameter();
        this.classificationParameters = classificationParameters;
        this.postTreatmentParameters = postTreatmentParameters;
        this.input = input;
    }

    public static < T extends RealType< T > & NativeType< T > > Pixels[][]
    run( RandomAccessibleInterval< T > input, PretreatmentParameters pretreatmentParameters, ClassificationParameters classificationParameters, PostTreatmentParameters postTreatmentParameters ) throws NoClassificationException, DataValidationException
    {
        LOGGER.debug( "Starting process..." );
        Selection< T > selection = new Selection<>( pretreatmentParameters, classificationParameters, postTreatmentParameters, input );
        selection.run();
        LOGGER.debug( "Process complete." );
        return selection.output;
    }


    public static Logger getLOGGER()
    {
        return LOGGER;
    }

    public static long getAmplitudePT()
    {
        return amplitudePT;
    }

    public static long getOtsuPT()
    {
        return otsuPT;
    }

    public static long getIslandSearchPT()
    {
        return islandSearchPT;
    }

    /**
     * @throws NoClassificationException if the no classification was performed
     * @throws DataValidationException   if one of the input parameters is not valid
     */
    public void run() throws NoClassificationException, DataValidationException
    {
        /* Pretreatment of the image.*/
        Img< FloatType > pretreatedImage = Pretreatment.run( input, radius );
        /* Classification. */
        Img< BitType > classifiedPixel = Classification.run( pretreatedImage, pretreatedImage.factory(), classificationParameters );
        amplitudePT = AmplitudeClassification.getProcessingTime();
        otsuPT = OtsuClassification.getProcessingTime();
        /* PostTreatment and output*/
        output = PostTreatment.run( classifiedPixel, postTreatmentParameters );
        islandSearchPT = IslandSearch.getProcessingTime();
    }

    public Pixels[][] getOutput()
    {
        return output;
    }
}
