/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.classification;

import fr.pasteur.ida.zellige.ReferenceSurfaceExtraction;
import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;
import fr.pasteur.ida.zellige.steps.selection.util.ExtremaDetection;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.binary.Thresholder;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class makes it possible to get a binary classification of the pixels (background/ foreground) according to their amplitude and their amplitude's neighborhood.
 * First, the amplitude of all local maximums is computed and stored as an {@link Img}.This class generate a binary image according to a percentage of the smallest mode value  of the input image
 *
 * @param <T> the type of the input {@link Img}
 */
public class AmplitudeClassification< T extends RealType< T > & NativeType< T > >
{

    private final RandomAccessibleInterval< T > input;
    private final ImgFactory< T > factory;
    private double userThreshold;
    private Img< BitType > output;
    private static long processingTime;

    private final static Logger LOGGER = LoggerFactory.getLogger( AmplitudeClassification.class );


    public AmplitudeClassification( RandomAccessibleInterval< T > input,
                                    ImgFactory< T > factory )
    {
        this.input = input;
        this.factory = factory;
        LOGGER.debug( "Derivative method used : {}", ReferenceSurfaceExtraction.getDerivativeMethod() );
    }


    /**
     * @param <T>                - the type of the input
     * @param input              - the input image as a {@link RandomAccessibleInterval}
     * @param factory            the input image {@link ImgFactory}
     * @param amplitudeThreshold - the parameter user value to apply to the first mode of the image histogram.
     * @return a binary image of background foreground classification based on contrast
     * @throws DataValidationException if the value of parameter amplitudeThreshold is not valid.
     */
    public static < T extends RealType< T > & NativeType< T > > Img< BitType > run( final RandomAccessibleInterval< T > input,
                                                                                    final ImgFactory< T > factory, double amplitudeThreshold ) throws DataValidationException
    {
        long start = System.currentTimeMillis();
        Img< BitType > amplitude = new ArrayImgFactory<>( new BitType() ).create( input );
        AmplitudeClassification< T > classification =
                new AmplitudeClassification<>( input, factory, amplitude, amplitudeThreshold );
        classification.run();
        long stop = System.currentTimeMillis();
        classification.setProcessingTime( stop - start );
        return classification.getOutput();
    }

    public static < T extends RealType< T > & NativeType< T > > Img< BitType >
    applyThreshold( Img< T > input, int userThreshold )
    {
        T value = input.firstElement().createVariable();
        value.setReal( ( float ) userThreshold );
        return Thresholder.threshold( input, value, true, 5 );
    }

    public AmplitudeClassification( RandomAccessibleInterval< T > input,
                                    ImgFactory< T > factory, Img< BitType > amplitude,
                                    double userThreshold )
    {
        this.input = input;
        this.factory = factory;
        this.output = amplitude;
        this.userThreshold = userThreshold;
        LOGGER.debug( "Derivative method used : {}", ReferenceSurfaceExtraction.getDerivativeMethod() );
    }

    /**
     * @param <T>     - the type of the input
     * @param input   - the input image as a {@link RandomAccessibleInterval}
     * @param factory the input image {@link ImgFactory}
     * @return a binary image of background foreground classification based on contrast
     * @throws DataValidationException if the value of parameter amplitudeThreshold is not valid.
     */
    public static < T extends RealType< T > & NativeType< T > > Img< T > computeAmplitudeImage( final RandomAccessibleInterval< T > input,
                                                                                                final ImgFactory< T > factory ) throws DataValidationException
    {
        AmplitudeClassification< T > classification =
                new AmplitudeClassification<>( input, factory );
        return classification.getAmplitudeImg();
    }
    /**
     * @param max the image containing only the values of local maximums
     * @param min the image containing only the values of local minimums
     * @return an image containing the value of the maximums amplitude
     */
    private Img< T > getAmplitude(
            RandomAccessibleInterval< T > max, ImgFactory< T > factory, RandomAccessibleInterval< T > min )
    {
        Img< T > amp = factory.create( max );
        RandomAccess< T > maxAccess = max.randomAccess();
        RandomAccess< T > minAccess = min.randomAccess();
        RandomAccess< T > ampAccess = amp.randomAccess();
        for ( int x = 0; x <= max.dimension( 0 ) - 1; x++ )
        {
            maxAccess.setPosition( x, 0 );
            for ( int y = 0; y <= max.dimension( 1 ) - 1; y++ )
            {
                maxAccess.setPosition( y, 1 );
                for ( int z = 0; z <= max.dimension( 2 ) - 1; z++ )
                {
                    maxAccess.setPosition( z, 2 );
                    double maxValue = maxAccess.get().getRealDouble();
                    if ( maxValue != 0 )
                    {
                        minAccess.setPosition( maxAccess );
                        double amplitude = getAmplitude( maxValue, minAccess, ( int ) max.dimension( 2 ) );
                        ampAccess.setPosition( maxAccess );
                        ampAccess.get().setReal( amplitude );
                    }
                }
            }
        }
//        ImageJFunctions.show( amp, "amplitude" );
        return amp;
    }

    /**
     * @param maxValue  the intensity value at local maximum positioned at minAccess location
     * @param minAccess - the random access positioned at  the same specific local maximum location
     * @return the chosen amplitude value of the local maximum positioned at minAccess location
     */
    private double getAmplitude( double maxValue, RandomAccess< T > minAccess, int depth )
    {
        double up = findValueUp( minAccess, maxValue );
        double down = findValueDown( minAccess, maxValue, depth );
        double result = Math.max( Math.abs( maxValue - up ), Math.abs( maxValue - down ) );
        if ( result == 0 )
        {
            return maxValue;

        }
        return ( result );
    }

    /**
     * @param minAccess the random access of the local minimum image
     * @param maxValue  the intensity value of the local maximum located at minAccess position
     * @return the amplitude value above the local maximum location in the Z dimension
     */
    private double findValueUp(
            RandomAccess< T > minAccess, double maxValue )
    {
        int start = minAccess.getIntPosition( 2 );
        for ( int z = start - 1; z >= 0; z-- )
        {
            minAccess.setPosition( z, 2 );
            double value = minAccess.get().getRealDouble();
            if ( value != 0 )
            {
                return value;
            }
        }
        return maxValue;
    }

    /**
     * @param minAccess the random access of the local minimum image
     * @param maxValue  the intensity value of the local maximum located at minAccess position
     * @return the amplitude value below the local maximum location in the Z dimension
     */
    private double findValueDown(
            RandomAccess< T > minAccess, double maxValue, int depth )
    {
        int start = minAccess.getIntPosition( 2 );
        for ( int z = start + 1; z <= depth - 1; z++ )
        {
            minAccess.setPosition( z, 2 );
            double value = minAccess.get().getRealDouble();
            if ( value != 0 )
            {
                return value;
            }
        }
        return maxValue;
    }

    //
    public void run() throws DataValidationException
    {
        Img< T > maximums = ExtremaDetection.findMaxima( input, factory );
        Img< T > minimums = ExtremaDetection.findMinima( input, factory );
        Img< T > amp = getAmplitude( maximums, factory, minimums );
        T threshold = maximums.firstElement().createVariable();
        threshold.setReal( userThreshold );
        LOGGER.debug( "Amplitude Threshold applied  = {}", threshold );
        output = Thresholder.threshold( amp, threshold, true, 5 );
    }


    public Img< BitType > getOutput()
    {
        return output;
    }

    public Img< T > getAmplitudeImg() throws DataValidationException
    {
        Img< T > maximums = ExtremaDetection.findMaxima( input, factory );
        Img< T > minimums = ExtremaDetection.findMinima( input, factory );
        return getAmplitude( maximums, factory, minimums );
    }

    public static long getProcessingTime()
    {
        return processingTime;
    }

    public void setProcessingTime( long time )
    {
        processingTime = time;
    }
}

