/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.construction.rounds;

import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.element.Surface;
import fr.pasteur.ida.zellige.element.ose.OSEListArray;
import fr.pasteur.ida.zellige.steps.construction.exception.NoSurfaceFoundException;
import fr.pasteur.ida.zellige.steps.construction.rounds.ose.OseConstructionXZ;
import fr.pasteur.ida.zellige.steps.construction.rounds.ose.OseConstructionYZ;
import fr.pasteur.ida.zellige.steps.construction.rounds.surface.SurfacesConstruction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public abstract class ConstructionRound
{
    private final static Logger LOGGER = LoggerFactory.getLogger( ConstructionRound.class );
    private final double startingSizeThreshold;
    private final int overlap;
    private final double connexityRate;
    private final double surfaceMinSizeFactor;
    private final ArrayList< Surface > surfaces;
    private long OSConstructionProcessingTime;
    private long ConstructionProcessingTime;
    private int startingOSCount;
    private int OSCount;
    private int smallSurfacesCount;
    private int goodSurfacesCount;

    protected ConstructionRound( ConstructionParameters constructionParameters )
    {
        this.startingSizeThreshold = constructionParameters.getStartingSizeThreshold();
        this.overlap = constructionParameters.getOverlap();
        this.connexityRate = constructionParameters.getConnexityRate();
        this.surfaces = new ArrayList<>();
        this.surfaceMinSizeFactor = constructionParameters.getSurfaceMinSizeFactor();
    }

    protected ConstructionRound( final double startingSizeThreshold,
                                 final int overlap,
                                 final double connexityRate,
                                 final double surfaceMinSizeFactor )
    {
        this.startingSizeThreshold = startingSizeThreshold;
        this.overlap = overlap;
        this.connexityRate = connexityRate;
        this.surfaces = new ArrayList<>();
        this.surfaceMinSizeFactor = surfaceMinSizeFactor;
    }


    void finalizeSurface( ArrayList< Surface > surfaces ) throws NoSurfaceFoundException
    {
        goodSurfacesCount = surfaces.size();
        if ( ! surfaces.isEmpty() )
        {
            mergeSurface( surfaces );
        }
        else
        {
            LOGGER.debug( this.getClass().toGenericString() + " : No Surface Found in {}", this.getClass() );
            throw new NoSurfaceFoundException();
        }
    }

    /**
     * Merges the TempSurface objects of the specified list.
     *
     * @param surfaces - the list to merge
     */
    void mergeSurface( ArrayList< Surface > surfaces )
    {
        LOGGER.debug( this.getClass().toGenericString() + " : Merging step..." );
        // Merging step.
        ArrayList< Surface > toRemoved = new ArrayList<>();
        for ( int i = surfaces.size() - 1; i > 0; i-- )
        {
            Surface one = surfaces.get( i );
            int j = i - 1;
            {
                Surface two = surfaces.get( j );
                if ( one.sameSurface( two ) )
                {
                    LOGGER.debug( " Two surfaces have been merged" );
                    two.merge( one );
                    toRemoved.add( one );
                }
                else
                {
                    LOGGER.debug( "Not the same : " + two.overlappingRate( one ) * 10 / 10.00 );
                }
            }
        }
        for ( Surface s : toRemoved )
        {
            surfaces.remove( s );
        }
        LOGGER.debug( "Merging completed" );
    }

    protected OSEListArray constructOSE( Pixels[][] maximums )
    {
        if ( this instanceof FirstRoundConstruction )
        {
            OseConstructionXZ oseConstruction = new OseConstructionXZ( maximums, startingSizeThreshold );
            oseConstruction.processAllSection();
            this.OSCount = oseConstruction.getOSCount();
            this.startingOSCount = oseConstruction.getStartingStatus().getStartingOSNb();
            return oseConstruction.getOseListArray();
        }
        else
        {
            OseConstructionYZ oseConstruction = new OseConstructionYZ( maximums, startingSizeThreshold );
            oseConstruction.processAllSection();
            this.OSCount += oseConstruction.getOSCount();
            this.startingOSCount += oseConstruction.getStartingStatus().getStartingOSNb();
            return oseConstruction.getOseListArray();
        }
    }

    protected ArrayList< Surface > constructSurfaces( OSEListArray oseLists, int lineLength )
    {
        SurfacesConstruction construction =
                new SurfacesConstruction( oseLists, lineLength, overlap, connexityRate, surfaceMinSizeFactor );
        construction.buildAllSurfaces();
        int tempSmallSurfaceCount = construction.getSmallSurfaceCount();
        smallSurfacesCount += tempSmallSurfaceCount;
        return construction.getConstructedSurfaces();
    }

    public long getOSConstructionProcessingTime()
    {
        return OSConstructionProcessingTime;
    }

    public void setOSConstructionProcessingTime( long OSConstructionProcessingTime )
    {
        this.OSConstructionProcessingTime = OSConstructionProcessingTime;
    }

    public long getConstructionProcessingTime()
    {
        return ConstructionProcessingTime;
    }

    public void setConstructionProcessingTime( long constructionProcessingTime )
    {
        ConstructionProcessingTime = constructionProcessingTime;
    }

    public int getSmallSurfacesCount()
    {
        return smallSurfacesCount;
    }

    public ArrayList< Surface > getSurfaces()
    {
        return surfaces;
    }


    public int getGoodSurfacesCount()
    {
        return goodSurfacesCount;
    }

    public int getOSCount()
    {
        return OSCount;
    }

    public int getStartingOSCount()
    {
        return startingOSCount;
    }

}
