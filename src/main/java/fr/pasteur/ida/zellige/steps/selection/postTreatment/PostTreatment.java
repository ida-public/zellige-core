/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.postTreatment;

import fr.pasteur.ida.zellige.element.Coordinate;
import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.steps.Utils;
import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;
import fr.pasteur.ida.zellige.steps.selection.postTreatment.islandSearch.IslandSearch;
import fr.pasteur.ida.zellige.steps.selection.util.ExtremaDetection;
import net.imglib2.Cursor;
import net.imglib2.IterableInterval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PostTreatment
{
    private final static Logger LOGGER = LoggerFactory.getLogger( PostTreatment.class );

    private Pixels[][] output;
    private final Img< BitType > input;


    public PostTreatment( Img< BitType > input )
    {
        this.input = input;
    }

    public static Pixels[][] run( Img< BitType > input, PostTreatmentParameters parameters ) throws DataValidationException
    {
        PostTreatment postTreatment = new PostTreatment( input );
        postTreatment.process( parameters );
        return postTreatment.getOutput();
    }

    public static Pixels[][] run( Img< BitType > input, double sigmaXY, double sigmaZ, int islandSize ) throws DataValidationException
    {
        PostTreatment postTreatment = new PostTreatment( input );
        postTreatment.process( sigmaXY, sigmaZ, islandSize );
        return postTreatment.getOutput();
    }

    /**
     * @param image - the source image as a float type {@link IterableInterval}
     * @return the source image converted into float type Img object
     */
    public static Img< FloatType > convertBitTypeIntoFloatType( IterableInterval< BitType > image )
    {
        final ImgFactory< FloatType > imgFactory = new ArrayImgFactory<>( new FloatType() );
        final Img< FloatType > copy = imgFactory.create( image );

        Cursor< BitType > bitTypeCursor = image.cursor();
        Cursor< FloatType > floatTypeCursor = copy.cursor();
        BitType bitType = bitTypeCursor.next();
        FloatType floatType = floatTypeCursor.next();
        floatType.set( ( int ) bitType.getRealDouble() );
        while ( bitTypeCursor.hasNext() )
        {

            bitTypeCursor.fwd();
            floatTypeCursor.fwd();
            if ( bitType.get() )
            {
                floatTypeCursor.get().setOne();
            }
        }
        return copy;
    }

    public static Img< BitType > runIslandSearch( Img< BitType > input, int islandSize )
    {
        if ( islandSize > 0 )
        {
            int connexity = 4;
            return IslandSearch.run( input, islandSize, connexity );
        }
        return input;
    }

    static void runDilatation( Img< FloatType > input, double sigmaXY, double sigmaZ )
    {
        LOGGER.debug( "Running dilatation with sigma XY = {} and sigma Z = {}", sigmaXY, sigmaZ );
        Utils.gaussianConvolution( input.copy(), input, new double[]{ sigmaXY, sigmaXY, sigmaZ } );
    }

    void runDilatation( Img< FloatType > input, PostTreatmentParameters parameters )
    {
        double sigmaXY = parameters.getSigmaXY();
        double sigmaZ = parameters.getSigmaZ();
        LOGGER.debug( "Running dilatation with sigma XY = {} and sigma Z = {}", sigmaXY, sigmaZ );
        Utils.gaussianConvolution( input.copy(), input, new double[]{ sigmaXY, sigmaXY, sigmaZ } );
    }

    /**
     * @param stack an image as a {@link RandomAccessibleInterval}
     * @param <T>   the image type
     * @return the image as a 2D {@link Pixels} array.
     */
    public static < T extends RealType< T > & NativeType< T > > Pixels[][] buildPixelArray(
            final RandomAccessibleInterval< T > stack )
    {
        RandomAccess< T > access = stack.randomAccess();
        Pixels[][] pixels = new Pixels[ ( int ) stack.dimension( 1 ) ][ ( int ) stack.dimension( 0 ) ];
        for ( int x = 0; x <= stack.dimension( 0 ) - 1; x++ )
        {
            for ( int y = 0; y <= stack.dimension( 1 ) - 1; y++ )
            {
                for ( int z = 0; z <= stack.dimension( 2 ) - 1; z++ )
                {
                    double value = Utils.setPositionAndGet( access, x, y, z ).getRealDouble();
                    if ( value > 0 )
                    {
                        if ( pixels[ y ][ x ] == null )
                        { // nothing yet
                            pixels[ y ][ x ] = new Pixels( new Coordinate( x, y, z ) );
                        }
                        else // already at least two coordinates
                        {
                            pixels[ y ][ x ].add( new Coordinate( x, y, z ) );
                        }
                    }
                }
            }
        }
        return pixels;
    }

    public static Pixels[][] runSmoothing( Img< BitType > ISImage, int sigmaXY, int sigmaZ ) throws DataValidationException
    {
        Img< FloatType > converted = convertBitTypeIntoFloatType( ISImage );
        // Use of Converter.convert() not possible because classifiedPixel type do not extend RealType

        /* Dilatation of the resulting image*/
        runDilatation( converted, sigmaXY, sigmaZ );

        /* Final local maximum detection */
        converted = ExtremaDetection.findMaxima( converted.copy(), converted.factory() );
        return buildPixelArray( converted );
    }

    public void process( PostTreatmentParameters parameters ) throws DataValidationException
    {
        LOGGER.debug( "Starting post treatment." );
        /* Isolated Pixel removal */
        int islandSize = parameters.getIslandSize();
        Img< BitType > temp = input;
        if ( islandSize != 0 )
        {
            temp = runIslandSearch( input, islandSize );
        }
        /* Conversion into FloatType before dilatation */
        Img< FloatType > converted = convertBitTypeIntoFloatType( temp );
        // Use of Converter.convert() not possible because classifiedPixel type do not extend RealType

        /* Dilatation of the resulting image*/
        runDilatation( converted, parameters );

        /* Final local maximum detection */
        converted = ExtremaDetection.findMaxima( converted.copy(), converted.factory() );
        output = buildPixelArray( converted );
        LOGGER.debug( "Post treatment complete" );
    }

    public void process( double sigmaXY, double sigmaZ, int islandSize ) throws DataValidationException
    {
        LOGGER.debug( "Starting post treatment." );
        /* Isolated Pixel removal */
        Img< BitType > temp = input;
        if ( islandSize != 0 )
        {
            temp = runIslandSearch( input, islandSize );
        }
        /* Conversion into FloatType before dilatation */
        Img< FloatType > converted = convertBitTypeIntoFloatType( temp );
        // Use of Converter.convert() not possible because classifiedPixel type do not extend RealType

        /* Dilatation of the resulting image*/
        runDilatation( converted, sigmaXY, sigmaZ );

        /* Final local maximum detection */
        converted = ExtremaDetection.findMaxima( converted.copy(), converted.factory() );
        output = buildPixelArray( converted );
        LOGGER.debug( "Post treatment complete" );
    }

    public Pixels[][] getOutput()
    {
        return output;
    }
}
