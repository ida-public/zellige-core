/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.construction.rounds.ose;

import fr.pasteur.ida.zellige.element.Coordinate;
import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.element.ose.AbstractOSE;
import fr.pasteur.ida.zellige.element.ose.OSEList;
import fr.pasteur.ida.zellige.element.ose.OSEListArray;
import fr.pasteur.ida.zellige.element.ose.OSEStartingStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public abstract class OseConstruction
{

    private final Pixels[][] maximums;
    private final OSEListArray oseListArray;
    private final OSEStartingStatus startingStatus;
    private final static Logger LOGGER = LoggerFactory.getLogger( OseConstruction.class );


    public OseConstruction( Pixels[][] maximums , double threshold)
    {
        this.maximums = maximums;
        this.startingStatus = new OSEStartingStatus( threshold);
        this.oseListArray = new OSEListArray( maximums.length );
    }

    public void processAllSection()
    {
        LOGGER.info( "Starting Ose construction..." );
        for ( int i = 0; i <= maximums.length - 1; i++ )
        {
            oseListArray.set(i, findOSE( maximums[ i ] ));
        }
        startingStatus.setStartingStatus();
        LOGGER.debug( "Starting size = " + startingStatus.getStartingSize() );
        LOGGER.debug( "Ose construction complete. All sections processed." );
    }

    /**
     * This method finds all the possible 2D surfaces that can be created from the coordinates detected as
     * local maximums on an array of {@link }.
     *
     * @param rawCoordinates - the raw Coordinates from the projection.
     * @return all the orthogonal surfaces for the projection as an {@link ArrayList <AbstractOSE>}.
     */
    private OSEList findOSE( Pixels[] rawCoordinates )
    {
        ArrayList< Coordinate > startingCoordinates = checkForSideCoordinates( rawCoordinates );
        OSEList paths = findSimplePaths( startingCoordinates, rawCoordinates );
        OSEList finalPaths = findComplexPaths( paths );
        finalPaths.removeIf( ose -> ose.size() < 10 );
        for ( AbstractOSE ose : finalPaths )
        {
            ose.set();
        }
        finalPaths.reset();
        return finalPaths;
    }

    public abstract OSEList findSimplePaths( ArrayList< Coordinate > startingCoordinates, Pixels[] rawCoordinates );

    /**
     * @param rawCoordinates -  the raw Coordinates from the projection.
     * @param smallPath      - the one-Z-AbstractOSE
     * @param ose             - the first AbstractOSE considered.
     * @param current        - the starting {@link Coordinate} considered.
     */
      void findSimplePaths
    ( Pixels[] rawCoordinates,
      ArrayList< AbstractOSE > smallPath, AbstractOSE ose, Coordinate current, Queue< Coordinate > queue )
    {
        int i = ose.startingIndex( current );
        while ( i <= rawCoordinates.length - 2 // x must be < to the length of the image minus 1 because [X + 1]
                && rawCoordinates[ i ] != null // the contents in the array at index x must not be null
                && rawCoordinates[ i + 1 ] != null // the contents in the array at index (x +1) must not be null
                && current != null )
        {
            Coordinate next = current.getNext( rawCoordinates[ i + 1 ], 0 );
            if ( next != null )//  Conditions :  |x1 - x2 |= 1 => only 1D surface
            {
                ose.add( next );
            }
            else
            {
                if ( ( i ) < rawCoordinates.length - 3
                        && rawCoordinates[ i + 2 ] != null// the contents in the array at index (x +1) must not
                        // be null
                        && current.getRightNumber() == 0 )
                {
                    next = current.getNext( rawCoordinates[ i + 2 ], 0 );
                    if ( next != null )//fake coordinate
                    {
                            ose.createCoordinate(current);
                        
                        ose.add( next );
                        queue.remove( next );
                        i++;
                    }
                }
            }
            current = next;
            i++; //increment of the index
        }
        smallPath.add( ose );
    }

    /**
     * @param shortPaths - the list of one-z-AbstractOSE.
     * @return - the list of "big paths" AbstractOSE.
     */
    private OSEList findComplexPaths( OSEList shortPaths )
    {
        OSEList paths = new OSEList();
        Queue< AbstractOSE > queue = new LinkedList<>( shortPaths );
        while ( ! queue.isEmpty() )
        {
            AbstractOSE first = queue.remove();
            shortPaths.remove( first );
            findComplexPaths( first, shortPaths, queue, paths );
        }
        return paths;
    }

    /**
     * @param first      - the starting AbstractOSE.
     * @param smallPaths - the list of all small path AbstractOSE.
     * @param queue      - the remaining AbstractOSE.
     * @param longPaths  - the list of long path AbstractOSE.
     */
    private  void findComplexPaths( AbstractOSE first,
                                    ArrayList< AbstractOSE > smallPaths,
                                    Queue< AbstractOSE > queue, ArrayList< AbstractOSE > longPaths )
    {
        for ( AbstractOSE o : smallPaths )
        {
            if ( first.isNextTo( o ))
            {
                first.addAll( o );
                o.setVisited( true );
                queue.remove( o );
            }
        }
        smallPaths.removeIf( AbstractOSE::isVisited );
        longPaths.add( first );
    }

    /**
     * Stores the coordinates that don't have a coordinate to the left.
     *
     * @param slice - a one-dimensional array containing the local maximums.
     * @return - a list of coordinates that don't have a coordinate to the left.
     */
    ArrayList< Coordinate > checkForSideCoordinates( Pixels[] slice )
    {
        /* Stores the coordinates that have no coordinates to the left.*/
        ArrayList< Coordinate > noLeftCoordinates = new ArrayList<>();
        /* Starting point. */
        int start = 0;
        while ( start <= slice.length - 3 && slice[ start ] == null )
        {
            start++;//the index starts where a List of Coordinates is found in the array
        }
        if ( slice[ start ] != null && slice[ start + 1 ] != null ) //some Coordinates were found
        {
            /* For the first list of coordinates. */
            for ( int i = 0; i <= slice[ start ].size() - 1; i++ )// the first coordinates of the array have obviously no left coordinates
            {
                Coordinate coordinate = slice[ start ].get( i );
                noLeftCoordinates.add( coordinate );
                coordinate.setRightCoordinates( slice[ start + 1 ] );
            }
        }
        /* For the rest. */
        for ( int i = start + 1; i <= slice.length - 2; i++ )
        {
            if ( slice[ i ] != null )
            {
                for ( Coordinate coordinate : slice[ i ].get() )
                {
                    coordinate.setRightCoordinates( slice[ i + 1 ] );
                    coordinate.setLeftCoordinates( slice[ i - 1 ] );
                    boolean hasNoLeft = coordinate.numberOfNeighbours( slice[ i - 1 ], 0 ) == 0;
                    if ( hasNoLeft )
                    {
                        noLeftCoordinates.add( coordinate );
                    }
                }
            }
        }
        /* the last one */ //Avoid ArrayOutOfBoundException
        int end = slice.length - 1;
        if ( slice[ end ] != null && slice[ end - 1 ] != null )
        {
            for ( Coordinate coordinate : slice[ end ].get() )
            {
                coordinate.setLeftCoordinates( slice[ end - 1 ] );
                boolean hasNoLeft = coordinate.numberOfNeighbours( slice[ end - 1 ], 0 ) == 0;
                if ( hasNoLeft )
                {
                    noLeftCoordinates.add( coordinate );
                }
            }
        }
        return noLeftCoordinates;
    }

    public Pixels[][] getMaximums()
    {
        return maximums;
    }

    public OSEListArray getOseListArray()
    {
        return oseListArray;
    }

    public OSEStartingStatus getStartingStatus()
    {
        return startingStatus;
    }

    public int getOSCount()
    {
        return startingStatus.getOSCount();
    }
}
