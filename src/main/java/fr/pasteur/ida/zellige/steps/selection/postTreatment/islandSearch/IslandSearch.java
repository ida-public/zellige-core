/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.postTreatment.islandSearch;

import fr.pasteur.ida.zellige.steps.Utils;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.logic.BitType;
import net.imglib2.util.ImgUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IslandSearch
{
    private final static Logger LOGGER = LoggerFactory.getLogger( IslandSearch.class );

    private static int inputCount;
    private static int pixelRemovedCount;
    private final RandomAccessibleInterval< BitType > classifiedImage;
    private final Img< BitType > output;
    private final int islandSize;
    private final int type;
    private static long processingTime;

    /**
     * @param classifiedImage  a binary image as a {@link RandomAccessibleInterval} containing small size islands
     *                         of connected pixels
     * @param output           - a binary image without small size islands of connected pixels
     * @param islandSize       - the island size limit
     * @param connectivityType the type of pixel connectivity (4-connected or 8-connected)
     */
    private IslandSearch( RandomAccessibleInterval< BitType > classifiedImage, Img< BitType > output, int islandSize, int connectivityType )
    {
        LOGGER.debug( "Starting process..." );
        this.classifiedImage = classifiedImage;
        this.output = output;
        this.islandSize = islandSize;
        this.type = connectivityType;
        process();
        LOGGER.debug( "Process completed." );
    }

    /**
     * @param classifiedImage  the binary image to filter
     * @param islandSize       the island limit size
     * @param connectivityType the type of pixel connectivity (4-connected or 8-connected)
     * @return the small island free image
     */
    public static Img< BitType > run( RandomAccessibleInterval< BitType > classifiedImage, int islandSize, int connectivityType )
    {
        long start = System.currentTimeMillis();
        ImgFactory< BitType > factory = new ArrayImgFactory<>( new BitType() );
        Img< BitType > output = factory.create( classifiedImage );
        ImgUtil.copy( classifiedImage, output );
        new IslandSearch( classifiedImage, output, islandSize, connectivityType );
        long stop = System.currentTimeMillis();
        IslandSearch.setProcessingTime( stop - start );
        return output;
    }


    /**
     * Computes the island search
     */
    private void process()
    {
        World world = convertImgIntoWorld();
        world.run();
        convertWorldIntoImg( world, output );
        LOGGER.debug( "Input count : {}.", inputCount );
        LOGGER.debug( "Number of pixels removed : {}.", pixelRemovedCount );
    }

    /**
     * @return the World object representation of the input image.
     */
    private World convertImgIntoWorld()
    {
        inputCount = 0;
        int width = ( int ) classifiedImage.dimension( 0 );
        int height = ( int ) classifiedImage.dimension( 1 );
        int depth = ( int ) classifiedImage.dimension( 2 );
        RandomAccess< BitType > access = classifiedImage.randomAccess();
        World world = new World( width, height, depth, islandSize, type );
        for ( int z = 0; z < depth; z++ )
        {
            for ( int x = 0; x < width; x++ )
            {
                for ( int y = 0; y < height; y++ )
                {
                    final boolean value = Utils.setPositionAndGet( access, x, y, z ).get();
                    if ( value )
                    {
                        inputCount++;
                        world.setSand( x, y, z );
                    }
                }
            }
        }
        LOGGER.debug( "Input = {} pixels.", inputCount );
        return world;
    }

    /**
     * @param world - the processed world
     * @param image - the output image
     */
    private void convertWorldIntoImg( World world, Img< BitType > image )
    {
        pixelRemovedCount = 0;
        RandomAccess< BitType > access = image.randomAccess();
        for ( int z = 0; z < world.getDepth(); z++ )
        {
            for ( int y = 0; y < world.getHeight(); y++ )
            {
                for ( int x = 0; x < world.getWidth(); x++ )
                {
                    Sand sand = world.getSand( x, y, z );
                    if ( sand != null && sand.getIslandStatus() == 0 )
                    {
                        Utils.setPosition( access, x, y, z );
                        access.get().setZero();
                        pixelRemovedCount++;
                    }
                }
            }
        }
        LOGGER.debug( "A number of {} pixels removed.", pixelRemovedCount );
    }

    public static long getProcessingTime()
    {
        return processingTime;
    }

    public static void setProcessingTime( long processingTime )
    {
        IslandSearch.processingTime = processingTime;
    }
}
