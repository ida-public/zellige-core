/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.steps.selection.classification;


import net.imglib2.Cursor;
import net.imglib2.IterableInterval;
import net.imglib2.algorithm.stats.Max;
import net.imglib2.algorithm.stats.RealBinMapper;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;


/**
 * Static methods to determine the local and global threshold of an ImgLib2 image.
 */
public class Threshold
{


    /**
     * Returns the pixel maximum value of the specified input
     *
     * @param input - the input image
     * @param <T>   - the input type
     * @return - the maximum value of the specified input
     */
    public static < T extends RealType< T > & NativeType< T > > T findMax( final IterableInterval< T > input )
    {
        Cursor< T > iterator = input.cursor();
        T max = input.firstElement().createVariable();
        while ( iterator.hasNext() )
        {
            final T type = iterator.next();

            if ( type.compareTo( max ) > 0 )
            {
                max.set( type );
            }
        }
        return max;
    }


    public static < T extends RealType< T > & NativeType< T > > T findMin( final IterableInterval< T > input, boolean zero )
    {
        if ( zero )
        {
            return findMinBesideZero( input );
        }
        return findMin( input );
    }


    /**
     * Returns the minimal value of the specified input.
     *
     * @param input - the input image
     * @param <T>   - the input type
     * @return the minimal value of the specified input
     */
    public static < T extends RealType< T > & NativeType< T > > T findMin( final IterableInterval< T > input )
    {
        Cursor< T > cursor = input.cursor();
        T type = cursor.next();
        T min = type.createVariable();
        while ( cursor.hasNext() )
        {
            type = cursor.next().copy();
            if ( type.compareTo( min ) <= 0 )
            {
                min.set( type.copy() );
            }
        }
        return min;
    }


    public static < T extends RealType< T > & NativeType< T > > T findMinBesideZero( final IterableInterval< T > input )
    {
        Cursor< T > cursor = input.cursor();
        double min = Max.findMax( input ).get().getRealDouble();
        while ( cursor.hasNext() )
        {
            cursor.fwd();
            final double value = cursor.get().getRealDouble();
            if ( value > 0 && value < min )
            {
                min = value;
            }
        }

        T minimum = input.firstElement().createVariable();
        minimum.setReal( min );
        return minimum;
    }
// TODO implements Otsu-2D ???


    public static < T extends RealType< T > & NativeType< T > > int[] getHistogram( IterableInterval< T > image, T min, T max, int numBins )
    {
        HistogramZ< T > H = new HistogramZ<>( new RealBinMapper<>( min, max, numBins ), image );
        H.process();

        return H.getHistogram();
    }


    public static int OtsuCelebiIndex( int[] histogram )
    {
        // Otsu's threshold algorithm
        // M. Emre Celebi 6.15.2007, Fourier Library https://sourceforge.net/projects/fourier-ipal/
        // ported to ImageJ plugin by G.Landini

        int ih;
        int threshold;
        int num_pixels = 0;
        double total_mean;    /* mean gray-level for the whole image */
        double bcv, term;    /* between-class variance, scaling term */
        double max_bcv;        /* max BCV */
        double[] cnh = new double[ histogram.length ];    /* cumulative normalized histogram */
        double[] mean = new double[ histogram.length ]; /* mean gray-level */
        double[] h = new double[ histogram.length ];/* normalized histogram */

        /* Calculate total number of pixels */
        for ( ih = 0; ih < histogram.length; ih++ )
        {
            num_pixels = num_pixels + histogram[ ih ];
        }

        term = 1.0 / ( double ) num_pixels;

        /* Calculate the normalized histogram */
        for ( ih = 0; ih < histogram.length; ih++ )
        {
            h[ ih ] = term * histogram[ ih ];
        }

        /* Calculate the cumulative normalized histogram */
        cnh[ 0 ] = h[ 0 ];
        for ( ih = 1; ih < histogram.length; ih++ )
        {
            cnh[ ih ] = cnh[ ih - 1 ] + h[ ih ];
        }

        mean[ 0 ] = 0.0;

        for ( ih = 1; ih < histogram.length; ih++ )
        {
            mean[ ih ] = mean[ ih - 1 ] + ih * h[ ih ];
        }

        total_mean = mean[ histogram.length - 1 ];

        //	Calculate the BCV at each gray-level and find the threshold that maximizes it
        threshold = Integer.MIN_VALUE;
        max_bcv = 0.0;

        for ( ih = 0; ih < histogram.length; ih++ )
        {
            bcv = total_mean * cnh[ ih ] - mean[ ih ];
            bcv *= bcv / ( cnh[ ih ] * ( 1.0 - cnh[ ih ] ) );

            if ( max_bcv < bcv )
            {
                max_bcv = bcv;
                threshold = ih;
            }
        }
        return threshold;
    }


    public static < T extends RealType< T > & NativeType< T > > T getThreshold( IterableInterval< T > input )
    {
        T max = Threshold.findMax( input );
        T min = Threshold.findMin( input );
        int[] histogram = getHistogram( input, min, max, 256 );
        return getThreshold( min, max, histogram );
    }


    public static < T extends RealType< T > & NativeType< T > > T getThreshold( IterableInterval< T > input, double percent )
    {
        T max = Threshold.findMax( input );
        T min = Threshold.findMin( input );
        int[] histogram = getHistogram( input, min, max, 256 );
        T threshold = getThreshold( min, max, histogram );
        threshold.mul( percent );
        return threshold;
    }

    public static < T extends RealType< T > & NativeType< T > > T getThreshold( T min, T max, int[] histogram )
    {
        histogram[ 0 ] = 0;
        int thresholdIndex = OtsuCelebiIndex( histogram );
       return  getBinValueFromIndex( min, max, histogram, thresholdIndex );
    }


    public static < T extends RealType< T > & NativeType< T > > T getBinValueFromIndex( T min, T max, int[] histogram, int thresholdIndex )
    {
        double binWidth = ( max.getRealDouble() - min.getRealDouble() ) / ( double ) histogram.length;
        double result = ( ( double ) ( thresholdIndex + 1 ) * binWidth + min.getRealDouble() );
        T threshold = min.createVariable();
        threshold.setReal( result );
        return threshold;
    }

    public static < T extends RealType< T > & NativeType< T > > T getFirstMaxValue( Img< T > input, boolean zero )
    {
        T max = Threshold.findMax( input );
        T min = Threshold.findMin( input );
        int numBins = (int)max.getRealDouble();
        HistogramZ< T > H = new HistogramZ<>( new RealBinMapper<>( min, max, 256 ), input );
        H.process();
        int[] histogram = H.getHistogram();

        if ( zero )
        {
            histogram[ 0 ] = 0;
        }
        int index = getFirstMaxIndex( histogram );

        return H.getBinCenter( index );
    }

    public static int getFirstMaxIndex( int[] histogram )
    {
        for ( int i = 2; i <= histogram.length - 2; i++ )
        {
            if ( histogram[ i - 1 ] < histogram[ i ] && histogram[ i ] > histogram[ i + 1 ] )
            {
                return i;
            }
        }
        return - 1; // no max ?
    }
}


