/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.element.ose;

import fr.pasteur.ida.zellige.element.Coordinate;
import fr.pasteur.ida.zellige.element.Surface;
import fr.pasteur.ida.zellige.element.surfaceLine.SurfaceLineX;

public class OseXZ extends AbstractOSE
{
    public OseXZ( OSEStartingStatus startingStatus )
    {
        super( startingStatus );
    }

    @Override
    public boolean isNextTo( AbstractOSE next )
    {
        return ( next.get( 0 ).getX() - this.get( this.size() - 1 ).getX() == 1 // the two OS are next to each other
                && this.get( this.size() - 1 ).getRightNumber() == 1 // this OS has only one possible right coordinate
                && next.get( 0 ).getLeftNumber() == 1  // the next OS has only one possible left coordinate
                && this.get( this.size() - 1 ).isNext( next.get( 0 ), 1 ) );// the difference between the z values equals 1
    }

    @Override
    public void createCoordinate( Coordinate current )
    {
        this.add( new Coordinate( current.getX() + 1, current.getY(), current.getZ() ) );
    }

    @Override
    public void setFirstOSE( Surface surface )
    {
        int i = this.get(0). getY();
        int lineLength = surface.getWidth();
        surface.set( i, new SurfaceLineX( lineLength, this ) );
    }


    public int getCoordinate(int index)
    {
       return this.get(index).getX();
    }
    @Override
    public int startingIndex( Coordinate current )
    {
        return current.getX();
    }
}
