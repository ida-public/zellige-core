/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.element;

import java.util.ArrayList;
import java.util.Arrays;

public class Pixels
{
    private Coordinate[] list;

    public Pixels( Coordinate coordinate )
    {
        this.list = new Coordinate[]{ coordinate };
    }

    public boolean contains( Object o )
    {
        if ( o instanceof Coordinate )
        {
            Coordinate coordinate = ( Coordinate ) o;
            for ( Coordinate c : this.list )
            {
                if ( c == coordinate )
                {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * Adds a new Coordinate to this instance by creating a new array and adding the Coordinate at the end.
     *
     * @param coordinate - the {@link Coordinate }to add
     */
    public void add( Coordinate coordinate )
    {
        if ( !contains( coordinate ) )
        {
            Coordinate[] newList = new Coordinate[ this.size() + 1 ];
            System.arraycopy( list, 0, newList, 0, list.length );
            newList[ newList.length - 1 ] = coordinate;
            this.list = newList;
        }
    }


    /**
     * @param pixels - the {@link Pixels} (list of Coordinates to add to.)
     */
    public void addAll( Pixels pixels )
    {
        ArrayList<Coordinate> temp = new ArrayList <>( pixels.size());
        for ( int i = 0; i <= pixels.size() - 1; i++ )
        {
            Coordinate coordinate = pixels.get( i);
            if ( !this.contains( coordinate ) )
            {
                temp.add(coordinate);
            }
        }
        Coordinate[] newList = new Coordinate[ this.size() + temp.size() ];
        Coordinate[] t =  new Coordinate[temp.size()];
         t = temp.toArray(t);
        System.arraycopy( list, 0, newList, 0, list.length );
        System.arraycopy( t, 0, newList, list.length, temp.size() );
        this.list = newList;
    }

    /**
     * returns the Coordinate in the specified index.
     *
     * @param index - the position in the list.
     * @return - the Coordinate at the specified position.
     */
    public Coordinate get( int index )
    {
        return list[ index ];
    }


    /**
     * Resets the parameters of each {@link Coordinate }before the Y dimension reconstruction.
     * ( rightsNumber , leftsNumber, noLeft ).
     */
    public void resetSideCoordinate()
    {
        for ( Coordinate coordinate : this.list )
        {
            coordinate.reset();
        }
    }

    /**
     * Returns the number of Coordinate of this object.
     *
     * @return the number of Coordinate of this object.
     */
    public int size()
    {
        return this.list.length;
    }




    public Coordinate[] get()
    {
        return list;
    }


    public boolean containsAll( Pixels list )
    {
        int count = 0;
        for ( Coordinate c : list.get() )
        {
            if ( this.contains( c ) )
            {
                count++;
            }
        }
        return count == list.size();
    }

    /**
     * Indicates whether some other object is "equal to" this one
     *
     * @param o -  the reference object with which to compare.
     */
    @Override
    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        if ( o == null || getClass() != o.getClass() )
        {
            return false;
        }
        Pixels list = ( Pixels ) o;
        if ( this.size() == list.size() )
        {
            Arrays.sort( this.get() );
            Arrays.sort(list.get());
            return Arrays.equals( list.get(), this.get() );
        }
        else
        {
            return false;
        }
    }

    public String toString()
    {
        return Arrays.toString(list);
    }

}
