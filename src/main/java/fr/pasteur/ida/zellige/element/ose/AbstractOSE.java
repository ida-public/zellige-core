/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.element.ose;

import fr.pasteur.ida.zellige.element.Coordinate;
import fr.pasteur.ida.zellige.element.Surface;

import java.util.ArrayList;

public abstract class AbstractOSE extends ArrayList< Coordinate >
{

    /* Not necessary for the program.*/
    private final OSEStartingStatus startingStatus;
    /*-------------*/

    /**
     * The visited status for the 1D and 2D reconstructions.
     */
    private boolean visited;
    /**
     * True if the OS instance can be used as the beginning of a new TempSurface.
     */
    private boolean start = true;

    public AbstractOSE( OSEStartingStatus startingStatus )
    {
        this.startingStatus = startingStatus;
    }


    /**
     * Sets the start status to false if the size is inferior to the OS start minimum size.
     * Default value = 70 Pixels.
     * {@link Coordinate} of the OS
     */
    public void set()
    {
        Integer j = startingStatus.get( this.size() );
        startingStatus.put( this.size(), ( j == null ) ? 1 : j + 1 );
        startingStatus.setOSCount();

    }

    /**
     * Test if the current instance is adjacent to the next OS instance.
     *
     * @param next      - the other OS instance.
     * @return true if the two OS are next to each other, false otherwise.
     */
    public abstract boolean isNextTo( AbstractOSE next );

    public abstract void createCoordinate( Coordinate coordinate );

    public abstract void setFirstOSE( Surface surface);

    public abstract int startingIndex(Coordinate current);

    /**
     * Returns true if the OS has been visited.
     *
     * @return returns true if the OS has been visited
     */
    public boolean isVisited()
    {
        return visited;
    }

    /**
     * Sets the visited parameter to the specified value.
     *
     * @param visited - the value to set.
     */
    public void setVisited( boolean visited )
    {
        this.visited = visited;
    }

    /**
     * Sets the visited status to true (the instance belongs to a ReferenceSurface)
     * and therefore sets the start status to false.
     */
    public void setVisited()
    {
        this.visited = true;
        this.start = false;
    }

    /**
     * Indicates whether some other object is "equal to" this one
     *
     * @param o -  the reference object with which to compare.
     */
    @Override
    public boolean equals( Object o )
    {
        if ( o == null )
        {
            return false;
        }
        else
        {
            return super.equals( o );
        }
    }

    /**
     * Returns true if the object can be used to start a Surface.
     *
     * @return true if the object can be used to start a Surface
     */
    public boolean isAStart()
    {
        return ( this.size() >= this.startingStatus.getStartingSize() && start );
    }

    public abstract int getCoordinate(int index);

}
