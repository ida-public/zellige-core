/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.element;

import fr.pasteur.ida.zellige.element.surfaceLine.SurfaceLine;
import fr.pasteur.ida.zellige.element.surfaceLine.SurfaceLineX;
import fr.pasteur.ida.zellige.element.surfaceLine.SurfaceLineY;

import java.lang.reflect.InvocationTargetException;

public class Surface
{

    private final SurfaceLine[] surfaceLines;
    private final int width, height;

    public Surface( int width, int height )
    {
        this.width = width;
        this.height = height;
        this.surfaceLines = new SurfaceLine[ height ];
    }

//

    /**
     * Returns true if the instance has a {@link Pixels }which the size is superior to 1 (at least 2{@link Coordinate})
     *
     * @return True if the instance has any duplicates, false otherwise.
     */
    public boolean hasDuplicate()
    {
        for ( int i = 0; i <= this.getHeight() - 1; i++ )
        {
            SurfaceLine s = this.get( i );
            if ( s != null && s.hasDuplicate() )
            {
                return true;
            }
        }
        return false;
    }


    /**
     * Checks if one {@link Pixels}object contains all the {@link Coordinate} objects than the other one.
     *
     * @param one the first {@link Pixels} object.
     * @param two the first {@link Pixels} object.
     * @return true if one {@link Pixels} object contains all the {@link Coordinate} objects than the other one.
     */
    private boolean sameSurface( Pixels one, Pixels two )
    {
        if ( one.size() > two.size() )
        {
            return ( one.containsAll( two ) );
        }
        else if ( one.size() < two.size() )
        {
            return ( two.containsAll( one ) );
        }
        else // same size.
        {
            return one.equals( two );
        }
    }

    /**
     * Checks if two {@link SurfaceLine} share a majority of {@link Pixels}.
     *
     * @param one the first {@link SurfaceLine} object.
     * @param two the second {@link SurfaceLine} object.
     * @return True if both {@link SurfaceLine} objects are similar, false otherwise.
     */
    private boolean sameSurface( SurfaceLine one, SurfaceLine two )
    {
        int count = 0;
        int common = 0;
        for ( int i = 0; i < one.getLength() - 1; i++ )
        {
            Pixels p1 = one.get( i );
            Pixels p2 = two.get( i );
            if ( p1 != null && p2 != null ) // There is a PixelList in both SurfaceLine
            {
                common++;
                if ( sameSurface( p1, p2 ) )
                {
                    count++; // there a match
                }
            }
        }
        double r = ( double ) count / ( double ) common;
        return ( r ) > 0.70;
    }

    public double overlappingRate(  Surface other )
    {
        int inCommon = 0;
        int count = 0;
        for ( int i = 0; i <= this.getHeight() - 1; i++ )
        {
            SurfaceLine refLine = this.get( i );
            if ( refLine != null )
            {
                for ( int j = 0; j < refLine.getLength(); j++ )
                {
                    Pixels refPixels = refLine.get( j );
                    if ( refPixels != null )
                    {
                        count++;
                        SurfaceLine toTest = other.get(i);

                        if ( toTest != null && refPixels.equals( other.get( i ).get( j ) ))
                        {
                            inCommon++;
                        }
                    }
                }

            }

        }
        return inCommon / ( double ) count;
    }

    public boolean isTheSameSurfaceAs( Surface other )
    {
        for ( int i = 0; i <= this.getHeight() - 1; i++ )
        {
            SurfaceLine refLine = this.get( i );
            if ( refLine != null )
            {
                SurfaceLine toTest = other.get( i );
                if ( toTest != null )
                {

                    for ( int j = 0; j < refLine.getLength(); j++ )
                    {
                        Pixels refPixels = refLine.get( j );
                        if ( refPixels != null )
                        {
                            Pixels pixelsToTest = toTest.get( j );
                            if ( pixelsToTest != null )
                            {
//                            return false;
//                        }
                                if ( ! refPixels.equals( pixelsToTest ) )
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }

            }

        }
        return true;
    }


    /**
     * Checks if this object shares a majority of {@link Pixels} with another {@link Surface} object.
     *
     * @param other the other {@link Surface} object.
     * @return True if both {@link Surface} objects are similar, false otherwise.
     */
    public boolean sameSurface( Surface other )
    {
        int count = 0;
        int common = 0;
        for ( int i = 0; i <= this.getHeight() - 1; i++ )
        {
            if ( this.get( i ) != null && other.get( i ) != null )// There is a SurfaceLine at index i in both TempSurface
            {
                common++;
                if ( sameSurface( this.get( i ), other.get( i ) ) )
                {
                    count++;
                }
            }
        }
        return common > 90 && ( double ) count / ( double ) common > 0.90;
    }

    /**
     * Combines the current instance with another {@link Surface} instance.
     *
     * @param otherSurface - the {@link Surface} to combine with.
     */
    public void merge( Surface otherSurface )
    {
        for ( int i = 0; i <= this.getHeight() - 1; i++ )
        {
            if ( this.get( i ) != null &&
                    otherSurface.get( i ) != null &&
                    this.get( i ).getCoordinatesNumber() != otherSurface.get( i ).getCoordinatesNumber() )
            {
                this.get( i ).merge( otherSurface.get( i ) );
            }
        }
    }

    /**
     * Transpose the instance : length equals dimension height instead of width.
     *
     * @return the identical transposed {@link Surface}
     */
    public Surface transpose()
    {
        int height = this.getWidth();
        int width = this.getHeight();
        Surface surface = new Surface( width, height );
        Class< ? > theClass = getTransposedClass();
        for ( int i = 0; i <= this.getHeight() - 1; i++ )
        {
            SurfaceLine surfaceLine = this.get( i );
            if ( surfaceLine != null )
            {
                for ( int j = 0; j <= surfaceLine.getLength() - 1; j++ )
                {
                    Pixels pixels = surfaceLine.get( j );
                    if ( pixels != null )
                    {
                        SurfaceLine newSurfaceLine = surface.get( j );
                        if ( newSurfaceLine == null )
                        {
                            try
                            {
                                newSurfaceLine = ( SurfaceLine ) theClass.getDeclaredConstructors()[ 1 ].newInstance( width, j );
                            }
                            catch ( InstantiationException | IllegalAccessException | InvocationTargetException e )
                            {
                                e.printStackTrace();
                            }
                            surface.set( j, newSurfaceLine );
                        }
                        assert newSurfaceLine != null;
                        newSurfaceLine.set( i, pixels );
                    }
                }
            }
        }
        return surface;
    }

    /**
     * Returns the {@link SurfaceLine} array of the object.
     *
     * @return the {@link SurfaceLine} array of the object.
     */
    public SurfaceLine[] get()
    {
        return surfaceLines;
    }

    /**
     * Returns the {@link SurfaceLine} object at the specified index position.
     *
     * @param index - the position of the SurfaceLine
     * @return the SurfaceLine object at the specified index position
     */
    public SurfaceLine get( int index )
    {
        return this.surfaceLines[ index ];
    }

    /**
     * Sets the specified {@link SurfaceLine} at the specified index position. If the SurfaceLine at the specified index
     * not null, the both are merged.
     *
     * @param index       the position of the SurfaceLine
     * @param surfaceLine the SurfaceLine object to set
     */
    public void set( int index, SurfaceLine surfaceLine )
    {
        if ( this.surfaceLines[ index ] != null )
        {
            surfaceLines[ index ].merge( surfaceLine );
        }
        else
        {
            this.surfaceLines[ index ] = surfaceLine;
        }
    }


    /**
     * Returns the number of {@link Coordinate} in the instance {@link SurfaceLine} array.
     *
     * @return The instance {@link Coordinate} number.
     */
    public int getSize()
    {
        int size = 0;
        for ( SurfaceLine surfaceLine : surfaceLines )
        {
            if ( surfaceLine != null )
            {
                size += surfaceLine.getCoordinatesNumber();
            }
        }
        return size;
    }


    public Class< ? > getTransposedClass()
    {

        for ( SurfaceLine surfaceLine : surfaceLines )
        {
            if ( surfaceLine != null )
            {
                return surfaceLine.getClass() == SurfaceLineX.class ? SurfaceLineY.class : SurfaceLineX.class;
            }
        }
        return null;
    }


    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    @Override
    public boolean equals( Object obj )
    {
        if ( this == obj )
        {
            return true;
        }
        if ( obj == null || getClass() != obj.getClass() )
        {
            return false;
        }
        Surface other = (Surface ) obj;
        {
            return overlappingRate( other ) == 1;
//            return isTheSameSurfaceAs( other );
        }
    }
}
