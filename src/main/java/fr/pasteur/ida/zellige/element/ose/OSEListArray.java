/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.element.ose;

public class OSEListArray
{

    private final OSEList [] oseLists;
    private int index = 0;

    public OSEListArray( int length )
    {
        this.oseLists = new OSEList[length];
    }

    /**
     * Returns the index value of the first OSList containing a starting OS.
     *
     * @param index - the previous index value
     * @return the value of the first OSList containing a starting OS
     */
    private int findIndexValue( int index )
    {
        int i = index;
        int limitValue;
        limitValue = oseLists.length - 1;
        while ( i <= limitValue - 2 )
        {
            if ( oseLists[ i ] != null
                    && ! oseLists[ i ].isEmpty()
                    && oseLists[ i ].containsAStart() )
            {
                return i;
            }
            i++;
        }
        index = limitValue;
        return index;
    }

    /**
     * Returns the first OS with a start status in the OSList array.
     *
     * @return the first OS with a true Start status
     */
    public AbstractOSE getAStartingOse()
    {
       reset();
       index = findIndexValue( index );
        for ( AbstractOSE os : oseLists[ index ] )
        {
            if ( os.isAStart() )
            {
                os.setVisited();
                return os;
            }
        }
        return null;
    }

    /**
     * Resets the "visited" value of each OS contained in each OSList to FALSE.
     */
    public void reset()
    {
        for ( OSEList oseList : oseLists )
        {
            oseList.reset(); // empty list instead of null
        }
    }

    public void set(int i, OSEList oseList)
    {
        this.oseLists[i] = oseList;
    }

    public OSEList get(int index)
    {
        return oseLists[index];
    }

    public int getLength()
    {
        return oseLists.length;
    }

    public OSEList[] getOseLists()
    {
        return oseLists;
    }
}
