/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.element.ose;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TreeMap;

public class OSEStartingStatus extends TreeMap< Integer, Integer >//TODO an array is sufficient
{
    private final static Logger LOGGER = LoggerFactory.getLogger( OSEStartingStatus.class );

    private int startingSize;
    private final double userThreshold;
    private int startingOSNb;
    private int OSCount;

    public OSEStartingStatus( double userThreshold )
    {
        this.userThreshold = userThreshold;
        LOGGER.debug( "OSE starting threshold = {}", userThreshold );
    }

    public  void setStartingStatus()
    {
        LOGGER.debug( "OS count= {}", OSCount );
        int size = ( int ) ( this.size() * userThreshold );
        if (this.size() != 0)
        {
            {
                for ( int i = 0; this.size() > 20 && i <= size; i++ )
                {
                    startingOSNb += this.remove( this.lastKey() );
                }
            }
            this.startingSize = this.lastKey();
            startingOSNb += this.get( this.lastKey() );
            LOGGER.debug( "Starting OSE size = {}", startingSize );
            LOGGER.debug( " Nb of starting OS : {} ", startingOSNb );
        }
    }

    public int getStartingSize()
    {
        return startingSize;
    }

    public int getOSCount()
    {
        return OSCount;
    }

    public void setOSCount()
    {
        this.OSCount++;
    }

    public int getStartingOSNb()
    {
        return startingOSNb;
    }

}
