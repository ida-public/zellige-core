/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.element;

/**
 * A Coordinate instance has 3 stack position parameters (x, y and z) and 2 parameters for the orthogonal section
 * element reconstruction : the number of diagonal
 */
public class Coordinate  implements Comparable <Coordinate> //extends Pixel
{
    public static int number = 0; /* Total number of coordinates instances.*/

    /** The position in dimension X.*/
    private final int x;
    /** The position in dimension Y. */
    private final int y;
    /** The position in dimension Z. */
    private final int z;
    /** The number of coordinates to the right of the current instance with a different z value.*/
    private int rightNumber;
    /** The number of coordinates to the left of the current instance with a different z value.*/
    private int leftNumber;


    /**
     * Constructor
     *
     * @param x - the coordinates for the first dimension.
     * @param y - the coordinates for the second dimension.
     * @param z - the coordinates for the third dimension.
     */
    public Coordinate( int x, int y, int z )
    {
        this.x = x;
        this.y = y;
        this.z = z;
        number++;
    }

    /**
     * Sets the number of Coordinates to the right of the current Coordinate
     * instance which belong to the same ReferenceSurface.
     *
     * @param rights - the Pixel to the right.
     */
    public void setRightCoordinates( Pixels rights )
    {
        if (rights!= null)
        {
            this.rightNumber = numberOfNeighbours(rights, 1);
        }
    }

    /**
     * Sets the number of Coordinates to the left of the current Coordinate
     * instance which belong to the same ReferenceSurface.
     *
     * @param lefts - the Pixel to the left.
     */
    public void setLeftCoordinates( Pixels lefts )
    {
        if (lefts != null)
        {
            this.leftNumber = numberOfNeighbours(lefts, 1);
        }
    }


    /**
     * Computes the number of Coordinates which belong to the same surface as the current Pixel instance.
     *
     * @param pixels   - the specified Pixel.
     * @param distance - the distance constraint (0 or 1).
     * @return the number of Coordinates next to the current Pixel.
     */
    public int numberOfNeighbours( Pixels pixels, int distance )
    {
        if (pixels != null)
        {
            int count = 0;
            for (Coordinate coordinate : pixels.get()) {
                if (isNext(coordinate, distance)) {
                    count++;
                }
            }
            return count;
        }
        return 0;
    }

    /**
     * Determines the Coordinate next to the current Coordinate instance.
     *
     * @param nextPixels - the next Pixel instance.
     * @param distance   - the distance constraint.
     * @return a new Pixel containing the coordinates that respect the distance constraint,
     * null if no Coordinates are found.
     */
    public Coordinate getNext( Pixels nextPixels, int distance )
    {
        for (Coordinate coordinate : nextPixels.get() )
            {
                if ( isNext( coordinate, distance ) )
                {
                    return coordinate;
                }
            }
        return null;
    }

    /**
     * @param nextCoordinate - the next Coordinate.
     * @param distance       - the distance authorized between this Pixel and the coordinate.
     * @return true, if this Coordinate Z value has a distance equals to the distance parameter
     * from the nextCoordinate , false otherwise
     */
    public boolean isNext( Coordinate nextCoordinate, int distance )
    {
        return Math.abs( this.getZ() - nextCoordinate.getZ() ) == distance;
    }

    /**
     * Resets all the instances necessary variables for a construction.
     */
    public void reset( )
    {
        this.leftNumber = 0;
        this.rightNumber = 0;
    }

    /**
     * Indicates whether some other object is "equal to" this one
     *
     * @param o  -  the reference object with which to compare.
     */
    @Override
    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        if ( o == null || getClass() != o.getClass() )
        {
            return false;
        }
        Coordinate coordinate = ( Coordinate ) o;
        return ( this.x == coordinate.x &&
                this.y == coordinate.y &&
                this.z == coordinate.z );
    }

    /**
     * Returns the number of Coordinates to the right of this object.
     * @return the number of Coordinates to the right of this object
     */
    public int getRightNumber( )
    {
        return rightNumber;
    }

    /**
     * Returns the number of Coordinates to the left of this object.
     *
     * @return the number of Coordinates to the left of this object.
     */
    public int getLeftNumber( )
    {
        return leftNumber;
    }

    /**
     * Returns the value of x.
     * @return the value of x
     */
    public int getX( )
    {
        return x;
    }

    /**
     * Returns the value of y.
     * @return the value of y
     */
    public int getY( )
    {
        return y;
    }

    /**
     * Returns the z value of this object.
     *
     * @return the value of z
     */
    public int getZ()
    {
        return z;
    }

    public String toString()
    {
        return ("( " + getX() + ", " + getY() + ", " + z + " ) ");
    }

    @Override
    public int compareTo( Coordinate o )
    {
        if (this.x == o.x)
        {
            if (this.y == o.y)
            {
                return Integer.compare( this.z, o.z );
            }
            else if ( this.y > o.y)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
        else if (this.x > o.x)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
}
