/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.element.ose;

import java.util.ArrayList;
import java.util.Collection;

/**
 * An OSList object is simply an ArrayList of {@link AbstractOSE}
 */
public class OSEList extends ArrayList< AbstractOSE >
{


    public OSEList()
    {
    }

    @Override
    public boolean add( AbstractOSE os )
    {
        if ( this.contains( os ) )
        {
            return false;
        }
        else
        {
            return super.add( os );
        }
    }

    @Override
    public boolean addAll( Collection< ? extends AbstractOSE > c )
    {
        boolean add = false;
        for ( AbstractOSE os : c )
        {
            add = add( os );
        }
        return add;
    }

    public int getSize()
    {
        int size = 0;
        for ( AbstractOSE os : this )
        {
            size = size + os.size();
        }
        return size;
    }

    public void reset()
    {
        for ( AbstractOSE os : this )
        {
            os.setVisited( false );
        }
    }

    public boolean containsAStart()
    {
        for ( AbstractOSE os : this )
        {
            if ( os.isAStart() )
            {
                return true;
            }
        }
        return false;
    }
}
