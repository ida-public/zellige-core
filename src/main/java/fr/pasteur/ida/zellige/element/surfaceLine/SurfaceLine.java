/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.element.surfaceLine;

import fr.pasteur.ida.zellige.ReferenceSurfaceExtraction;
import fr.pasteur.ida.zellige.element.Coordinate;
import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.element.ose.AbstractOSE;


public abstract class SurfaceLine
{


    /**
     * An array containing the pixels (Coordinates or List of Coordinates).
     */
    private final Pixels[] dimension;
    /**
     * The position on the stack, index X for dimension 0, Y for dimension 1.
     */
    private int line;
    /**
     * The number of Pixels in the Pixels array.
     */
    private int size = 0;

    /**
     * Constructor.
     *
     * @param length the size of the {@link SurfaceLine}
     * @param line   - the position on the stack.
     */
    public SurfaceLine( int length, int line  )
    {      
          this.dimension = new Pixels[ length  ] ;
                this.line = line;
    }

    /**
     * Constructor.
     *
     * @param length the size of the {@link SurfaceLine}
     * @param os - a list of Coordinates to fill the pixel array.
     */
    public SurfaceLine(int length , AbstractOSE os)
    {
            this.dimension = new Pixels[ length ] ;
            set( os );
    }
    
    
    public SurfaceLine( AbstractOSE os, int size )
    {
        this.dimension = new Pixels[ size] ;
        set( os );
    }

    public SurfaceLine match2( AbstractOSE os, int overlap, double connexity, SurfaceLine surfaceLine )
    {
        int match = 0;
        int k = 0;
        for ( int i = 0; i <= os.size() - 1; i++ )
        {
            int j = os.getCoordinate( i );
            Coordinate coordinate = os.get( i );
            if ( this.get( j ) != null )
            {
                k++;
                if ( isAMatch( this.get( j ), coordinate ) )
                {
                    match++;
                }
            }
            surfaceLine.set( j, coordinate );
        }
        if ( k == 0 || match == 0 )
        {
            return null;
        }
        else if ( ( double ) match / ( double ) k >= connexity && k >= overlap )
        {
            return surfaceLine;
        }
        else
        {
            return null;
        }
    }


    public void set( AbstractOSE os )
    {
        for ( Coordinate coordinate : os )
        {
            if ( this instanceof SurfaceLineX )
            {
                int x = coordinate.getX();
                this.set( x, coordinate );
            }
            else
            {
                int y = coordinate.getY();
                this.set( y, coordinate );
            }
        }
    }

    public boolean isInAdequacyWith( SurfaceLine surfaceLine)
    {
        return ( surfaceLine == null ) || overlappingRate( surfaceLine ) < ReferenceSurfaceExtraction.getAdequacy();

    }

    /**
     * Tests if the OS matches the SurfaceLine instance and creates a new SurfaceLine object if so.
     *
     * @param os      - the OS to test against the SurfaceLine instance.
     * @param j       - an integer witch indicates the line of the resulting SurfaceLine.
     * @param overlap - the minimum number of matching coordinates.
     * @param connexity - the minimum percentage of match between the OS and the current instance.
     * @return - a new SurfaceLine if there is a match, null otherwise.
     */
    public abstract SurfaceLine match( AbstractOSE os, int j, int overlap, double connexity );


    /**
     * Gets the Pixel at a specified index.
     *
     * @param i -  the index position.
     * @return the Pixel at the specified index.
     */
    public Pixels get( int i )
    {
        return this.dimension[ i ];
    }


    /**
     * Sets a Pixels in the SurfaceLine instance at the specified index.
     *
     * @param i     - the index value.
     * @param pixels - the Pixels to add.
     */
    public void set( int i, Pixels pixels )
    {
        if ( this.get( i ) == null )
        {
            this.size = this.size + pixels.size();
        }
        else
        {
            int size = this.get( i ).size();
            this.size = this.size - size + pixels.size();
        }
        this.dimension[ i ] = pixels;
    }

    public void set(int i, Coordinate coordinate)
    {
            if (dimension [i] == null)
            {
                dimension[i] = new Pixels( coordinate );
            }
            else
            {
               dimension[i].add( coordinate );
            }
    }

    /**
     * Combines the current SurfaceLine instance to another SurfaceLine instance.
     *
     * @param other - the other SurfaceLine.
     */
    public void merge( SurfaceLine other )
    {
        double o = overlappingRate( other );
//        LOGGER.debug("overlap = " + o);
//        if (o < 0.30)
        for ( int i = 0; i <= this.dimension.length - 1; i++ )
        {
            // only two situations where we have to do something
            if ( this.get( i ) != null && other.get( i ) != null )
            {
                this.set( i, this.merge( this.get( i ), other.get( i ) ) );
            }
            else if ( this.get( i ) == null && other.get( i ) != null )
            {
                this.set( i, other.get( i ) );
            }
        }
    }

    public double overlappingRate( SurfaceLine fixedLine )
    {
        int overlappedCoordinates = 0;
        int pixelCount = 0;
        for ( int i = 0; i <= this.dimension.length - 1; i++ )
        {
            // only two situations where we have to do something
            if ( this.get( i ) != null )
            {
                pixelCount++;
                if ( fixedLine.get( i ) != null )

                {
                    overlappedCoordinates++;
                }
            }
        }
        return overlappedCoordinates / ( double ) pixelCount;
    }

    /**
     * Combines two Pixels
     *
     * @param list1 - the first pixel list.
     * @param list2 - the second pixel list.
     * @return a pixel list containing the specified Pixels.
     */
    public Pixels merge( Pixels list1, Pixels list2 )
    {
        if (! list1.equals( list2 ))
        {
                list1.addAll( list2 );
                return list1;
        }
        return list1;
    }


    /**
     * Tests if two Pixels are matched (if they belong to the same ReferenceSurface).
     *
     * @param pixels - the first Pixel.
     * @param c - the second Pixel.
     * @return - true if they are a match, false otherwise.
     */
    public boolean isAMatch( Pixels pixels, Coordinate c )
    {
//        if (pixels.size() <= 3)//TODO how many Coordinates authorized ?
//        {
            for ( Coordinate coordinate : pixels.get() )
            {
                if ( ( Math.abs( c.getZ() - coordinate.getZ() ) ) <= 1 )
                {
                    return true;
                }
            }
//        }
//        else
        return false;
    }

    /**
     * Computes and returns the size of the SurfaceLine instance.
     *
     * @return the computed size;
     */
    public int getCoordinatesNumber( )
    {
        int size = 0;
        for ( int i = 0; i <= this.dimension.length - 1; i++ )
        {
            if ( dimension[ i ] != null )
            {
                size = size + dimension[ i ].size();
            }
        }
        return size;
    }

    /**
     * Computes and returns the size of the SurfaceLine instance.
     *
     * @return the computed size;
     */
    public int getPixelNumber()
    {
        int size = 0;
        for ( int i = 0; i <= this.dimension.length - 1; i++ )
        {
            if ( dimension[ i ] != null )
            {
                size++;
            }
        }
        return size;
    }

    public void setSize( int size )
    {
        this.size = size;
    }

    public Pixels[] getDimension()
    {
        return dimension;
    }

    public int getLength( )
    {
        return this.dimension.length;
    }





    public boolean hasDuplicate( )
    {
        for ( int i = 0; i <= this.dimension.length - 1; i++ )
        {
            if (this.dimension[i] != null && this.dimension[i].size() > 1)
            {
                return true;
            }
        }
        return false;
    }

    public int getLine( )
    {
        return line;
    }

    public void setLine( int line )
    {
        this.line = line;
    }

}
