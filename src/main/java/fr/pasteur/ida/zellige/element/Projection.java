/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.element;

import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.UnsignedShortType;

public class Projection< T extends RealType< T > & NativeType< T > >
{
    private Img< T > projection;
    private Img< UnsignedShortType > extractedHeightMap;
    private Img< T > segmentedSurface;
    private Img< T > reduced3DSpace;
    private Img< BitType > segmentedSurfaceMask;
    private int index;


    public Img< T > getSegmentedSurface()
    {
        return segmentedSurface;
    }

    public void setSegmentedSurface( Img< T > segmentedSurface )
    {
        this.segmentedSurface = segmentedSurface;
    }

    public Img< UnsignedShortType > getExtractedHeightMap()
    {
        return extractedHeightMap;
    }

    public void setExtractedHeightMap( Img< UnsignedShortType > extractedHeightMap )
    {
        this.extractedHeightMap = extractedHeightMap;
    }

    public Img< T > get()
    {
        return projection;
    }

    public void setProjection( Img< T > projection )
    {
        this.projection = projection;
    }

    public Img< T > getReduced3DSpace()
    {
        return reduced3DSpace;
    }

    public void setReduced3DSpace( Img< T > reduced3DSpace )
    {
        this.reduced3DSpace = reduced3DSpace;
    }

    public Img< BitType > getSegmentedSurfaceMask()
    {
        return segmentedSurfaceMask;
    }

    public void setSegmentedSurfaceMask( Img< BitType > segmentedSurfaceMask )
    {
        this.segmentedSurfaceMask = segmentedSurfaceMask;
    }
}
