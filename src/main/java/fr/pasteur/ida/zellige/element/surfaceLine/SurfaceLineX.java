/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige.element.surfaceLine;

import fr.pasteur.ida.zellige.element.ose.AbstractOSE;

public class SurfaceLineX extends SurfaceLine
{


    /**
     * @param length the size of the {@link SurfaceLine}
     * @param os     - a list of Coordinates to fill the pixel array.
     */
    public SurfaceLineX( int length, AbstractOSE os )
    {
        super( length, os );
        this.setLine( os.get( 0 ).getY() );
    }

    public SurfaceLine match( AbstractOSE os, int direction, int overlap, double connexity )
    {
        SurfaceLineX surfaceLineX = new SurfaceLineX( this.getLength(), this.getLine() + direction );
        return this.match2( os, overlap, connexity, surfaceLineX );
    }

    /**
     * Constructor.
     *
     * @param length the size of the {@link SurfaceLine}
     * @param line - the position on the stack.
     */
    public SurfaceLineX( int length, int line )
    {
        super(length,  line );
    }

    public SurfaceLineX( AbstractOSE os, int size )
    {
        super( os , size);
        this.setLine( os.get( 0 ).getY() );
    }



}
