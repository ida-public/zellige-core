/*-
 * #%L
 * Zellige
 * %%
 * Copyright (C) 2020 - 2023 Institut Pasteur
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */
package fr.pasteur.ida.zellige;

import fr.pasteur.ida.zellige.element.Pixels;
import fr.pasteur.ida.zellige.element.ReferenceSurface;
import fr.pasteur.ida.zellige.element.Surface;
import fr.pasteur.ida.zellige.steps.construction.ConstructionCompletion;
import fr.pasteur.ida.zellige.steps.construction.exception.NoSurfaceFoundException;
import fr.pasteur.ida.zellige.steps.construction.rounds.ConstructionParameters;
import fr.pasteur.ida.zellige.steps.construction.rounds.FirstRoundConstruction;
import fr.pasteur.ida.zellige.steps.construction.rounds.SecondRoundConstruction;
import fr.pasteur.ida.zellige.steps.projection.DisplayParameters;
import fr.pasteur.ida.zellige.steps.projection.NoPossibleDisplayException;
import fr.pasteur.ida.zellige.steps.projection.ProjectionParameters;
import fr.pasteur.ida.zellige.steps.projection.ReferenceSurfaceProjection;
import fr.pasteur.ida.zellige.steps.selection.Selection;
import fr.pasteur.ida.zellige.steps.selection.classification.ClassificationParameters;
import fr.pasteur.ida.zellige.steps.selection.exception.DataValidationException;
import fr.pasteur.ida.zellige.steps.selection.exception.NoClassificationException;
import fr.pasteur.ida.zellige.steps.selection.postTreatment.PostTreatmentParameters;
import fr.pasteur.ida.zellige.steps.selection.pretreatment.PretreatmentParameters;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.ImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static fr.pasteur.ida.zellige.steps.selection.util.Derivative.FORWARD;


public class ReferenceSurfaceExtraction< T extends RealType< T > & NativeType< T > >
{

    private final static Logger LOGGER = LoggerFactory.getLogger( ReferenceSurfaceExtraction.class );

    private final ArrayList< ReferenceSurface< T > > referenceSurfaces = new ArrayList<>();
    private final RandomAccessibleInterval< T > input;
    private final ImgFactory< T > factory;

    private Pixels[][] maximums;
    private final static double adequacy = 1;

    private long FS_OSConstructionProcessingTime;
    private long FS_SurfaceConstructionProcessingTime;
    private long SS_OSConstructionProcessingTime;
    private long SS_SurfaceConstructionProcessingTime;
    private int FS_startingOS_count;
    private int FS_OS_count;
    private int FS_smallSurfaces;
    private int FS_goodSurfaces;
    private int FS_finalizedSurfaces;
    private int SS_startingOS_count;
    private int SS_OS_count;
    private int SS_smallSurfaces;
    private int SS_goodSurfaces;
    private static final String derivativeMethod = FORWARD;

    public ReferenceSurfaceExtraction( RandomAccessibleInterval< T > input, ImgFactory< T > factory )
    {
        this.input = input;
        this.factory = factory;
    }

    public static double getAdequacy()
    {
        return adequacy;
    }



    public void select( PretreatmentParameters pretreatmentParameters, ClassificationParameters classificationParameters, PostTreatmentParameters postTreatmentParameters ) throws NoClassificationException, DataValidationException
    {
        /* First step : Pixel selection */
        LOGGER.debug( "Running selection..." );
        maximums = Selection.run( input, pretreatmentParameters, classificationParameters, postTreatmentParameters );
    }

    public void construct( ConstructionParameters[] constructionParameters ) throws NoSurfaceFoundException
    {
        LOGGER.debug( "Running construction..." );
        /*  First round construction*/
        FirstRoundConstruction step1 = new FirstRoundConstruction( maximums, constructionParameters[ 0 ] );
        step1.process();
        ArrayList< Surface > tempSurfaces = step1.getSurfaces();
        FS_startingOS_count = step1.getStartingOSCount();
        FS_OS_count = step1.getOSCount();
        FS_goodSurfaces = step1.getGoodSurfacesCount();
        FS_smallSurfaces = step1.getSmallSurfacesCount();
        FS_finalizedSurfaces = tempSurfaces.size();
        FS_OSConstructionProcessingTime = step1.getOSConstructionProcessingTime();
        FS_SurfaceConstructionProcessingTime = step1.getConstructionProcessingTime();
        LOGGER.debug( "first round surfaces = " + tempSurfaces.size() );
        /* Second round construction */
        SecondRoundConstruction step2 =
                new SecondRoundConstruction( tempSurfaces, constructionParameters[ 1 ] );
        step2.process();
        ArrayList< Surface > finalSurfaces = step2.getSurfaces();
        SS_startingOS_count = step2.getStartingOSCount();
        SS_OS_count = step2.getOSCount();
        SS_goodSurfaces = step2.getGoodSurfacesCount();
        SS_smallSurfaces = step2.getSmallSurfacesCount();
        SS_OSConstructionProcessingTime = step2.getOSConstructionProcessingTime();
        SS_SurfaceConstructionProcessingTime = step2.getConstructionProcessingTime();
        /* Building reference surfaces */
        referenceSurfaces.addAll( ConstructionCompletion.run( input, factory, finalSurfaces ) );
//        referenceSurfaceInstantiation( finalSurfaces );
        LOGGER.debug( " Constructions of {} surfaces.", referenceSurfaces.size() );

    }

    public void project( ProjectionParameters projectionParameters, DisplayParameters displayParameters ) throws NoPossibleDisplayException
    {
        LOGGER.debug( "Running projection..." );
        for ( ReferenceSurface< T > referenceSurface : referenceSurfaces )
        {
            referenceSurface.init();
            ReferenceSurfaceProjection.run( referenceSurface, projectionParameters, displayParameters );
        }
    }

    public void project( int delta, String projectionType, boolean projectionDisplay,
                         boolean rawHeightMapDisplay,
                         boolean extractedHeightMapDisplay,
                         boolean reduced3DSpaceDisplay,
                         boolean segmentedSurfaceDisplay,
                         boolean segmentedSurfaceMaskDisplay, int delta2 ) throws NoPossibleDisplayException
    {
        LOGGER.debug( "Running projection..." );
        for ( ReferenceSurface< T > referenceSurface : referenceSurfaces )
        {
            referenceSurface.init();
            ReferenceSurfaceProjection.run( referenceSurface, delta, projectionType,
                    projectionDisplay,
                    rawHeightMapDisplay,
                    extractedHeightMapDisplay,
                    reduced3DSpaceDisplay,
                    segmentedSurfaceDisplay,
                    segmentedSurfaceMaskDisplay, delta2 );
        }
    }

    public void setMaximums( Pixels[][] maximums )
    {
        this.maximums = maximums;
    }

    public ArrayList< ReferenceSurface< T > > getReferenceSurfaces()
    {
        return referenceSurfaces;
    }

    public Pixels[][] getMaximums()
    {
        return maximums;
    }

    public static String getDerivativeMethod()
    {
        return derivativeMethod;
    }


    public long getFS_OSConstructionProcessingTime()
    {
        return FS_OSConstructionProcessingTime;
    }

    public long getFS_SurfaceConstructionProcessingTime()
    {
        return FS_SurfaceConstructionProcessingTime;
    }

    public long getSS_OSConstructionProcessingTime()
    {
        return SS_OSConstructionProcessingTime;
    }

    public long getSS_SurfaceConstructionProcessingTime()
    {
        return SS_SurfaceConstructionProcessingTime;
    }

    public int getFS_smallSurfaces()
    {
        return FS_smallSurfaces;
    }

    public int getFS_goodSurfaces()
    {
        return FS_goodSurfaces;
    }

    public int getFS_finalizedSurfaces()
    {
        return FS_finalizedSurfaces;
    }

    public int getSS_smallSurfaces()
    {
        return SS_smallSurfaces;
    }

    public int getSS_goodSurfaces()
    {
        return SS_goodSurfaces;
    }

    public int getFS_OS_count()
    {
        return FS_OS_count;
    }

    public int getSS_OS_count()
    {
        return SS_OS_count;
    }

    public int getFS_startingOS_count()
    {
        return FS_startingOS_count;
    }

    public int getSS_startingOS_count()
    {
        return SS_startingOS_count;
    }



}


