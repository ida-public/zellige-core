<img src="src/main/resources/icons/Zellige_logo.png" width="408" height="408" alt="drawing"/>

# Zellige : 3D fluorescence microscopic images surface extraction tool.

Zellige is a software tool allowing the automated extraction of a non-prescribed number of various surfaces from a 3D
image acquired by fluorescence microscopy. The program is available through a FIJI plugin.

**Table of content.**

* [Citation.](#citation)
* [Motivation](#motivation)
* [Description.](#description)
* [Installation.](#installation)
* [Example datasets.](#example-datasets)
* [Usage.](#usage)

+ [Other projection tools.](#other-projection-tools)

## Citation.

If you use this work for your research, please cite our work:

> __Extracting multiple surfaces from 3D microscopy images in complex biological tissues with the Zellige software tool.__
>
> Céline Trébeau, Jacques Boutet de Monvel, Gizem Altay, Jean-Yves Tinevez, Raphaël Etournay
>
> bioRxiv 2022.04.05.485876; doi: https://doi.org/10.1101/2022.04.05.485876

## Motivation

The quantitative study of epithelium development by fluorescence microscopy requires either the dissection of the
epithelium to discard all potential contaminating structure (in terms of signal) surrounding it or the acquisition of
all the epithelium adjacent structures allowing to have a more accurate state of the epithelium in its biological and
physical environment. The latter approach requires to segment each structure separately. Most existing methods allow for
the extraction of a single surface which has to be smooth, and presenting a sufficiently high signal intensity and
contrast and usually fail otherwise. These methods so far do not address the need to extract several surfaces of
interest within the same volume. We developed Zellige, a tool allowing the automated extraction of a non-prescribed
number of surfaces of varying inclination, contrast, and texture from a 3D image. The tool requires the adjustment of a
small set of control parameters, for which we provide an intuitive interface implemented as a Fiji plugin.

## Description.

Zellige performs projection of multiple surfaces of interest on a 2D plane from a 3D image. For now, it can only be used
on 1-channel 3D images presenting one or several surfaces. The program takes a 3D stack as input and generates the
projection of all surfaces contained in the 3D stack as 2D images. That process can be divided into 3 stages :

![ZelligePipeline](doc/zellige_pipeline.jpg)

- first the selection of all the pixels belonging to any surface
- second the regrouping of those selected pixels to produce a **height map** for each surface
- third the use of each height map to extract a projection.

The height-map is then used to extract a projection from the 3D image. A fixed offset can be specified separately for
each channel, and is used to collect intensity in planes above or below the reference surface. Several planes, specified
by a ∆z parameter, can be accumulated to generate a better projection, averaging the pixel values or taking the maximum
value of these planes.

## Installation.

As a FIJI plugin (https://fiji.sc/) ,Zellige can be installed directly within
the [Fiji updater](https://imagej.net/ImageJ_Updater).

In the `Manage update sites` window, check the `Zellige` plugin. Click the `Close` button, then the `Apply changes`
button. After the plugin is downloaded, restart Fiji. The plugin can then be launched from the _Plugins > Process >
Zellige_ menu item.

## Example datasets

https://zenodo.org/communities/zellige/

## Usage

https://imagej.net/plugins/zellige

### Other projection tools.

There are several other tools to generate this kind of projections:

- Local Z projector, an ImageJ plugin: https://gitlab.pasteur.fr/iah-public/localzprojector
- Stack Focuser, an ImageJ plugin: https://imagej.nih.gov/ij/plugins/stack-focuser.html
- SurfCut, an Image macro: https://bmcbiol.biomedcentral.com/articles/10.1186/s12915-019-0657-1
- PreMosa, a standalone software: https://academic.oup.com/bioinformatics/article/33/16/2563/3104469
- Extended Depth of Field, an ImageJ plugin: http://bigwww.epfl.ch/demo/edf/
- Min. Cost Z Surface, an ImageJ plugin: https://imagej.net/Minimum_Cost_Z_surface_Projection
- FastSME and SME, MATLAB software for the extraction of smooth-manifold
  structures: https://openaccess.thecvf.com/content_cvpr_2018_workshops/w44/html/Basu_FastSME_Faster_and_CVPR_2018_paper.html
